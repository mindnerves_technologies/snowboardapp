import React, { Component } from 'react';
import {
    View,
    ActivityIndicator,
    Dimensions,
} from 'react-native';

class CommonActivityIndicator extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style = { this.props.showCenter && { flex: 1, justifyContent: 'center' } }>
                <ActivityIndicator size={ this.props.showCenter ? "large" : "small" } color='#30a192' />
            </View>
        )
    }
}

export default CommonActivityIndicator;
