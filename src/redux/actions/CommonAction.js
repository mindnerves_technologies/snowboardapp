// import {  } from '../services/CommonService';

export const NO_NETWORK = 'NO_NETWORK';
export const NETWORK_COME = 'NETWORK_COME';

export function networkLost() {
	return { type: NO_NETWORK };
}

export function networkAvailable() {
	return { type: NETWORK_COME };
}