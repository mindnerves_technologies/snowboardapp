/**
 * @author Kartikaiya Mayatwal
 * Date April 17, 2021
 */

import { combineReducers } from 'redux';
import CommonReducer from './CommonReducer';
export default combineReducers({
	CommonReducer
});
