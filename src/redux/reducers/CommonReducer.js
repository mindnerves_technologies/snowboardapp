import {
	NO_NETWORK,
	NETWORK_COME
} from '../actions/CommonAction';

const initialState = {
	isNetworkAvailable: true,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case NETWORK_COME:
			return {
				...state,
				isNetworkAvailable: true,
			};
		case NO_NETWORK:
			return {
				...state,
				isNetworkAvailable: false,
			};
		default: return state;
	}
}