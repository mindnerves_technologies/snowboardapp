import React from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import View, { StyleSheet, ScrollView } from 'react-native';
import Text, { Body, Container, Title, Header, Left, Right, Icon, Subtitle } from 'native-base';
import Api from "../constant/Api";
import Toast from 'react-native-simple-toast';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import { firebase } from '@react-native-firebase/database';
// import { ScrollView } from 'react-native-gesture-handler';
// import database from '@react-native-firebase/database';
// const reference = database().ref('/chats');
let msgdata = [];
export default class ChatScreen extends React.Component {
    state = {
        connection_status: false,
        messages: [],
        userFname: '',
        userId: null,
        getuserData: null,
        filteredchatdata: [],
        // msgdata :[],
    }

    componentDidMount() {
        Util.getAsyncStorage('userData').then((data) => {
            console.log("34Chat", data)
            if (data !== null) {
                this.setState({ userId: data.userId, getuserData: data, userToken: data.userToken, userFname: data.userFName })
                var ref = firebase
                    .app()
                    .database('https://snowboard-8d322-default-rtdb.firebaseio.com/')
                    .ref('/chats')
                    .orderByChild("chat_id")
                    .equalTo("1_" + (data.userId).toString())
                    .on('value', snapshot => {
                        console.log('filtered data', snapshot.val());
                        if (snapshot.val() == null) {
                            return null;
                        }
                        else {
                            var value = Object.values(snapshot.val());
                            // console.log('filtered value', value);
                            let filterdata = value;
                            // filterdata.push(size)
                            console.log('filtered data after push', filterdata);
                            // this.setState({ filteredchatdata: snapshot.val() })
                            msgdata = [];
                            filterdata.map((item, index) => {
                                console.log(item, "+++++++++++++")
                                // console.log(index, "+++++++++++++")
                                    msgdata.push(
                                        {
                                            _id: index,
                                            text: item.content,
                                            // createdAt: new Date(),
                                            createdAt: item.created_at,
                                            user: {
                                                _id: item.sender_id,
                                                avatar: 'https://placeimg.com/140/140/any',
                                            },
                                        }
                                    )
                                console.log("msgdata after apply", msgdata)
                            })

                            const sorted = msgdata.sort((a, b) => {
                                const dateA = new Date(`${a.createdAt}`).valueOf();
                                const dateB = new Date(`${b.createdAt}`).valueOf();
                                if (dateA < dateB) {
                                    return 1; // return -1 here for DESC order
                                }
                                return -1 // return 1 here for DESC Order
                            });

                            this.setState({
                                messages: msgdata,
                            })
                        }


                    });
            }
        })

        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });
                //call API

            } else {
                this.setState({ connection_status: false });
            }
        });
    }

    onSend(messages = []) {
        // console.log("filteredchatdata after snap", this.state.filteredchatdata)
        console.log('onSend', messages)
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
        this.postMessage(messages)
    }

    postMessage = (msg) => {
        const UID = this.state.userId
        const token = this.state.userToken
        if (this.state.connection_status) {
            var url = Api.baseUrl + 'chat'
            let mUserData = {
                "content": msg[0].text,
                "name": this.state.userFname,
                "user_id": this.state.userId,
                "sender_id": this.state.userId
            };
            { console.log("token", token) }
            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + token,
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("postresponse 75", response)
                    // this.setState({ challengedata: response.data.videos, level: response.data.videos[0].level })
                    var ref = firebase
                        .app()
                        .database('https://snowboard-8d322-default-rtdb.firebaseio.com/')
                        .ref('/chats')
                        .child(response.id.toString())
                        // .push()
                        .set(
                            // {[response.id]:response}
                            response, () => null
                        )

                }
                )
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }


    render() {
        return (
            <Container>
                <Header style={styles.headerContainer}>
                    <Left style={{ flex: 0.2, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818' }} onPress={() => this.props.navigation.goBack()} />
                    </Left>
                    <Body style={{ alignItems: 'flex-start' }}>
                        {this.state.userFname !== "" && this.state.userFname !== undefined &&
                            <Title style={styles.titleStyle}>{this.state.userFname}</Title>
                        }
                        <Subtitle style={styles.subTitleStyle}>{"Online"}</Subtitle>
                    </Body>
                    {/* <Body>
                        <Title style={{ fontSize: 25, fontWeight: 'bold', color: '#fff',textAlign:'center' }}>{"Rahul"}</Title>
                    </Body> */}
                    <Right style={{ flex: 0.2 }} />
                </Header>
                <GiftedChat
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    user={{
                        _id: this.state.userId,
                    }}
                />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    // profileImage: {
    //     width: 30,
    //     height: 30,
    //     borderRadius: 15,
    //     marginLeft: 6
    // },
    // composeContainer:{
    //     flexDirection: 'row',
    //     backgroundColor : colors.textInputBackgroundColor
    // },
    // textInputStyle:{
    //     borderWidth:StyleSheet.hairlineWidth, 
    //     borderRadius:20, paddingLeft:10, paddingTop:7,
    //     marginHorizontal:5,
    //     backgroundColor: colors.chatInputBackGroundColor
    // },
    // iconsView:{
    //     width:40, alignItems:'center', justifyContent:'center'
    // },
    // imageBubbleStyle : {
    //         width: 150,
    //         height: 100,
    //         borderRadius: 13,
    //         margin: 3,
    //         marginRight:10,
    //         resizeMode: 'cover'
    // },
    headerContainer: {
        backgroundColor: '#e3e6f0'
    },
    titleStyle: {
        color: '#D73951', fontWeight: 'bold'
    },
    subTitleStyle: {
        color: '#D73951'
    },
    // backArrow:{
    //     fontSize: fonts.fontSize_18, color: colors.white
    // }
});