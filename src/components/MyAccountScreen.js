import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon, CheckCircleIcon } from 'native-base';
import Images from "../constants/Images";
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import Toast from 'react-native-simple-toast';
import CommonActivityIndicator from '../Util/ActivityIndicator';
import I18n from '../common/localization/index';

// const { width } = Dimensions.get("window");
// const height = width * 0.6;

export default class MyAccountScreen extends React.Component {
    state = {

    }

    componentDidMount = () => {
        // this.willBlur = this.props.navigation.addListener("willBlur", payload =>
        // 	BackHandler.removeEventListener("hardwareBackPress", this.handleBackButtonClick()),
        // );
        //To get the network state once

        // NetInfo.addEventListener(state => {
        // 	if (state.isConnected) {
        // 		this.setState({ connection_status: true });
        // 		//call API
        // 	} else {
        // 		this.setState({ connection_status: false });
        // 	}
        // });
        console.log("Before api")

        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data.userId,getuserData: data})
            }
        })

        setTimeout(() => {
            console.log("Before api")
            this.LoadViewProfileAPI();
            console.log("after api")
        }, 100);
    }


    LoadViewProfileAPI = () => {

        // this.setState({ showProgressBar: true })

        // if (this.state.connection_status) {
        console.log("In api")
        var url = 'http://139.162.159.9/snowboard/api/user/' + this.state.userId;
        console.log("In api 53")
        let mUserData = {
            "UID": this.state.userId
        };
        console.log("In api 57", url)
        fetch(url, {
            method: 'GET',
            headers: {
                // 'Authorization': 'Bearer exn50dak2a5iahy02hawo5il0y6j25ct',
                'Content-Type': 'application/json; charset=utf-8',
                'Accept': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-control-allow-origin': '*',
            },
            // body: JSON.stringify(mUserData),
        })
            .then((response) => response.json())
            .then((response) => {
                console.log("response", response)
                if (response.data !== "") {
                    this.setState({ Profiledata: response })
                }
                else {
                    this.setState({ response: response })
                }
            })
            .catch((error) => {
                console.log("In catch 75")
                this.setState({ showProgressBar: false })
                Toast.showWithGravity(error, Toast.SHORT, Toast.CENTER);
            })
            .done();

        // } 

        // else {
        //     Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);

        //     this.setState({ showProgressBar: false })
        // }
    }

    logout = async () => {

        // clear cookies
        // CookieManager.clearAll()
        //   .then((res) => {
    
    
        //   });
    
        try {
        //   this.props.data.setState({showDrawer:false})
        //   this.setState({ showDrawer: false })
          await AsyncStorage.removeItem('userData');
          this.props.navigation.replace('Login')
          // this.props.global_navigation.replace('dashboard')
        } catch (error) {
        }
    
      }


    render() {
        // const {Profiledata}=this.state;
        // console.log("Profiledata",Profiledata)
        return (

            <Container>
                <View style={{ flex: 1, }}>
                    <ScrollView>
                        <View>
                            <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: '#e3e6f0' }}>
                                <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                                <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#00a0e4', paddingLeft: 20 }}>{I18n.t("myaccount")}</Text>
                            </View>
                            <View style={{
                                elevation: 0,
                                backgroundColor: '#fff',
                                width: this.state.deviceWidth, padding: 15, borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717'
                            }}>
                                {this.state.Profiledata !== undefined && this.state.Profiledata.data !== "" ?
                                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                        <Image
                                            source={Images.Profileimg}
                                            style={{ tintColor: '#696969', height: 50, width: 50, marginRight: 30 }}
                                        />

                                        <View style={{ flex: 1, }}>

                                            <Text style={[styles.text, { fontSize: 20, fontFamily: 'Montserrat-Regular' }]}>
                                                {this.state.Profiledata.data.first_name} {this.state.Profiledata.data.last_name}
                                            </Text>

                                            <Text style={[styles.text, { fontSize: 14, fontFamily: 'Montserrat-Regular' }]}>
                                                {this.state.Profiledata.data.email}
                                            </Text>

                                            <Text style={[styles.text, { fontSize: 14, fontFamily: 'Montserrat-Regular' }]}>
                                                {this.state.Profiledata.data.level}
                                            </Text>
                                            {/* <TouchableOpacity style={{ alignItems: 'center', width: 130, justifyContent: 'center', backgroundColor: '#717384', height: 30, borderRadius: 10, marginTop: 10 }} >
                                                <Text style={{ color: '#fff', fontSize: 15, textAlign: 'center' }}>{"Upload_Video"}</Text>
                                            </TouchableOpacity> */}

                                        </View>
                                    </View>
                                :
                                <CommonActivityIndicator></CommonActivityIndicator>
                               }
                            </View>

                            <View>
                                <Text style={{ fontSize: 20, marginLeft: 15, fontFamily: 'Montserrat-Bold', color: '#696969' }}>{I18n.t('account')}</Text>
                                <View style={{ paddingVertical: 15 }}>
                                    <TouchableOpacity style={styles.userRow}
                                    onPress={() => this.props.navigation.navigate("Editprofile")}
                                    >
                                        <View style={styles.profileContainer}>
                                            <Icon type={'Entypo'} name={"user"} style={{ fontSize: 30, color: '#696969', marginTop: 5, marginLeft: 40 }} />
                                            <View style={{ flex: 1, paddingLeft: 10, flexDirection: 'row', }}>
                                                <Text style={[styles.profileName, { fontWeight: "normal", fontFamily: 'Montserrat-Regular' }]}>{I18n.t('editprofile')}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ paddingVertical: 15 }}>
                                    <TouchableOpacity style={styles.userRow}>
                                        <View style={styles.profileContainer}>
                                            <Icon type={'Entypo'} name={"lock"} style={{ fontSize: 30, color: '#696969', marginTop: 5, marginLeft: 40 }} />
                                            <View style={{ flex: 1, paddingLeft: 10, flexDirection: 'row', }}>
                                                <Text style={[styles.profileName, { fontWeight: "normal", fontFamily: 'Montserrat-Regular' }]}>{I18n.t('changepassword')}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            
                            {this.state.getuserData != null ?
                                <View style={{ width: 410, paddingHorizontal: 80, paddingVertical: 10 }}>
                                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 50, borderRadius: 20 }} onPress={() => this.logout()}>
                                        <Text style={{ color: '#fff', fontSize: 20, fontFamily: 'Montserrat-Regular' }}>{I18n.t('Signout')}</Text>
                                    </TouchableOpacity>
                                </View>
                                : null}
                        </View>
                    </ScrollView>
                </View>
            </Container>

        )
    }
}

const styles = StyleSheet.create({
    profileContainer: {
        flexDirection: "row",
        alignItems: "center",
        // marginTop: 2,
        marginLeft: 6,
        marginBottom: 8
    },
    text: {
        fontFamily: 'bold',
        color: '#000',
        marginBottom: 5
    },
    profileImage: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginLeft: 40
    },
    profileName: {
        marginLeft: 6,
        fontSize: 16,
        color: '#181818'
    },
    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },
    userRow: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#171717'
    },

})
