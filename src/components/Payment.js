// import { CardField, useStripe } from '@stripe/stripe-react-native';
// import React, { useState } from "react";
// export default function Payment() {
//     const { confirmPayment } = useStripe();

//     return (
//       <CardField
//         postalCodeEnabled={true}
//         placeholder={{
//           number: '4242 4242 4242 4242',
//         }}
//         cardStyle={{
//           backgroundColor: '#FFFFFF',
//           textColor: '#000000',
//         }}
//         style={{
//           width: '100%',
//           height: 50,
//           marginVertical: 30,
//         }}
//         onCardChange={(cardDetails) => {
//           console.log('cardDetails', cardDetails);
//         }}
//         onFocus={(focusedField) => {
//           console.log('focusField', focusedField);
//         }}
//       />
//     );
//   }


import {
    CardField,
    CardFieldInput,
    useStripe,
} from '@stripe/stripe-react-native';
import { StripeProvider } from '@stripe/stripe-react-native';
import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions } from "react-native";
import { Text } from 'native-base';
const { height, width } = Dimensions.get("window");
import Toast from 'react-native-simple-toast';
import Api from "../constant/Api";

// secret key:"sk_test_51KVZg7SIjp4YCshVJ75U9Pee3KGiRQ9PirQnKUDjzszmYaYYSwnAzayYW4MD4kaMEOoJoerpKMMsXZfAK56bBq8u00GWL0ABCZ";
// publishableKey = 'pk_test_51KVZg7SIjp4YCshV5achqah7KPzMjI57e7ndtkG9NmlM9Kmscm7DukZfCSHQnUclC0trNtGncgkfvWzOT5Cl7jnL00TH6baBEv'

const publishableKey = 'pk_test_mDZMmOjg91048aGeRkKUTw2X00JDm4eCZU'
export default class Payment extends React.Component {
    // const { confirmPayment, handleCardAction } = useStripe()
    constructor(props) {
        super(props);
        this.state = {
            card: CardFieldInput.Details | null,
        };
    }

    stripeapicall = () => {
        this.setState({ activityIndicator: false })
        { console.log("In stripe") }
        // if (this.state.connection_status) {

        var url = Api.baseUrl + 'stripe_payment'

        let mUserData =
        {
            "user_id": 192,
            "number": 4242424242424242,
            "exp_month": 2,
            "exp_year": 2023,
            "cvc": 314,
            "amount": 10,
            "credit": 20
        };


        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(mUserData),
        })
            .then((response) => response.json())
            .then((response) => {
                console.log("response", response)
            })
            .catch((error) => {
                this.setState({ activityIndicator: true })
                Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
            })
            .done();

        // }
        // else {
        //   Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //   this.setState({ activityIndicator: true })
        // }
    }

    render() {
        return (
            <View style={{ alignItems: 'center' }}>
                <StripeProvider
                    publishableKey={publishableKey}
                >
                    <CardField
                        postalCodeEnabled={true}
                        placeholder={{
                            number: '4242 4242 4242 4242',
                        }}
                        cardStyle={{
                            backgroundColor: '#FFFFFF',
                            textColor: '#000000',
                        }}
                        style={{
                            width: '100%',
                            height: 50,
                            marginVertical: 30,
                        }}
                        onCardChange={(cardDetails) => {
                            // setCard(cardDetails);
                            this.setState({ cardDetails: cardDetails })
                        }}
                        onFocus={(focusedField) => {
                            console.log('focusField', focusedField);
                        }}
                    />
                    <TouchableOpacity style={{ backgroundColor: 'coral', padding: 6, alignItems: 'center', width: 100 }}
                        onPress={this.stripeapicall}
                    >
                        <Text>{'Pay Now'}</Text>
                    </TouchableOpacity>
                </StripeProvider>
            </View>
        )
    }
}

