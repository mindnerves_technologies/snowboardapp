import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions, ImageBackground } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
import Carousel from 'react-native-snap-carousel';
const { height, width } = Dimensions.get("window");
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import I18n from '../common/localization/index';
import Images from "../constants/Images";

// const isCarousel = React.useRef(null)
var data = [
    {
        title: "Ski Together",
        body: "  pellentesque eu, pretium quis, sem.",
        imgUrl: Images.snoboardimg
    },
    {
        title: "Ski Together",
        body: ". . Curabitur at lacus ac velit ornare lobortis. ",
        imgUrl: Images.homescreenimg2
    },
    {
        title: "Ski Together",
        body: "  posuere ac, mattis non, nunc.",
        imgUrl:Images.homescreenimg3
    }
]
export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data.userId, getuserData: data })
            }
        })
    }

    _renderBannerItem({ item, index }) {
        // { console.log("item", item) }
        return (
            <View style={{}}>
                <ImageBackground
                    source={item.imgUrl}
                    resizeMode="cover"
                    style={{
                        width: '100%', height: 480,
                    }}>
                    <View style={{ marginTop: 380 }}>
                        <Text style={{ fontSize: 25, textAlign: 'center', fontWeight: "bold", color: 'white' }}>{item.title}</Text>
                        <Text style={{ fontSize: 15, textAlign: 'center', fontWeight: "bold", color: 'white' }}>{item.body}</Text>
                    </View>
                </ImageBackground>

            </View>
        )
    }

    render() {
        return (
            <View>
                <View>
                    <View style={{ flexDirection: 'row', paddingVertical: 10, paddingLeft: 25, backgroundColor: '#e3e6f0' }}>
                        <Text style={{ fontSize: 25, fontFamily: 'Montserrat-Bold', color: '#00a0e4' }}>{I18n.t("snowboard")}</Text>
                    </View>
                    <View style={{ flexDirection: 'column', justifyContent: 'center', width: '100%', }}>
                        <Carousel
                            ref={'_carousel1'}
                            data={data}
                            renderItem={this._renderBannerItem.bind(this)}
                            sliderWidth={412}
                            itemWidth={412}
                            loop={true}
                            autoplay={true}
                            // autoplayDelay={100}
                            onSnapToItem={(index) => this.setState({ activeSlide: index })}
                            lockScrollWhileSnapping={false}
                            // inactiveSlideOpacity={1}
                            inactiveSlideScale={1}
                            enableMomentum={true}
                            activeSlideAlignment={'start'}
                        // autoplayInterval={100}
                        />
                    </View>
                    {/* {console.log("after carousal")} */}
                    <View style={{ width: 410, paddingHorizontal: 40, paddingTop: 40, paddingBottom: 10 }}>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 60, borderRadius: 50 }} onPress={() => this.props.navigation.navigate("SignupScreen")}>
                            <Text style={{ color: "white", fontSize: 20, fontFamily: 'Montserrat-Regular' }}>{I18n.t("createaccount")}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: 410, paddingHorizontal: 40, paddingTop: 20 }}>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff', height: 50, borderRadius: 50 }} onPress={() => this.props.navigation.navigate("Login")}>
                            <Text style={{ color: 'blue', fontSize: 20, fontFamily: 'Montserrat-Regular', textDecorationLine: 'underline' }}>{I18n.t("logintoaccount")}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {

    },
    image: {
        width: 100,
        height: 100,
    },
    header: {

    },
    body: {

    }
})