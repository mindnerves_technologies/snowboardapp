import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import Drawer from '../Style/Drawer';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import Toast from 'react-native-simple-toast';
import Modal from "react-native-modal";
import CommonActivityIndicator from '../Util/ActivityIndicator';
import Api from "../constant/Api";
import I18n from '../common/localization/index';
// import fonts from '../assets/fonts';

var array = [
    {
        name: "Super",
        about: "Installment credit comes in the form of a loan with a fixed loan amount",
        Euro: 10,
        Credit: 1000

    },
    {
        name: "Premium",
        about: "Installment credit comes in the form of a loan with a fixed loan amount",
        Euro: 90,
        Credit: 10000
    },

]

export default class creditTransaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDrawer: false,
            connection_status: false,
            // array:[],
        };
    }

    componentDidMount() {
        // this.callApi();
        // this.CacheProductCallApi();

        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data, getuserData: data, userToken1: data.userToken })
                this.CredittransactionApi(data);
                this.HeaderApi(data);
            }
        })

        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });
                //call API

            } else {
                this.setState({ connection_status: false });
            }
        });

    }

    HeaderApi = (data) => {
        // if (this.state.connection_status) {
        console.log("data.userToken,", data.userToken)
        var url = 'http://139.162.159.9/snowboard/api/crowntrophydetails'
        console.log("after url", data.userToken)

        fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': "Bearer " + data.userToken,
                'Content-Type': 'application/json',
            },
            // body: JSON.stringify(mUserData),
        })
            // .then((response) =>{
            //     // response.json()
            //     console.log("response assreqlist", response)
            // } )
            // // .then((response) => {
            // //     console.log("response assreqlist", response)
            // //     this.setState({ assreqlistdata: response.data.data_assessment_request })
            // // })
            .then((response) => response.json())
            .then((response) => {
                console.log("response headerapi", response)
                this.setState({ headerapidata: response.data })
            })
            .catch((error) => {
                console.log("error", error)
                this.setState({ showProgressBar: false })
                Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
            })
            .done();
        // }
        // else {
        //     Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //     this.setState({ showProgressBar: false })
        // }
    }

    CredittransactionApi = (data) => {

        // if (this.state.connection_status) {

        var url = Api.baseUrl + 'get_credit_transactions'

        fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': "Bearer " + data.userToken,
                'Content-Type': 'application/json',
            },
            // body: JSON.stringify(mUserData),
        })
            .then((response) => response.json())
            .then((response) => {
                console.log("response transaction", response)
                this.setState({ credittransactiondata: response.data })
            })
            .catch((error) => {
                console.log("error", error)
                this.setState({ showProgressBar: false })
                Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
            })
            .done();
        // }
        // else {
        //     Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //     this.setState({ showProgressBar: false })
        // }
    }

    clickHamburgerMenu = () => {
        this.setState({ showDrawer: !this.state.showDrawer })
    }

    _renderBannerItem({ item, index }) {
        console.log("item in credit transaction screen", item);
        return (
            <View>
                <ScrollView>
                    {/* <View style={[styles.card, styles.elevation]}> */}
                    <View style={{ paddingHorizontal: 15 }}>
                        <View style={{ flexDirection: 'row', marginTop: 25 }}>
                            {/* <Icon type={'material'} name={'transfer'} style={styles.profileImage} /> */}

                            <Image style={styles.profileImage} source={{ uri: "https://png.pngtree.com/png-vector/20190307/ourlarge/pngtree-vector-transaction-icon-png-image_755654.jpg" }} />
                            <View style={{ paddingLeft: 25 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={[styles.profileName, { fontWeight: "normal" }]}>{item.title}</Text>
                                    <Text style={{ color: 'green', fontFamily: 'Montserrat-SemiBold', right: 30 }}>{item.transaction_type == "credit" ? "+" : "-"}{item.credit}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 15, color: 'gray', paddingTop: 3, fontFamily: 'Montserrat-Regular' }}>{item.transaction_date.split(" ", 1)}</Text>
                                    <Text style={{ fontSize: 15, color: 'gray', paddingTop: 3, fontFamily: 'Montserrat-Regular', marginLeft: 20 }}>{item.transaction_date.substring(item.transaction_date.length - 8)}</Text>
                                </View>
                                <Text style={[styles.profileName, { fontWeight: "normal", fontSize: 15, paddingVertical: 15 }]}>{item.comment}</Text>
                            </View>

                        </View>
                        <View style={styles.userRow}>
                        </View>
                    </View>
                    {/* </View> */}
                </ScrollView>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <View style={{ flex: 1, }}>

                    <View style={{
                        width: '100%', justifyContent: 'space-between',
                        flexDirection: 'row', backgroundColor: '#e3e6f0', paddingVertical: 12, paddingHorizontal: 15, borderBottomWidth: 1
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("MyAccountScreen")}>
                            <Icon type={'FontAwesome'} name={"user-circle"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        </TouchableOpacity>
                        {this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.headerapidata.crown !== "false" && this.state.headerapidata.crown !== null ?
                            <Icon type={'MaterialCommunityIcons'} name={"crown"} style={[styles.iconStyle, { paddingTop: 4, color: this.state.headerapidata.crown }]} />
                            :
                            <Icon type={'MaterialCommunityIcons'} name={"crown"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        }
                        {this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.headerapidata.thropy !== "false" && this.state.headerapidata.thropy !== null ?
                            <Icon type={'FontAwesome'} name={"trophy"} style={[styles.iconStyle, { paddingTop: 4, color: this.state.headerapidata.thropy }]} />
                            :
                            <Icon type={'FontAwesome'} name={"trophy"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        }
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CreditPlanScreen')}>
                            {this.state.headerapidata !== undefined && this.state.headerapidata !== "" &&
                                <Text style={{ fontSize: 15 }}>{this.state.headerapidata.credit}</Text>
                            }
                            <Icon type={'FontAwesome'} name={"copyright"} style={styles.creditlogoStyle} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.clickHamburgerMenu()}>
                            <Icon type={'FontAwesome'} name={"bars"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15,
                        // backgroundColor: '#e3e6f0' 
                    }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818' }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#00a0e4', paddingLeft: 10 }}>{I18n.t("credittransactions")}</Text>
                    </View>
                    {console.log("this.state.credittransactiondata", this.state.credittransactiondata)}
                    {this.state.credittransactiondata !== undefined && this.state.credittransactiondata !== "" ?
                        <ScrollView>
                            <FlatList
                                data={this.state.credittransactiondata}
                                renderItem={this._renderBannerItem.bind(this)}
                                keyExtractor={(item, index) => index.toString()}
                                extraData={this.state}
                                ListEmptyComponent={this.ListEmptyComponent}
                            />
                        </ScrollView>
                        :
                        <CommonActivityIndicator></CommonActivityIndicator>
                    }
                </View>
                {this.state.showDrawer ?
                    <Drawer global_navigation={this.props.navigation} data={this} />
                    : null
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    profileImage: {
        width: "15%",
        height: "100%",
        // borderRadius: 20,
        marginLeft: 15
    },
    userRow: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#171717',
        // paddingVertical: 20,
        // paddingHorizontal: 10,
        paddingTop: 25
    },
    profileName: {
        width: "75%",
        // marginLeft: 25,
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 18,
        color: '#181818'
    },
    card: {
        backgroundColor: 'white',
        // marginHorizontal:10,
        marginLeft: 20,
        // marginRight:10,
        borderRadius: 8,
        // paddingVertical: 2,
        // paddingHorizontal:5,
        width: '90%',
        // marginVertical: 10,
        marginVertical: 10,
    },
    elevation: {
        elevation: 15,
        shadowColor: '#6c757d',
    },
    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
    creditlogoStyle: {
        fontSize: 20,
        color: '#696969',
        // marginLeft: 25
    },

})
