import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions, ImageBackground, TextInput } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Toast from 'react-native-simple-toast';
import Video from 'react-native-video';
import Modal from "react-native-modal";
import { CacheplpApi } from './VideoListScreen';
import { NoCacheplpApi } from './VideoListScreen';
import ModalDropdown from 'react-native-modal-dropdown';

export default class RequestAssesmentScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showExitAppAlert: false
        };
    }

    componentDidMount() {
        // this.cachecallApi();
        // this.NocachecallApi();
    }
    AskAssestment = () => {
        this.setState({ showExitAppAlert: true })
        return true
        //this.props.navigation.goBack(null);
    };
    Hidepopup = () => {
        Toast.showWithGravity(" Rating and Comment submitted", Toast.SHORT, Toast.CENTER)
        this.setState({ showExitAppAlert: false })
        return true
        //this.props.navigation.goBack(null);
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                {/* <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 30, backgroundColor: 'gray' }}>
                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#fff' }}>{"Start Challenge"}</Text>
                </View> */}
                <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: '#e3e6f0' }}>
                    <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#D73951', paddingLeft: 10 }}>{"Start Challenge"}</Text>
                </View>
                {/* <View style={{ alignItems: 'flex-start', width: 200, marginBottom: 30, marginLeft: 40 }}>
                    <Text style={styles.titleStyle}>{"Start Challenge"}</Text>
                </View> */}
                <View>
                    <View style={{ left: 30 }}>
                        <Text style={styles.titleStyle}>{"Title"}</Text>
                    </View>
                    {/* <Image
                        source={{ uri: 'https://cdn.pixabay.com/photo/2019/09/26/03/54/manipulation-4505018__340.jpg' }}
                        resizeMode="cover"
                        style={{
                            width: 330, height: 350,  marginLeft: 40,top:20
                        }} /> */}
                    <Video source={{ uri: 'https://media.istockphoto.com/videos/skier-spraying-snow-at-the-camera-video-id515900616' }}
                        style={{ width: 330, height: 350, marginLeft: 40, top: 20 }}
                        repeat={true}
                        ref={(ref) => {
                            this.player = ref
                        }}
                        resizeMode='cover'
                        onBuffer={this.onBuffer}
                        onError={this.videoError}
                        poster={'https://media.istockphoto.com/vectors/illustration-of-simple-black-video-player-design-template-for-laptop-vector-id1217988423?k=20&m=1217988423&s=612x612&w=0&h=PR7L7GrPienKTe1-eXPf-Yenm0ATDPwJeOc82ZTygbA='}
                        posterResizeMode={'cover'}
                        controls={true}
                        paused={true}
                    />

                </View>
                <View>
                    <Text style={{ fontSize: 15, paddingHorizontal: 50, paddingVertical: 20, textAlign: 'center', fontWeight: "bold", color: '#000', top: 20 }}>{'“Cross country skiing is great if you live in a small country.” – Steven Wright'}</Text>
                </View>
                <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row', backgroundColor: '#fff', paddingVertical: 20, paddingHorizontal: 150 }}>
                    <Icon type={'Entypo'} name={"star"} style={styles.iconStyle} />
                    <Icon type={'Entypo'} name={"star"} style={styles.iconStyle} />
                    <Icon type={'Entypo'} name={"star"} style={styles.iconStyle} />
                </View>
                {/* ---------------------- ModalDropdown start------------------- */}
                {/* <View style={{ flexDirection: 'row', backgroundColor: '#fff', marginHorizontal: 80, height: 60,marginVertical: 20, borderWidth: 1 }}>
                    <ModalDropdown
                        ref={el => this._digitdropdown = el}
                        options={['Beginner Level', 'Intermidiate Level', 'Expert Level']}
                        style={{}}
                        textStyle={{ fontSize: 20, color: '#000', marginLeft: 40, width: 150, top: 10 }}
                        dropdownStyle={{ width: 230, marginLeft: 10, marginTop: 25 }}
                        dropdownTextStyle={{ fontSize: 17, textAlign: 'center',color: '#000'}}
                        defaultTextStyle={{ fontSize: 20, color: '#000', marginLeft: 40 }}
                        defaultValue={'Select Level '}
                    // isFullWidth={true}
                    />
                    <TouchableOpacity
                        onPress={() => { this._digitdropdown && this._digitdropdown.show(); }}>
                        <Icon type={'Entypo'} name={"chevron-down"} style={{ fontSize: 35, color: 'gray', paddingTop: 10}} />
                    </TouchableOpacity>
                </View> */}
                {/* ---------------------- ModalDropdown End------------------- */}
                <View style={{ width: 410, paddingHorizontal: 80, marginTop: 20 }}>
                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#D73951', height: 60, borderRadius: 20 }} onPress={() => this.AskAssestment()}>
                        <Text style={{ color: '#fff', fontSize: 20 }}>{"Ask Assestment"}</Text>
                    </TouchableOpacity>
                </View>

                <Modal
                    animationIn="slideInLeft"
                    animationOut="slideOutLeft"
                    animationInTiming={500}
                    animationOutTiming={500}
                    isVisible={this.state.showExitAppAlert}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>

                        <View style={{ width: '70%', backgroundColor: 'white', borderRadius: 5 }}>
                            <View style={{ justifyContent: 'center', flexDirection: 'row', backgroundColor: '#fff', paddingVertical: 20 }}>
                                <Icon type={'Entypo'} name={"star"} style={styles.iconStyle} />
                                <Icon type={'Entypo'} name={"star"} style={styles.iconStyle} />
                                <Icon type={'Entypo'} name={"star"} style={styles.iconStyle} />
                            </View>
                            {/* <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 15, color: '#6c757d', }}>{'Comment'}</Text> */}
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <View
                                    style={[{
                                        backgroundColor: '#fff',
                                        // alignSelf: 'left',
                                        // right:70,
                                        borderWidth: 1,
                                        borderColor: "gray",
                                        // height: 50,
                                        borderRadius: 5,
                                        width: '80%',
                                        // alignSelf: 'center',
                                        marginVertical: 7,
                                    }, { marginTop: 1 }]}>

                                    <TextInput
                                        style={{
                                            height: 40,
                                            // fontFamily: "TIMES",
                                            fontSize: 16,
                                            color: "#000",
                                            paddingLeft: 15,
                                        }}
                                        placeholder={"Please enter Comment"}
                                        placeholderTextColor={'#7E7E7E'}
                                        //placeholderStyle={}
                                        textAlignVertical='center'
                                        //multiline={true}
                                        returnKeyType={"next"}
                                        ref="Comment"
                                        //value={this.state.EmailId} 
                                        onChangeText={(Comment) => this.setState({ Comment: Comment })}
                                    // allowFontScaling={false} 
                                    />
                                </View>
                                <View style={{ paddingVertical: 10 }}>
                                    <TouchableOpacity style={{ backgroundColor: '#D73951', justifyContent: 'center', height: 40, width: 150, borderRadius: 10 }}
                                        onPress={() => this.Hidepopup()}
                                    >
                                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{"Submit"}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* </View> */}
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 30,
        // fontWeight: '900',
        color: '#D73951',
        marginTop: 10,
        fontWeight: "bold"
        // marginLeft:30
        //  textAlign: 'left'
    },

    modalDropdownStyle: {
        width: 60,
        height: 200,
    },
    modalDropDownText: {
        fontSize: 16,
        padding: 10,
        // fontFamily: 'OpenSans-Semibold',
        color: '#212529',
    },
    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
})