import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, SafeAreaView, FlatList, ActivityIndicator, RefreshControl, Dimensions, BackHandler, Pressable, Linking } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import Drawer from '../Style/Drawer';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import Toast from 'react-native-simple-toast';
import Modal from "react-native-modal";
import CommonActivityIndicator from '../Util/ActivityIndicator';
import Api from "../constant/Api";
import I18n from '../common/localization/index';
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";

// const { width } = Dimensions.get("window");
// const height = width * 0.6;
var data = [
    {
        name: "Deer",
        about: "Video Description",
        videoUrl: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-fast.mp4'
    },
    {
        name: "Spider",
        about: "Video Description ",
        videoUrl: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-eek.mp4'
    },
    {
        name: "Snail",
        about: "Video Description",
        videoUrl: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-slow.mp4'
    },
    {
        name: "Cat",
        about: "Video Description",
        videoUrl: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-cute.mp4'
    },
]

export default class VideoListScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showDrawer: false,
            connection_status: false,
            activityIndicator: true,
            showExitAppAlert1: false,
            refreshing: false,
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount = () => {
        // this.willBlur = this.props.navigation.addListener("willBlur", payload =>
        // 	BackHandler.removeEventListener("hardwareBackPress", this.handleBackButtonClick()),
        // );
        this.checkPermission();
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
            onRegister: function (token) {
                console.log("TOKEN:", token);
            },
            onNotification: function (notification) {
                console.log("NOTIFICATION:", notification);
                //   notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
            onAction: function (notification) {
                console.log("ACTION:", notification.action);
                console.log("NOTIFICATION:", notification);
            },
            onRegistrationError: function (err) {
                console.error(err.message, err);
            },
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },

            popInitialNotification: true,
            requestPermissions: true,
        });

        
        //To get the network state once
        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data.userId, getuserData: data, Token: data.userToken })
                this.HeaderApi(data);
                this.AskchallengrApi(data);
            }
        })

        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });
                //call API

            } else {
                this.setState({ connection_status: false });
            }
        });

    }

    async checkPermission() {
        console.log("in checkpermission")
        const enabled = await messaging().hasPermission();
        console.log("in checkpermission after enabled", enabled)
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async getToken() {
        console.log("in getToken")
        // if (!fcmToken) {
        console.log("check fcmToken")
        fcmToken = await messaging().getToken();

        // if (fcmToken) {
        console.log("check fcmToken", fcmToken)
        // }
        // }
    }

    async requestPermission() {
        console.log("request permission call")
        try {
            await messaging().requestPermission();
            this.getToken();
        } catch (error) {

        }
    }

    componentDidUpdate(prevProps) {
        const prevParam = prevProps.navigation.state.params;
        const currentParam = this.props.navigation.state.params;
        if (JSON.stringify(prevParam) != JSON.stringify(currentParam)) {
            this.mountComponent();
        }
    }

    mountComponent() {
        Util.getAsyncStorage('userData').then((data) => {
            // console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data.userId, getuserData: data, Token: data.userToken })
                this.HeaderApi(data);
                this.AskchallengrApi(data);
            }
        })
    }

    UNSAFE_componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    // componentWillUnmount() {
    //     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    // }
    HeaderApi = (data) => {
        // if (this.state.connection_status) {
        // var url = 'http://139.162.159.9/snowboard/api/crowntrophydetails'
        var url = Api.baseUrl + 'crowntrophydetails'

        fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': "Bearer " + data.userToken,
                'Content-Type': 'application/json',
            },
            // body: JSON.stringify(mUserData),
        })
            // .then((response) =>{
            //     // response.json()
            //     console.log("response assreqlist", response)
            // } )
            // // .then((response) => {
            // //     console.log("response assreqlist", response)
            // //     this.setState({ assreqlistdata: response.data.data_assessment_request })
            // // })
            .then((response) => response.json())
            .then((response) => {
                // console.log("response headerapi", response)
                this.setState({ headerapidata: response.data })
            })
            .catch((error) => {
                // console.log("error", error)
                this.setState({ showProgressBar: false })
                Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
            })
            .done();
        // }
        // else {
        //     Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //     this.setState({ showProgressBar: false })
        // }
    }

    AskchallengrApi = (data) => {

        // if (this.state.connection_status) {
        // console.log("this.state.Token", this.state.Token)

        var url = Api.baseUrl + 'assessment'

        // let mUserData =
        // {
        //       email: this.state.email,
        // };


        fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': "Bearer " + this.state.Token,
                'Content-Type': 'application/json',
            },
            //   body: JSON.stringify(mUserData),
        })
            .then((response) => response.json())
            .then((response) => {
                // console.log("response", response)
                this.setState({ refreshing: false, seechallengedata: response })
            })
            .catch((error) => {
                // this.setState({ showProgressBar: false })
                console.log("error", error)
                Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
            })
            .done();
        // } 
        // else {
        //   Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //   this.setState({ showProgressBar: false })
        // }
    }

    handleBackButtonClick() {
        // this.props.navigation.replace('VideoListScreen')
        this.setState({ showExitAppAlert1: true })
        return true;
    }

    _onRefresh = () => {
        { console.log("In refresh") }
        this.setState({ refreshing: true });
        this.AskchallengrApi();
        { console.log("after api calling refresh", this.state.refreshing) }
        this.setState({ refreshing: false });
    }
    clickHamburgerMenu = () => {
        this.setState({ showDrawer: !this.state.showDrawer })
    }


    render() {
        // const { seechallengedata } = this.state
        // console.log("seechallengedata", seechallengedata)
        return (
            <SafeAreaView style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
                {/* <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }> */}
                <Container>
                    <View style={{ flex: 1 }}>

                        {/* {console.log("this.state.headerapidata in renderr", this.state.headerapidata)} */}
                        <View style={{
                            width: '100%', justifyContent: 'space-between',
                            flexDirection: 'row', backgroundColor: '#e3e6f0', paddingVertical: 12, paddingHorizontal: 15, borderBottomWidth: 1
                        }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("MyAccountScreen")}>
                                <Icon type={'FontAwesome'} name={"user-circle"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                            </TouchableOpacity>
                            {this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.headerapidata.crown !== "false" && this.state.headerapidata.crown !== null ?
                                <Icon type={'MaterialCommunityIcons'} name={"crown"} style={[styles.iconStyle, { paddingTop: 4, color: this.state.headerapidata.crown }]} />
                                :
                                <Icon type={'MaterialCommunityIcons'} name={"crown"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                            }
                            {this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.headerapidata.thropy !== "false" && this.state.headerapidata.thropy !== null ?
                                <Icon type={'FontAwesome'} name={"trophy"} style={[styles.iconStyle, { paddingTop: 4, color: this.state.headerapidata.thropy }]} />
                                :
                                <Icon type={'FontAwesome'} name={"trophy"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                            }
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('CreditPlanScreen')}>
                                {this.state.headerapidata !== undefined && this.state.headerapidata !== "" &&
                                    <Text style={{ fontSize: 15 }}>{this.state.headerapidata.credit}</Text>
                                }
                                <Icon type={'FontAwesome'} name={"copyright"} style={styles.creditlogoStyle} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.clickHamburgerMenu()}>
                                <Icon type={'FontAwesome'} name={"bars"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                            </TouchableOpacity>
                        </View>


                        <ScrollView
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            {this.state.seechallengedata !== "" && this.state.seechallengedata !== undefined ?

                                <View style={{ top: 12 }}>
                                    <View style={{ width: '100%', paddingTop: 20, paddingHorizontal: 25 }}>
                                        <View style={{ backgroundColor: '#FFFFFF', borderWidth: 1, height: 120, borderRadius: 10, paddingHorizontal: 25, paddingVertical: 12 }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                                <View>
                                                    {Object.keys(this.state.seechallengedata.data).map((xyz) => {
                                                        return xyz === "white" && <Text style={{ color: "#696969", fontSize: 20, fontWeight: 'bold' }}>{I18n.t('Level1')}</Text>
                                                    })}

                                                    {/* <Text style={{ color: "#696969", fontSize: 20, fontWeight: 'bold' }}>{"White :"}</Text> */}
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 20, color: '#696969', paddingTop: 5 }} />
                                                    <Text style={{ color: "#696969", fontSize: 20, paddingLeft: 5 }}>
                                                        {this.state.seechallengedata.data.white.accpted_self_assessment}{"/"}{this.state.seechallengedata.data.white.total_self_assessment}
                                                    </Text>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'MaterialCommunityIcons'} name={"star"} style={{ fontSize: 20, color: '#696969', paddingTop: 5 }} />
                                                    <Text style={{ color: "#696969", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.white.total_stars}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: '70%', paddingTop: 15, marginLeft: 105 }}>
                                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 40, borderRadius: 10 }}
                                                    onPress={() => this.props.navigation.navigate('ChallengeScreen', {
                                                        param_level: "white"
                                                    })}>
                                                    <Text style={{ color: "white", fontSize: 20, fontFamily: 'Montserrat-Regular' }}>{I18n.t("seethechallenge")}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>

                                    <View style={{ width: '100%', paddingTop: 20, paddingHorizontal: 25 }}>
                                        <View style={{ backgroundColor: '#32CD32', borderWidth: 1, height: 120, borderRadius: 10, paddingHorizontal: 25, paddingVertical: 12 }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                                <View>
                                                    {Object.keys(this.state.seechallengedata.data).map((xyz) => {
                                                        return xyz === "green" && <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold' }}>{I18n.t('Level2')}</Text>
                                                    })}
                                                    {/* <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold' }}>{"Green :"}</Text> */}
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 20, color: '#fff', paddingTop: 5 }} />
                                                    <Text style={{ color: "#fff", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.green.accpted_self_assessment}{"/"}{this.state.seechallengedata.data.green.total_self_assessment}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'MaterialCommunityIcons'} name={"star"} style={{ fontSize: 20, color: '#fff', paddingTop: 5 }} />
                                                    <Text style={{ color: "#fff", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.green.total_stars}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: '70%', paddingTop: 15, marginLeft: 105 }}>
                                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 40, borderRadius: 10 }}
                                                    onPress={() => this.props.navigation.navigate('ChallengeScreen', {
                                                        param_level: "green"
                                                    })}>
                                                    <Text style={{ color: "white", fontSize: 20, fontFamily: 'Montserrat-Regular' }}>{I18n.t("seethechallenge")}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>


                                    <View style={{ width: '100%', paddingTop: 20, paddingHorizontal: 25 }}>
                                        <View style={{ backgroundColor: '#0000FF', borderWidth: 1, height: 120, borderRadius: 10, paddingHorizontal: 25, paddingVertical: 12 }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                                <View>
                                                    {Object.keys(this.state.seechallengedata.data).map((xyz) => {
                                                        return xyz === "blue" && <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold' }}>{I18n.t('Level3')}</Text>
                                                    })}
                                                    {/* <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold' }}>{"Blue :"}</Text> */}
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 20, color: '#fff', paddingTop: 5 }} />
                                                    <Text style={{ color: "#fff", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.blue.accpted_self_assessment}{"/"}{this.state.seechallengedata.data.blue.total_self_assessment}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'MaterialCommunityIcons'} name={"star"} style={{ fontSize: 20, color: '#fff', paddingTop: 5 }} />
                                                    <Text style={{ color: "#fff", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.blue.total_stars}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: '70%', paddingTop: 15, marginLeft: 105 }}>
                                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 40, borderRadius: 10 }}
                                                    onPress={() => this.props.navigation.navigate('ChallengeScreen', {
                                                        param_level: "blue"
                                                    })}>
                                                    <Text style={{ color: "white", fontSize: 20, fontFamily: 'Montserrat-Regular', paddingHorizontal: 5 }}>{I18n.t("seethechallenge")}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>


                                    <View style={{ width: '100%', paddingTop: 20, paddingHorizontal: 25 }}>
                                        <View style={{ backgroundColor: '#FF0000', borderWidth: 1, height: 120, borderRadius: 10, paddingHorizontal: 25, paddingVertical: 12 }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                                <View>
                                                    {Object.keys(this.state.seechallengedata.data).map((xyz) => {
                                                        return xyz === "red" && <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold' }}>{I18n.t('Level4')}</Text>
                                                    })}
                                                    {/* <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold' }}>{"Red :"}</Text> */}
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 20, color: '#fff', paddingTop: 5 }} />
                                                    <Text style={{ color: "#fff", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.red.accpted_self_assessment}{"/"}{this.state.seechallengedata.data.red.total_self_assessment}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'MaterialCommunityIcons'} name={"star"} style={{ fontSize: 20, color: '#fff', paddingTop: 5 }} />
                                                    <Text style={{ color: "#fff", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.red.total_stars}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: '70%', paddingTop: 15, marginLeft: 105 }}>
                                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 40, borderRadius: 10 }}
                                                    onPress={() => this.props.navigation.navigate('ChallengeScreen', {
                                                        param_level: "red"
                                                    })}>
                                                    <Text style={{ color: "white", fontSize: 20, fontFamily: 'Montserrat-Regular' }}>{I18n.t("seethechallenge")}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>



                                    <View style={{ width: '100%', paddingTop: 20, paddingHorizontal: 25, marginBottom: 20 }}>
                                        <View style={{ backgroundColor: '#000000', borderWidth: 1, height: 120, borderRadius: 10, paddingHorizontal: 25, paddingVertical: 10 }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                                <View>
                                                    {Object.keys(this.state.seechallengedata.data).map((xyz) => {
                                                        return xyz === "black" && <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold' }}>{I18n.t('Level5')}</Text>
                                                    })}
                                                    {/* <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold' }}>{"Black :"}</Text> */}
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 20, color: '#fff', paddingTop: 5 }} />
                                                    <Text style={{ color: "#fff", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.black.accpted_self_assessment}{"/"}{this.state.seechallengedata.data.black.total_self_assessment}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon type={'MaterialCommunityIcons'} name={"star"} style={{ fontSize: 20, color: '#fff', paddingTop: 5 }} />
                                                    <Text style={{ color: "#fff", fontSize: 20, paddingLeft: 5 }}>{this.state.seechallengedata.data.black.total_stars}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: '70%', paddingTop: 15, marginLeft: 105 }}>
                                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 40, borderRadius: 10 }}
                                                    onPress={() => this.props.navigation.navigate('ChallengeScreen', {
                                                        param_level: "black"
                                                    })}>
                                                    <Text style={{ color: "white", fontSize: 20, fontFamily: 'Montserrat-Regular' }}>{I18n.t("seethechallenge")}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>

                                    <View style={{ width: '100%', paddingHorizontal: 25, marginBottom: 50 }}>

                                        <View style={{ backgroundColor: 'yellow', borderWidth: 1, height: 120, borderRadius: 10, alignItems: 'center', justifyContent: 'center', }}>
                                            <Text style={{ color: "#000", paddingBottom: 15, fontFamily: 'Montserrat-Regular' }}>{I18n.t("takelesson")}</Text>
                                            <View style={{ width: '63%', marginLeft: 110 }}>
                                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 40, borderRadius: 10 }}
                                                    onPress={() => Linking.openURL('https://swissrideactivity.ch/fr?ref=cf19cace-f336-4996-bc51-eeace68e8a6b')}
                                                >
                                                    <Text style={{ color: "white", fontSize: 20, fontFamily: 'Montserrat-Regular' }}>{I18n.t("booklesson")}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>

                                </View>
                                :
                                <CommonActivityIndicator></CommonActivityIndicator>
                            }
                        </ScrollView>

                        {/* <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row', backgroundColor: '#e3e6f0', paddingVertical: 10, paddingHorizontal: 15, borderTopWidth: 1 }}>
                            <Icon type={'Entypo'} name={"menu"} style={styles.iconStyle} />
                            <Icon type={'Entypo'} name={"menu"} style={styles.iconStyle} />
                            <Icon type={'Entypo'} name={"menu"} style={styles.iconStyle} />
                        </View> */}

                    </View>
                    <Modal
                        animationIn="slideInLeft"
                        animationOut="slideOutLeft"
                        animationInTiming={500}
                        animationOutTiming={500}
                        isVisible={this.state.showExitAppAlert1}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>

                            <View style={{ width: '100%', backgroundColor: 'white', borderRadius: 5 }}>

                                <Text
                                    style={{
                                        fontFamily: 'Montserrat-Regular', fontSize: 18, color: 'black',
                                        marginTop: 15, marginLeft: 20, textAlign: 'left', marginRight: 5
                                    }}>
                                    {'Exit app'}
                                </Text>

                                <Text
                                    style={{
                                        fontFamily: 'Montserrat-Regular', fontSize: 18, color: 'black',
                                        marginTop: 15, marginLeft: 20, textAlign: 'left'
                                    }}>
                                    {'Do you want to exit the App?'}
                                </Text>

                                <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 15, justifyContent: 'flex-end' }}>
                                    <Pressable
                                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                        onPress={() => {
                                            this.setState({ showExitAppAlert1: false })
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: 'Montserrat-Regular', fontSize: 16, color: 'black',
                                                marginRight: 30,
                                            }}>
                                            {'No'}
                                        </Text>
                                    </Pressable>

                                    <Pressable
                                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                        onPress={() => {
                                            this.setState({ showExitAppAlert1: false })
                                            // this.props.navigation.navigate('CreditPlanScreen')
                                            BackHandler.exitApp();
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: 'Montserrat-Regular', fontSize: 16, color: 'black',
                                                marginRight: 30,
                                            }}>
                                            {'Yes'}
                                        </Text>
                                    </Pressable>
                                </View>
                            </View>
                        </View>
                    </Modal>

                </Container>
                {this.state.showDrawer ?
                    <Drawer global_navigation={this.props.navigation} data={this} />
                    : null
                }
                {/* </ScrollView> */}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    // profileContainer: {
    //     flexDirection: "row",
    //     alignItems: "center",
    //     marginTop: 8,
    //     marginLeft: 6,
    //     marginBottom: 8
    // },
    // profileImage: {
    //     width: 150,
    //     height: 100,
    //     borderRadius: 10,
    //     marginLeft: 6
    // },
    // profileName: {
    //     marginLeft: 6,
    //     fontSize: 20,
    //     color: '#181818'
    // },
    // msgText: {
    //     marginLeft: 6,
    //     fontSize: 15,
    //     color: '#181818'
    // },
    // userRow: {
    //     borderBottomWidth: StyleSheet.hairlineWidth,
    //     borderBottomColor: '#171717'
    // },
    // topViewStyle: {
    //     alignSelf: 'center',
    //     backgroundColor: '#000',
    //     width: 50,
    //     height: 5,
    //     borderRadius: 25,
    //     marginTop: 20
    // },
    // backgroundVideo: {
    //     position: 'absolute',
    //     top: 0,
    //     left: 0,
    //     bottom: 0,
    //     right: 0,
    // },
    // videostyle: {
    //     width: 150,
    //     height: 100,
    //     borderRadius: 10,
    //     marginLeft: 6
    // }
    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
    creditlogoStyle: {
        fontSize: 20,
        color: '#696969',
        // marginLeft: 25
    },
})
