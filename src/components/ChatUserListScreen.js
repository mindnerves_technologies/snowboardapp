import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");

// const { width } = Dimensions.get("window");
// const height = width * 0.6;
var data = [
    {
        name: "Rahul",
        about: "Hello",
        imgUrl: "https://picsum.photos/id/11/200/300"
    },
    {
        name: "Ganesh",
        about: "I'm good ",
        imgUrl: "https://picsum.photos/id/10/200/300"
    },
    {
        name: "Raj",
        about: "Let it be..",
        imgUrl: "https://picsum.photos/id/12/200/300"
    },
    {
        name: "Romio",
        about: "good!",
        imgUrl: "https://picsum.photos/id/11/200/300"
    },
    {
        name: "John",
        about: "no worry! ",
        imgUrl: "https://picsum.photos/id/10/200/300"
    },
    {
        name: "Siddharth",
        about: "let's begin...",
        imgUrl: "https://picsum.photos/id/12/200/300"
    }
]

export default class ChatUserListScreen extends React.Component {
    state = {
        curTime: null,
    }

    componentDidMount() {
        setInterval(() => {
            this.setState({
                curTime: new Date().toLocaleString()
            })
        }, 1000)
    }
    _renderBannerItem({ item, index }) {
        // { console.log("item", item) }
        return (
            <View>
                <TouchableOpacity style={styles.userRow}
                    // onPress={() => this.props.navigation.navigate("ChatScreen",{Username:item.name})}
                    onPress={() => this.props.navigation.navigate("ChatScreen")}
                >
                    <View style={styles.profileContainer}>
                        <Image source={{ uri: item.imgUrl }} style={styles.profileImage} />
                        <View style={{ flex: 1, paddingLeft: 10 }}>
                            <Text style={[styles.profileName, { fontWeight: "normal" }]}>{item.name}</Text>
                            <Text style={[styles.msgText, { fontWeight: "normal" }]}>{item.about}</Text>
                            {/* <Text>{this.state.curTime}</Text> */}
                        </View>
                        <Text style={{ marginRight: 15, fontSize: 13, color: 'green' }}>{'7.32 am'}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (

            <Container>
                <View style={{ flex: 1, }}>
                    <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: '#e3e6f0' }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#D73951', marginLeft: 10 }}>{"Messaging"}</Text>
                        <Icon type={'FontAwesome'} name={'user-circle'} style={{ fontSize: 25, color: '#D73951', marginLeft: 70, paddingTop: 8 }} />
                        <Text style={{ color: '#D73951', fontSize: 20, paddingTop: 8, paddingLeft: 10 }}>{'6'}</Text>
                        <Text style={{ color: '#D73951', fontSize: 20, paddingTop: 8, paddingLeft: 5 }}>{'people'}</Text>
                    </View>
                    <FlatList
                        data={data}
                        renderItem={this._renderBannerItem.bind(this)}
                        keyExtractor={(item, index) => index.toString()}
                        extraData={this.state}
                        ListEmptyComponent={this.ListEmptyComponent}
                    />
                    {/* <TouchableOpacity style={styles.userRow}>
                        <View style={styles.profileContainer}>
                            <Image source={{ uri: 'https://picsum.photos/id/11/200/300' }} style={styles.profileImage} />
                            <View style={{ flex: 1, paddingLeft: 10 }}>
                                <Text style={[styles.profileName, { fontWeight: "normal" }]}>{"item.name"}</Text>
                                <Text style={[styles.msgText, { fontWeight: "normal" }]}>{"item.about"}</Text>
                            </View>
                        </View>
                    </TouchableOpacity> */}

                </View>
            </Container>

        )
    }
}

const styles = StyleSheet.create({
    profileContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 8,
        marginLeft: 6,
        marginBottom: 8
    },
    profileImage: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginLeft: 6
    },
    profileName: {
        marginLeft: 6,
        fontSize: 16,
        color: '#181818'
    },
    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },
    userRow: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#171717'
    },
    topViewStyle: {
        alignSelf: 'center',
        backgroundColor: '#000',
        width: 50,
        height: 5,
        borderRadius: 25,
        marginTop: 20
    },
})
