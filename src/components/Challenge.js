import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, BackHandler, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import VideoPlayer from 'react-native-video-player';
import Toast from 'react-native-simple-toast';
import * as ImagePicker from 'react-native-image-picker';
import Drawer from '../Style/Drawer';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import CommonActivityIndicator from '../Util/ActivityIndicator';
import Api from "../constant/Api";
import I18n from '../common/localization/index';
// import fonts from '../assets/fonts';

// var data = [
//     {
//         name: "Challenge1",
//         about: "Here’s your one-stop shop for snowboard Team Challenge highlights from 2018..",
//         videoUrl: 'https://media.istockphoto.com/videos/snowboarding-fresh-snow-turn-video-id500241549'
//     },
//     {
//         name: "Challenge2",
//         about: "Snowboard brands Salomon, Burton, Nitro, Rome SDS, Capita, and DC all hand,Nitro.",
//         videoUrl: 'https://media.istockphoto.com/videos/skier-spraying-snow-at-the-camera-video-id515900616'
//     },

// ]

export default class Challenge extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDrawer: false,
            // showcontrol:false,
            text: "",               // to store text input
            textMessage: false,     // text input message flag
            loading: false,         // manage loader
            image: "",              // store image
            video: "",
            connection_status: false,
            colors: 'red'
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        // {console.log("this.props.navigation.state.params.param_OID",this.props.navigation.state.params.param_level)}
    }

    componentDidMount() {
        // this.callApi();
        // this.CacheProductCallApi();
        // setTimeout(() => {
        //     this.setState({showcontrol:true})
        // }, 1000);
        // Util.getAsyncStorage('AssessmentLevelData').then((data) => {
        //     console.log("AssessmentLevelData", data)
        //     if (data !== null) {
        //         this.setState({ acceptchalengedata: data.assesment_level_Data, assesment_video_id: data.assesment_video_id, level: data.level })
        //     }
        // })
        console.log("in component did mount")
        Util.getAsyncStorage('userData').then((data) => {
            console.log("data in challenge asyncstorage", data)
            if (data !== null) {
                this.setState({ userId: data.userId, getuserData: data, userToken: data.userToken })
                this.challengApi(data);
                this.HeaderApi(data);
            }
        })


        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });
                //call API

            } else {
                this.setState({ connection_status: false });
            }
        });
    }

    // componentDidUpdate() {
    //     Util.getAsyncStorage('userData').then((data) => {
    //         console.log("data in challenge asyncstorage", data)
    //         if (data !== null) {
    //             this.setState({ userId: data.userId, getuserData: data, userToken: data.userToken })
    //             this.challengApi(data);
    //             this.HeaderApi(data);
    //         }
    //     })
    // }
    componentDidUpdate(prevProps) {
        const prevParam = prevProps.navigation.state.params;
        const currentParam = this.props.navigation.state.params;
        if (JSON.stringify(prevParam) != JSON.stringify(currentParam)) {
            this.mountComponent();
        }
    }

    mountComponent() {
        Util.getAsyncStorage('userData').then((data) => {
            console.log("data in challenge asyncstorage", data)
            if (data !== null) {
                this.setState({ userId: data.userId, getuserData: data, userToken: data.userToken })
                this.challengApi(data);
                this.HeaderApi(data);
            }
        })
    }

    // this.props.navigation.navigate('Cart', { fromCome: new Date().getTime() });

    UNSAFE_componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true;
    }
    HeaderApi = (data) => {
        // if (this.state.connection_status) {
        console.log("data.userToken,", data.userToken)
        var url = 'http://139.162.159.9/snowboard/api/crowntrophydetails'
        console.log("after url", data.userToken)

        fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': "Bearer " + data.userToken,
                'Content-Type': 'application/json',
            },
            // body: JSON.stringify(mUserData),
        })
            // .then((response) =>{
            //     // response.json()
            //     console.log("response assreqlist", response)
            // } )
            // // .then((response) => {
            // //     console.log("response assreqlist", response)
            // //     this.setState({ assreqlistdata: response.data.data_assessment_request })
            // // })
            .then((response) => response.json())
            .then((response) => {
                console.log("response headerapi", response)
                this.setState({ headerapidata: response.data })
            })
            .catch((error) => {
                console.log("error", error)
                this.setState({ showProgressBar: false })
                Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
            })
            .done();
        // }
        // else {
        //     Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //     this.setState({ showProgressBar: false })
        // }
    }

    challengApi = (data) => {

        if (this.state.connection_status) {

            var url = Api.baseUrl + 'assessmentlevel'


            let mUserData =
            {
                "level": this.props.navigation.state.params.param_level,
            };

            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + data.userToken,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("response challenge", response)
                    this.setState({
                        assesmentleveldata: response, challengedata: response.data,
                        level: response.data[0]?.level
                    })
                })
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }

    clickHamburgerMenu = () => {
        this.setState({ showDrawer: !this.state.showDrawer })
    }
    getRequestDetails = (item) => {
        // {console.log("item in getrequestDetails api",item)}
        this.getRequestDetailsApi(item);
        return true
    };

    getRequestDetailsApi = (item) => {
        { console.log("item in api", item.level + "    " + item.id) }
        { console.log("item in api", item) }
        { console.log("this.state.userToken", this.state.userToken) }
        if (this.state.connection_status) {

            var url = Api.baseUrl + 'getrequestdetails'

            let mUserData =
            {
                "level": item.level,
                "assesment_video_id": item.id
            };

            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + this.state.userToken,
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("response accept challenge", response)
                    // this.setState({ challengedata: response.data.videos, level: response.data.videos[0].level })
                    if (response.status === 'ok') {

                        if (response.data !== null) {


                            let assessment_level_Data = {
                                assesment_level_Data: response.data,
                                level: item.level,
                                assesment_video_id: item.id

                            }

                            console.log(" object", assessment_level_Data);
                            // this.setState({ activityIndicator: true })
                            AsyncStorage.setItem('AssessmentLevelData', JSON.stringify(assessment_level_Data))
                            // Toast.showWithGravity('Welcome To Snowboard App'+"\n\t\t\t\t\t\t\t\t\t\t\t\t\t🙏🙏", Toast.LONG, Toast.CENTER)
                            this.props.navigation.navigate('UserassesmentScreen')

                            //this.props.navigation.goBack(null);

                        } else {
                            Toast.showWithGravity("Response data is empty", Toast.SHORT, Toast.CENTER);
                            this.setState({ activityIndicator: true })
                        }


                    }
                    else {
                        this.setState({ activityIndicator: true })
                        Toast.showWithGravity('Response Failure', Toast.SHORT, Toast.CENTER);
                    }
                })
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }

    selectVideo = async () => {
        ImagePicker.launchImageLibrary({ mediaType: 'video', includeBase64: true }, (response) => {
            // console.log("response", response);
            this.setState({ video: response });
        })
    }
    selectVideo1 = async () => {
        ImagePicker.launchImageLibrary({ mediaType: 'video', includeBase64: true }, (response) => {
            // console.log("response", response);
            this.setState({ video1: response });
        })
    }

    starhighlight = (item) => {
        console.log("inside anonymous function", item)
        if (item?.adminstar == null || item?.adminstar == "") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, marginLeft: 30 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3,paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.adminstar == "0") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, marginLeft: 30 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3,paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.adminstar == "1") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'yellow', paddingTop: 3, marginLeft: 30}} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3,paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.adminstar == "2") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'yellow', paddingTop: 3, marginLeft: 30}} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'yellow', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'gray', paddingTop: 3,paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.adminstar == "3") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'yellow', paddingTop: 3, marginLeft: 30 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'yellow', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 18, color: 'yellow', paddingTop: 3,paddingLeft: 5 }} />
                </View>
            )
        }

        return true
    };

    snowflakehighlight = (item) => {
        console.log("inside anonymous function", item)
        if (item?.snowflex == null || item?.snowflex == "") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3,paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.snowflex == "0") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3,paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.snowflex == "1") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'skyblue', paddingTop: 3,paddingLeft: 5   }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.snowflex == "2") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'skyblue', paddingTop: 3,paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'skyblue', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'gray', paddingTop: 3, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.snowflex == "3") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'skyblue', paddingTop: 3,paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'skyblue', paddingTop: 3, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 18, color: 'skyblue', paddingTop: 3, paddingLeft: 5 }} />
                </View>
            )
        }

        return true
    };

    professionalassesmentstatus= (item) => {
        console.log("professionalassesmentstatus", item)
        // if (item?.assessment_status == null) {
        //     return (
        //         <View style={{}}>
        //             <Text style={{ fontSize: 15, color: '#6c757d',maxWidth:"70%",color: 'green' }}>{"Professional assessment request not send"}</Text>
        //         </View>
        //     )
        // }

        if (item?.assessment_status == "pending") {
            return (
                <View style={{}}>
                    <Text style={{ fontSize: 15, color: '#6c757d',maxWidth:"70%",color: 'green' }}>{I18n.t("Professionalassessmentpending")}</Text>
                </View>
            )
        }
        
        // if (item?.assessment_status == "assess") {
        //     return (
        //         <View style={{}}>
        //             <Text style={{ fontSize: 15, color: '#6c757d',color: 'green',maxWidth:"70%" }}>{"Professional assessment request completed"}</Text>
        //         </View>
        //     )
        // }
        
        return true
    };


    coachingstatus= (item) => {
        console.log("coachingstatus", item)
        
        // if (item?.coaching_status == null) {
        //     return (
        //         <View style={{}}>
        //             <Text style={{ fontSize: 15, color: '#D73951',maxWidth:"70%"}}>{"Coaching request not send"}</Text>
        //         </View>
        //     )
        // }

        if (item?.coaching_status == "pending") {
            return (
                <View style={{}}>
                    <Text style={{ fontSize: 15, color: '#D73951',maxWidth:"70%"}}>{I18n.t("coachingpending")}</Text>
                </View>
            )
        }
        
        if (item?.coaching_status == "accept") {
            return (
                <View style={{}}>
                    <Text style={{ fontSize: 15, color: '#D73951',maxWidth:"70%" }}>{I18n.t("coachinginprogress")}</Text>
                </View>
            )
        }
        // if (item?.coaching_status == "completed") {
        //     return (
        //         <View style={{}}>
        //             <Text style={{ fontSize: 15, color: '#D73951',maxWidth:"70%" }}>{"Coaching request completed"}</Text>
        //         </View>
        //     )
        // }
        
        return true
    };

    _renderBannerItem({ item, index }) {
        { console.log("item", item) }
        const { video } = this.state;
        return (
            <View>
                <View style={styles.userRow}>
                    <View style={{ paddingHorizontal: 10, flexDirection: 'row' }}>
                        {/* <Image source={{ uri: 'https://picsum.photos/id/11/200/300' }} style={styles.profileImage} /> */}
                        <View>
                            {/* {video ? */}
                            {/* <Video source={{ uri: video.assets[0].uri }}
                                    style={styles.videostyle}
                                    repeat={true}
                                    ref={(ref) => {
                                        this.player = ref
                                    }}
                                    resizeMode='cover'
                                    onBuffer={this.onBuffer}
                                    onError={this.videoError}
                                    poster={'https://media.istockphoto.com/vectors/illustration-of-simple-black-video-player-design-template-for-laptop-vector-id1217988423?k=20&m=1217988423&s=612x612&w=0&h=PR7L7GrPienKTe1-eXPf-Yenm0ATDPwJeOc82ZTygbA='}
                                    posterResizeMode={'cover'}
                                    controls={true}
                                    // playInBackground={false}
                                    // autoplay={false}
                                    paused={true}
                                /> */}
                            {/* : */}
                            {/* {console.log("video url",item.url)} */}
                            <Video source={{ uri: "http://139.162.159.9/snowboard/" + item.url }}
                                style={styles.videostyle}
                                repeat={true}
                                ref={(ref) => {
                                    this.player = ref
                                }}
                                resizeMode='cover'
                                onBuffer={this.onBuffer}
                                onError={this.videoError}
                                poster={'https://media.istockphoto.com/vectors/illustration-of-simple-black-video-player-design-template-for-laptop-vector-id1217988423?k=20&m=1217988423&s=612x612&w=0&h=PR7L7GrPienKTe1-eXPf-Yenm0ATDPwJeOc82ZTygbA='}
                                posterResizeMode={'cover'}
                                controls={true}
                                // playInBackground={false}
                                // autoplay={false}
                                paused={true}
                            />
                            {/* } */}

                            {/* <TouchableOpacity style={{ marginLeft: 40, marginTop: 225 }} onPress={() => this.props.navigation.navigate("RequestAssesmentScreen")}>
                                        <Text style={{ color: "blue", textAlign: 'left', fontFamily: 'Montserrat-Regular', textDecorationLine: 'underline', fontSize: 17 }}>{"Assessment"}</Text>
                                    </TouchableOpacity> */}
                        </View>
                        {/* <TouchableOpacity style={{ marginLeft: 50, marginTop: 150, justifyContent: 'center', height: 50, width: 105, borderRadius: 10 }} onPress={() => this.props.navigation.navigate("RequestAssesmentScreen")}>
                                <Text style={{ color: "blue", textAlign: 'center', fontFamily: 'Montserrat-Regular', textDecorationLine: 'underline', fontSize: 17 }}>{"Assesment"}</Text>
                            </TouchableOpacity> */}
                        <View style={{ paddingHorizontal: 15, marginLeft: 15,}}>
                            <View style={{ height: 170 }}>
                                <Text style={{ fontSize: 25, color: '#6c757d', width: 180, fontFamily: 'Montserrat-SemiBold' }}>{item.title}</Text>
                                {console.log("this.state.assesmentleveldata", this.state.assesmentleveldata)}
                                <View style={{flexDirection:'row'}}>
                                {
                                    this.snowflakehighlight(item)
                                }
                                {
                                    this.starhighlight(item)
                                }
                                
                                </View>

                                <Text style={{ fontSize: 20, color: '#6c757d', paddingTop: 5, width: 170, fontFamily: 'Montserrat-Regular' }}>{item.difficulty}</Text>
                                <Text style={{ fontSize: 15, color: '#6c757d', paddingTop: 5, width: 180, fontFamily: 'Montserrat-Regular' }}>{item.description}</Text>
                            </View>
                            {/* {console.log("acceptchalengedata", this.state.acceptchalengedata)} */}
                            {/* {item.assessment_status=="pending"?
                                <View>
                                    <Text style={{ fontSize: 15, color: 'green', paddingTop: 2, width: 170, fontFamily: 'Montserrat-Regular' }}>{"Assessment pending by Professional "}</Text>
                                </View>
                                :
                                <View>
                                    <Text style={{ fontSize: 15, color: 'green', paddingTop: 2, width: 170, fontFamily: 'Montserrat-Regular' }}>{"Assessment assessed by professional"}</Text>
                                </View>
                            }   */}
                            <View style={{paddingTop:5,height:75}}>
                            {
                                this.professionalassesmentstatus(item)
                            }
                            </View>
                            <View style={{paddingTop:5,height:50}}>
                            {
                                this.coachingstatus(item)
                            }
                            </View>
                            
                            {/* {item.assessment_status=="pending"&&
                                 <View>
                                     <Text style={{ fontSize: 15, color: 'red', paddingTop: 2, width: 170, fontFamily: 'Montserrat-Regular' }}>{"Assessment "}{this.state.acceptchalengedata?.assesment_details?.assessment_status}</Text>
                                 </View>
                            } */}
                            {item.level_status !== "locked" ?
                                <View style={{ flexDirection: 'row', paddingTop: 10}}>
                                    <TouchableOpacity style={{ backgroundColor: '#00a0e4', justifyContent: 'center', height: 50, width: '74%', borderRadius: 10, right: 10 }}
                                        // onPress={() => this.selectVideo()}
                                        // onPress={() => this.props.navigation.navigate("UserassesmentScreen")}
                                        onPress={() => this.getRequestDetails(item)}
                                    >
                                        <Text style={{ color: "#FFF", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{I18n.t("acceptchallenge")}</Text>
                                    </TouchableOpacity>
                                    {/* <TouchableOpacity style={{ marginLeft: 10, backgroundColor: '#D73951', justifyContent: 'center', height: 50, width: 90, borderRadius: 10, right: 10 }} onPress={() => this.props.navigation.navigate("UserassesmentScreen")}>
                                            <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{"Demo"}</Text>
                                        </TouchableOpacity> */}
                                </View>
                                :
                                <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                                    <View style={{ backgroundColor: '#c0c0c0', justifyContent: 'center', height: 50, width: '74%', borderRadius: 10, right: 10 }}
                                    // onPress={() => this.selectVideo()}
                                    // onPress={() => this.props.navigation.navigate("UserassesmentScreen")}
                                    // onPress={() => this.getRequestDetails(item)}
                                    >
                                        <Text style={{ color: "#FFF", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{"Accept The Challenge"}</Text>
                                    </View>
                                    {/* <TouchableOpacity style={{ marginLeft: 10, backgroundColor: '#D73951', justifyContent: 'center', height: 50, width: 90, borderRadius: 10, right: 10 }} onPress={() => this.props.navigation.navigate("UserassesmentScreen")}>
                                            <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{"Demo"}</Text>
                                        </TouchableOpacity> */}
                                </View>
                            }
                        </View>

                    </View>
                </View>
            </View>
        )
    }



    render() {
        const { video } = this.state;
        const { video1 } = this.state;
        // { console.log("video.assets", video) }
        return (
            <Container>
                <View style={{ flex: 1, }}>
                    <View style={{
                        width: '100%', justifyContent: 'space-between',
                        flexDirection: 'row', backgroundColor: '#e3e6f0', paddingVertical: 12, paddingHorizontal: 15, borderBottomWidth: 1
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("MyAccountScreen")}>
                            <Icon type={'FontAwesome'} name={"user-circle"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        </TouchableOpacity>
                        {this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.headerapidata.crown !== "false" && this.state.headerapidata.crown !== null ?
                            <Icon type={'MaterialCommunityIcons'} name={"crown"} style={[styles.iconStyle, { paddingTop: 4, color: this.state.headerapidata.crown }]} />
                            :
                            <Icon type={'MaterialCommunityIcons'} name={"crown"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        }
                        {this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.headerapidata.thropy !== "false" && this.state.headerapidata.thropy !== null ?
                            <Icon type={'FontAwesome'} name={"trophy"} style={[styles.iconStyle, { paddingTop: 4, color: this.state.headerapidata.thropy }]} />
                            :
                            <Icon type={'FontAwesome'} name={"trophy"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        }
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CreditPlanScreen')}>
                            {this.state.headerapidata !== undefined && this.state.headerapidata !== "" &&
                                <Text style={{ fontSize: 15 }}>{this.state.headerapidata.credit}</Text>
                            }
                            <Icon type={'FontAwesome'} name={"copyright"} style={styles.creditlogoStyle} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.clickHamburgerMenu()}>
                            <Icon type={'FontAwesome'} name={"bars"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        </TouchableOpacity>
                    </View>

                    {/* <View style={{ flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 15 }}>
                    <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={styles.titleStyle}>{"Level 1"}</Text>
                    </View> */}
                    {console.log("this.state.level in challenge", this.state.level)}
                    {/* <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15 }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 10 }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={{ fontSize: 30, textTransform: 'uppercase', fontFamily: 'Montserrat-Bold', color: '#00a0e4', paddingLeft: 10 }}>{this.state.level}</Text>
                    </View> */}
                    {console.log("this.state.challengedata", this.state.challengedata)}
                    {this.state.challengedata !== "" && this.state.challengedata !== undefined ?
                        <View>
                            <ScrollView style={{marginBottom:80}}>
                            <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15 }}>
                                <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 10 }} onPress={() => this.props.navigation.navigate('VideoListScreen', { fromCome: new Date().getTime() })} />
                                <Text style={{ fontSize: 30, textTransform: 'uppercase', fontFamily: 'Montserrat-Bold', color: '#00a0e4', paddingLeft: 10 }}>{this.state.level}</Text>
                            </View>
                            <FlatList
                                data={this.state.challengedata}
                                renderItem={this._renderBannerItem.bind(this)}
                                keyExtractor={(item, index) => index.toString()}
                                extraData={this.state}
                                ListEmptyComponent={this.ListEmptyComponent}
                            />
                            </ScrollView>
                        </View>
                        :
                        <CommonActivityIndicator></CommonActivityIndicator>
                    }
                </View>
                {this.state.showDrawer ?
                    <Drawer global_navigation={this.props.navigation} data={this} />
                    : null
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    profileImage: {
        width: 180,
        height: 210,
        borderRadius: 10,
        marginLeft: 6
    },
    // profileName: {
    //     marginLeft: 6,
    //     fontSize: 20,
    //     color: '#181818'
    // },
    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },
    userRow: {
        // borderBottomWidth: StyleSheet.hairlineWidth,
        // borderBottomColor: '#171717',
        paddingVertical: 10,
        borderWidth: StyleSheet.hairlineWidth,
    },
    titleStyle: {
        fontSize: 30,
        color: '#00a0e4',
        fontFamily: 'Montserrat-Bold',
    },
    // backgroundVideo: {
    //     position: 'absolute',
    //     top: 0,
    //     left: 0,
    //     bottom: 0,
    //     right: 0,
    // },
    videostyle: {
        // width: 150,
        // height: 200,
        // // borderRadius: 10,
        // // marginLeft: 6
        width: 170,
        height: 350,
        borderRadius: 10,
        marginLeft: 6,
        // position: 'absolute',
    },

    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
    creditlogoStyle: {
        fontSize: 20,
        color: '#696969',
        // marginLeft: 25
    },

})
