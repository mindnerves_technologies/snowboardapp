import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import Drawer from '../Style/Drawer';
import NetInfo from "@react-native-community/netinfo";
import Util from '../Util/Util';
import Toast from 'react-native-simple-toast';
import CommonActivityIndicator from '../Util/ActivityIndicator';
import I18n from '../common/localization/index';
// import fonts from '../assets/fonts';

var data = [
    {
        name: "Level 1",
        about: "Self Assessment",
        about1: "Snowboarding is a winter sport that involves descending a slope that is covered with snow. ",
        value1: 'Beginner Level',
        value2: 'Expert Level',
        videoUrl: 'https://media.istockphoto.com/videos/snowboarding-fresh-snow-turn-video-id500241549'
    },
    {
        name: "Level 2",
        about: "Self Assessment",
        about1: "Snowboarding is a winter sport that involves descending a slope that is covered with snow.",
        value1: 'Expert Level',
        value2: 'Beginner Level',
        videoUrl: 'https://media.istockphoto.com/videos/skier-spraying-snow-at-the-camera-video-id515900616'
    },

]

export default class CoachingRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDrawer: false,
            connection_status: false,
        };
    }

    componentDidMount() {
        // this.callApi();
        // this.CacheProductCallApi();
        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data, getuserData: data, userToken1: data.userToken })
                this.CoachingRequestListApi(data);
            }
        })

        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });


            } else {
                this.setState({ connection_status: false });
            }
        });
    }

    CoachingRequestListApi = (data) => {
        if (this.state.connection_status) {

            var url = 'http://139.162.159.9/snowboard/api/getuserrequestdetails'

            let mUserData =
            {
                // "level": this.props.navigation.state.params.param_level,
            };

            fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': "Bearer " + data.userToken,
                    'Content-Type': 'application/json',
                },
                // body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("response coachreqlist", response)
                    this.setState({ coachreqlistdata: response.data.data_coaching_request })
                })
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }

    clickHamburgerMenu = () => {
        this.setState({ showDrawer: !this.state.showDrawer })
    }

    _renderBannerItem({ item, index }) {
        { console.log("item", item) }
        return (
            <View>
                <View style={[styles.card, styles.elevation]}>
                    <View style={{ paddingHorizontal: 10, flexDirection: 'row', paddingTop: 15, }}>
                        {/* <Image source={{ uri: 'https://picsum.photos/id/11/200/300' }} style={styles.profileImage} /> */}
                        <View>
                            <Video source={{ uri:"http://139.162.159.9/snowboard/" + item.vedio.url }}
                                style={styles.videostyle}
                                repeat={true}
                                ref={(ref) => {
                                    this.player = ref
                                }}
                                resizeMode='cover'
                                onBuffer={this.onBuffer}
                                onError={this.videoError}
                                poster={'https://media.istockphoto.com/vectors/illustration-of-simple-black-video-player-design-template-for-laptop-vector-id1217988423?k=20&m=1217988423&s=612x612&w=0&h=PR7L7GrPienKTe1-eXPf-Yenm0ATDPwJeOc82ZTygbA='}
                                posterResizeMode={'cover'}
                                controls={true}
                                // playInBackground={false}
                                // autoplay={false}
                                paused={true}
                            />
                            {/* <TouchableOpacity style={{marginLeft:40,marginTop:225}} onPress={() => this.props.navigation.navigate("RequestAssesmentScreen")}>
                                <Text style={{ color: "blue", textAlign: 'left',fontFamily: 'Montserrat-Regular', textDecorationLine: 'underline', fontSize: 17 }}>{"Assesment"}</Text>
                            </TouchableOpacity> */}
                        </View>

                        <View style={{ paddingHorizontal: 10 }}>
                            <View style={{ height: 200 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 25, color: '#181818', width: 160, fontFamily: 'Montserrat-SemiBold', textTransform: 'uppercase' }}>{item.assesment_level}</Text>
                                    {/* <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 22, color: 'gray', paddingTop: 8, paddingLeft: 5 }} /> */}
                                    {/* <Icon type={'MaterialCommunityIcons'} name={"star"} style={{ fontSize: 20, color: 'gray', paddingTop: 5, paddingLeft: 2 }} />
                                    <Icon type={'MaterialCommunityIcons'} name={"star"} style={{ fontSize: 20, color: 'gray', paddingTop: 5, paddingLeft: 2 }} /> */}
                                </View>

                                <View style={{ paddingTop: 10, maxWidth: 230, maxHeight: 100 }}>
                                    {/* <TouchableOpacity> */}
                                    <Text style={{ fontSize: 15, color: '#6c757d', fontFamily: 'Montserrat-Regular', }}>{item.vedio.description}</Text>
                                    {/* </TouchableOpacity> */}
                                    {/* <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 16, color: '#6c757d' }}>{item.value2}</Text>
                                    </View> */}

                                    {/* <View style={{ flexDirection: 'row', paddingTop: 7 }}>
                                        <Text style={{ fontSize: 15, color: '#6c757d', }}>{'Comment'}</Text>
                                        <Icon type={'MaterialCommunityIcons'} name={"comment"} style={{ fontSize: 20, color: 'gray', paddingTop: 5, paddingLeft: 15 }} />
                                    </View> */}
                                    {/* <Text style={{ fontSize: 20, color: '#6c757d', fontFamily: 'Montserrat-Regular', }}>{item.status}</Text> */}
                                    
                                        <View style={{ marginVertical: 10 }}>
                                        {item.status == "pending" &&
                                            <Text style={{ fontSize: 15, color: '#D73951', fontFamily: 'Montserrat-SemiBold', }}>{I18n.t("Coachingrequestpendingbyprofessional")}</Text>
                                        }
                                        {/* {item.status == "completed" &&
                                            <Text style={{ fontSize: 15, color: '#D73951', fontFamily: 'Montserrat-SemiBold', }}>{"Coaching request completed by professional"}</Text>
                                        } */}
                                            </View>
                                    

                                    {item.status == "accept" &&
                                        <View style={{}}>
                                            <TouchableOpacity style={{ backgroundColor: '#00a0e4', justifyContent: 'center', height: 40, width: 180, borderRadius: 10, right: 10, left: 1, top: 55 }} onPress={() => this.props.navigation.navigate("ChatScreen")}>
                                                <Text style={{ color: "#FFF", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{I18n.t("chat")}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                </View>
                            </View>

                        </View>

                    </View>
                </View>
            </View>
        )
    }



    render() {
        return (
            <Container>
                <View style={{ flex: 1, }}>
                    <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: '#e3e6f0' }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#00a0e4', paddingLeft: 10 }}>{I18n.t("coachingrequests")}</Text>
                    </View>
                    {console.log("this.state.coachreqlistdata", this.state.coachreqlistdata)}
                    {this.state.coachreqlistdata !== undefined && this.state.coachreqlistdata !== "" ?
                        <FlatList
                            data={this.state.coachreqlistdata}
                            renderItem={this._renderBannerItem.bind(this)}
                            keyExtractor={(item, index) => index.toString()}
                            extraData={this.state}
                            ListEmptyComponent={this.ListEmptyComponent}
                        />
                        :
                        <CommonActivityIndicator></CommonActivityIndicator>
                    }
                </View>
                {this.state.showDrawer ?
                    <Drawer global_navigation={this.props.navigation} data={this} />
                    : null
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    profileImage: {
        width: 180,
        height: 210,
        borderRadius: 10,
        marginLeft: 6
    },
    // profileName: {
    //     marginLeft: 6,
    //     fontSize: 20,
    //     color: '#181818'
    // },
    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },
    userRow: {
        // borderBottomWidth: StyleSheet.hairlineWidth,
        // borderBottomColor: '#171717',
        // paddingVertical: 20,
        // color:'gray',
        // borderBottomWidth: StyleSheet.hairlineWidth ,
    },
    card: {
        backgroundColor: 'white',
        // marginHorizontal:10,
        marginLeft: 8,
        // marginRight:10,
        borderRadius: 8,
        // paddingVertical: 2,
        // paddingHorizontal:5,
        width: '96%',
        // marginVertical: 10,
        marginVertical: 10,
    },
    elevation: {
        elevation: 15,
        shadowColor: '#6c757d',
    },
    titleStyle: {
        fontSize: 25,
        color: '#696969',
        fontFamily: 'Montserrat-Bold',
    },
    // backgroundVideo: {
    //     position: 'absolute',
    //     top: 0,
    //     left: 0,
    //     bottom: 0,
    //     right: 0,
    // },
    videostyle: {
        // width: 150,
        // height: 200,
        // // borderRadius: 10,
        // // marginLeft: 6
        width: 120,
        height: 190,
        borderRadius: 10,
        marginLeft: 6,
        // position: 'absolute',
    },

    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
    creditlogoStyle: {
        fontSize: 20,
        color: '#696969',
        // marginLeft: 25
    },

})
