import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import Drawer from '../Style/Drawer';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import Toast from 'react-native-simple-toast';
import Modal from "react-native-modal";
import CommonActivityIndicator from '../Util/ActivityIndicator';
import I18n from '../common/localization/index';
// import fonts from '../assets/fonts';

var data = [
    {
        name: "Super",
        about: "Installment credit comes in the form of a loan with a fixed loan amount",
        Euro: 10,
        Credit: 1000

    },
    {
        name: "Premium",
        about: "Installment credit comes in the form of a loan with a fixed loan amount",
        Euro: 90,
        Credit: 10000
    },

]

export default class CreditPlanScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDrawer: false
        };
    }

    componentDidMount() {
        // this.callApi();
        // this.CacheProductCallApi();
        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });
                //call API

            } else {
                this.setState({ connection_status: false });
            }
        });
        this.CreditplanApi();
    }

    CreditplanApi = () => {

        // if (this.state.connection_status) {

        var url = 'http://139.162.159.9/snowboard/api/credit_plans'

        let mUserData =
        {
            //   email: this.state.email,
        };


        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
            //   body: JSON.stringify(mUserData),
        })
            .then((response) => response.json())
            .then((response) => {
                console.log("response creditplandata", response)
                this.setState({ creditplandata: response.data })
            })
            .catch((error) => {
                // this.setState({ showProgressBar: false })
                Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
            })
            .done();
        // } 
        // else {
        //   Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //   this.setState({ showProgressBar: false })
        // }
    }

    clickHamburgerMenu = () => {
        this.setState({ showDrawer: !this.state.showDrawer })
    }

    _renderBannerItem({ item, index }) {
        return (
            <View>
                <ScrollView>
                    <View style={[styles.card, styles.elevation]}>
                        <View style={{ paddingHorizontal: 10, paddingTop: 15, }}>
                            <Text style={{ fontSize: 25, color: '#181818', width: 160, fontFamily: 'Montserrat-SemiBold' }}>{item.title}</Text>
                            <Text style={{ fontSize: 14, paddingVertical: 10, color: '#6c757d', fontFamily: 'Montserrat-SemiBold' }}>{item.description}</Text>
                            <View style={{ flexDirection: 'row', paddingBottom: 15 }}>
                                <Icon type={'FontAwesome'} name={"euro"} style={{ fontSize: 35, color: '#696969', paddingTop: 4 }} />
                                <Text >{" "}</Text>
                                <Text style={{ fontSize: 30, color: '#6c757d', fontFamily: 'Montserrat-SemiBold' }}>{item.amount}</Text>
                                <Text style={{ fontSize: 25, color: '#6c757d', fontFamily: 'Montserrat-SemiBold', top: 10 }}>{"/"}</Text>
                                <Text style={{ fontSize: 20, color: '#6c757d', fontFamily: 'Montserrat-SemiBold', top: 20 }}>{item.available_credits}</Text>
                                <Text style={{ fontSize: 20, color: '#6c757d', fontFamily: 'Montserrat-SemiBold', top: 20 }}>{" Credit"}</Text>
                            </View>
                            <TouchableOpacity 
                            style={{ backgroundColor: '#00a0e4', width: "60%", borderRadius: 10, marginBottom: 10 }} 
                            onPress={()=>{this.props.navigation.navigate('PaymentScreen')}}>
                                <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', paddingVertical: 5, fontSize: 16 }}>{I18n.t("buycredit")}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <View style={{ flex: 1, }}>

                    <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: '#e3e6f0' }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#00a0e4', paddingLeft: 10 }}>{I18n.t("creditplan")}</Text>
                    </View>
                    {console.log("this.state.creditplandata", this.state.creditplandata)}
                    {this.state.creditplandata !== "" && this.state.creditplandata !== undefined ?
                        <FlatList
                            data={this.state.creditplandata}
                            renderItem={this._renderBannerItem.bind(this)}
                            keyExtractor={(item, index) => index.toString()}
                            extraData={this.state}
                            ListEmptyComponent={this.ListEmptyComponent}
                        />
                        :
                        <CommonActivityIndicator></CommonActivityIndicator>
                    }
                </View>
                {this.state.showDrawer ?
                    <Drawer global_navigation={this.props.navigation} data={this} />
                    : null
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    profileImage: {
        width: 180,
        height: 210,
        borderRadius: 10,
        marginLeft: 6
    },

    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },

    card: {
        backgroundColor: 'white',
        // marginHorizontal:10,
        marginLeft: 40,
        // marginRight:10,
        borderRadius: 8,
        // paddingVertical: 2,
        // paddingHorizontal:5,
        width: '80%',
        // marginVertical: 10,
        marginVertical: 10,
    },
    elevation: {
        elevation: 15,
        shadowColor: '#6c757d',
    },
    titleStyle: {
        fontSize: 25,
        color: '#696969',
        fontFamily: 'Montserrat-Bold',
    },
    // backgroundVideo: {
    //     position: 'absolute',
    //     top: 0,
    //     left: 0,
    //     bottom: 0,
    //     right: 0,
    // },
    videostyle: {
        // width: 150,
        // height: 200,
        // // borderRadius: 10,
        // // marginLeft: 6
        width: 120,
        height: 190,
        borderRadius: 10,
        marginLeft: 6,
        // position: 'absolute',
    },

    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
    creditlogoStyle: {
        fontSize: 20,
        color: '#696969',
        // marginLeft: 25
    },

})
