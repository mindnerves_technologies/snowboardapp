import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
import Images from "../constants/Images";
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';


export default class PendingRequestScreen extends React.Component {
    state = {
    }
    render() {
        return (

            <Container>
                <View style={{ flex: 1, }}>
                    <ScrollView>
                        <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 30, backgroundColor: 'gray' }}>
                            <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#fff' }}>{"Pending Request"}</Text>
                        </View>

                        <View style={{
                            elevation: 0,
                            backgroundColor: '#f3f3f3',
                            width: this.state.deviceWidth, padding: 15, borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717'
                        }}>

                            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                <View>
                                    <Text>{'Dec'}</Text>
                                    <View style={styles.profileImage1}>
                                        <Text style={{ textAlign: 'center', marginTop: 5 }}>{'21'}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: "row", padding: 20, borderRadius: 10, backgroundColor: '#fff', }}>
                                    <Image
                                        source={Images.Profileimg}
                                        style={{ tintColor: '#000', height: 50, width: 50, marginRight: 30 }}
                                    />

                                    <View style={{ flex: 1, }}>

                                        <Text style={[styles.text, { fontSize: 20 }]}>
                                            {'Ganesh'}
                                        </Text>

                                        <Text style={[styles.text, { fontSize: 14 }]}>
                                            {"ganaborase83@gmail.com"}
                                        </Text>

                                        <View style={{ width: 180 }}>
                                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#841584', height: 40, borderRadius: 20 }} >
                                                <Text style={{ color: "white", fontSize: 15 }}>{"Pending Request"}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </View>
                            </View>
                        </View>

                        <View style={{
                            elevation: 0,
                            backgroundColor: '#f3f3f3',
                            marginTop: 30,
                            width: this.state.deviceWidth, padding: 15, borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717'
                        }}>

                            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                <View>
                                    <Text>{'Dec'}</Text>
                                    <View style={styles.profileImage1}>
                                        <Text style={{ textAlign: 'center', marginTop: 5 }}>{'21'}</Text>
                                    </View>
                                </View>
                                {/* <Image source={{ uri: 'https://picsum.photos/id/12/200/300' }} style={styles.profileImage1} /> */}
                                <View style={{ flex: 1, flexDirection: "row", padding: 20, borderRadius: 10, backgroundColor: '#fff', }}>
                                    <Image
                                        source={Images.Profileimg}
                                        style={{ tintColor: '#000', height: 50, width: 50, marginRight: 30 }}
                                    />

                                    <View style={{ flex: 1, }}>

                                        <Text style={[styles.text, { fontSize: 20 }]}>
                                            {'Vishwaroop'}
                                        </Text>

                                        <Text style={[styles.text, { fontSize: 14 }]}>
                                            {"ganaborase83@gmail.com"}
                                        </Text>

                                        <View style={{ width: 180 }}>
                                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#841584', height: 40, borderRadius: 20 }} >
                                                <Text style={{ color: "white", fontSize: 15 }}>{"Pending Request"}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </View>
                            </View>
                        </View>

                        <View style={{
                            elevation: 0,
                            backgroundColor: '#f3f3f3',
                            marginTop: 30,
                            width: this.state.deviceWidth, padding: 15, borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717'
                        }}>

                            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                <View>
                                    <Text>{'Dec'}</Text>
                                    <View style={styles.profileImage1}>
                                        <Text style={{ textAlign: 'center', marginTop: 5 }}>{'21'}</Text>
                                    </View>
                                </View>
                                {/* <Image source={{ uri: 'https://picsum.photos/id/12/200/300' }} style={styles.profileImage1} /> */}
                                <View style={{ flex: 1, flexDirection: "row", padding: 20, borderRadius: 10, backgroundColor: '#fff', }}>
                                    <Image
                                        source={Images.Profileimg}
                                        style={{ tintColor: '#000', height: 50, width: 50, marginRight: 30 }}
                                    />

                                    <View style={{ flex: 1, }}>

                                        <Text style={[styles.text, { fontSize: 20 }]}>
                                            {'Rahul'}
                                        </Text>

                                        <Text style={[styles.text, { fontSize: 14 }]}>
                                            {"ganaborase83@gmail.com"}
                                        </Text>

                                        <View style={{ width: 180 }}>
                                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: '#841584', height: 40, borderRadius: 20 }} >
                                                <Text style={{ color: "white", fontSize: 15 }}>{"Pending Request"}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    profileContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 8,
        marginLeft: 6,
        marginBottom: 8
    },
    text: {
        fontFamily: 'bold',
        color: '#000',
        marginBottom: 5
    },
    profileImage1: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginRight: 20, backgroundColor: 'skyblue'
    },
    profileImage: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginLeft: 40
    },
    profileName: {
        marginLeft: 6,
        fontSize: 16,
        color: '#181818'
    },
    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },
    userRow: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#171717'
    },

})
