import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import Drawer from '../Style/Drawer';
import NetInfo from "@react-native-community/netinfo";
import Util from '../Util/Util';
import Toast from 'react-native-simple-toast';
import CommonActivityIndicator from '../Util/ActivityIndicator';
import I18n from '../common/localization/index';
var comment = "ganeshborasehaiyeinsaanfrdghgfhfh dzxdx"

export default class AssestmentRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDrawer: false,
            connection_status: false,
            viewmore: false,
            id: '',
            viewMoreId: 0,
        };
    }

    componentDidMount() {
        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data, getuserData: data, userToken1: data.userToken })
                this.AssestmentRequestListApi(data);
            }
        });
        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });


            } else {
                this.setState({ connection_status: false });
            }
        });
    }

    AssestmentRequestListApi = (data) => {
        if (this.state.connection_status) {
            var url = 'http://139.162.159.9/snowboard/api/getuserrequestdetails';
            fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': "Bearer " + data.userToken,
                    'Content-Type': 'application/json',
                },
                // body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("response assreqlist", response)
                    this.setState({ assreqlistdata: response.data.data_assessment_request })
                })
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }

    clickHamburgerMenu = () => {
        this.setState({ showDrawer: !this.state.showDrawer })
    }

    starhighlight = (item) => {
        if (item?.admin_assessment_level == null || item?.admin_assessment_level == "") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 30 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.admin_assessment_level == "0") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 30 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.admin_assessment_level == "1") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'yellow', paddingTop: 8, paddingLeft: 30 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.admin_assessment_level == "2") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'yellow', paddingTop: 8,paddingLeft: 30 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'yellow', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.admin_assessment_level == "3") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'yellow', paddingTop: 8, paddingLeft: 30 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'yellow', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome'} name={"star"} style={{ fontSize: 15, color: 'yellow', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        return true;
    };

    snowflakehighlight = (item) => {
        if (item?.star == null || item?.star == "") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 20 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.star == "0") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 20 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.star == "1") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'skyblue', paddingTop: 8, paddingLeft: 20 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.star == "2") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'skyblue', paddingTop: 8, paddingLeft: 20 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'skyblue', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'gray', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        if (item?.star == "3") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'skyblue', paddingTop: 8, paddingLeft: 20 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'skyblue', paddingTop: 8, paddingLeft: 5 }} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={{ fontSize: 15, color: 'skyblue', paddingTop: 8, paddingLeft: 5 }} />
                </View>
            )
        }
        return true;
    };

    professionalassesmentstatus = (item) => {
        console.log("professionalassesmentstatus", item)
        if (item?.assessment_status == "pending") {
            return (
                <View style={{}}>
                    <Text style={{ fontSize: 15, color: '#6c757d', maxWidth: "87%", color: 'green' }}>{I18n.t("Professionalassessmentpending")}</Text>
                </View>
            )
        }
        if (item?.assessment_status == "accept") {
            return (
                <View style={{}}>
                    <Text style={{ fontSize: 15, color: '#6c757d', maxWidth: "87%", color: 'green' }}>{I18n.t("Professionalassessmentinprogress")}</Text>
                </View>
            )
        }
        return true;
    };

    selfassesmentstatus = (item) => {
        return true
    };

    setViewmore = (index) => {
        console.log("index++++++++++", index)
        console.log("this.state.viewmore++++++++++", this.state.viewmore)
        this.setState({ viewmore: !this.state.viewmore, viewMoreId: index })
        return true
    };

    _renderBannerItem({ item, index }) {
        return (
            <View>
                <View style={[styles.card, styles.elevation]}>
                    <View style={{ paddingHorizontal: 10, flexDirection: 'row', paddingTop: 15, }}>
                        <View>
                            <Video source={{ uri: "http://139.162.159.9/snowboard/" + item.assesment_request.url }}
                                style={styles.videostyle}
                                repeat={true}
                                ref={(ref) => {
                                    this.player = ref
                                }}
                                resizeMode='cover'
                                onBuffer={this.onBuffer}
                                onError={this.videoError}
                                poster={'https://media.istockphoto.com/vectors/illustration-of-simple-black-video-player-design-template-for-laptop-vector-id1217988423?k=20&m=1217988423&s=612x612&w=0&h=PR7L7GrPienKTe1-eXPf-Yenm0ATDPwJeOc82ZTygbA='}
                                posterResizeMode={'cover'}
                                controls={true}
                                paused={true}
                            />
                        </View>

                        <View style={{ paddingHorizontal: 10 }}>
                            <View style={{ height: 270 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 25, color: '#181818', width: 160, fontFamily: 'Montserrat-SemiBold', textTransform: 'uppercase' }}>{item.assesment_level}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 14, color: '#6c757d', paddingTop: 10, fontFamily: 'Montserrat-SemiBold', textTransform: 'uppercase' }}>{I18n.t("selfassessment")}</Text>
                                    {
                                        this.snowflakehighlight(item)
                                    }
                                </View>
                                <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                                    <Text style={{ fontSize: 14, maxWidth: 200, color: '#6c757d', fontFamily: 'Montserrat-SemiBold', textTransform: 'uppercase' }}>{I18n.t("professionalassessmentt")}</Text>
                                    {
                                        this.starhighlight(item)
                                    }
                                </View>
                                {item.admin_comment !== null &&
                                    <Text style={{ top: 5, flexDirection: 'row', width: 170 }}>
                                        <Text style={{ fontSize: 15, color: '#6c757d', }}>{(this.state.viewmore && (this.state.viewMoreId === item.id)) ? item.admin_comment : `${item.admin_comment.substring(0, 10)}`}</Text>
                                        <TouchableOpacity onPress={() => this.setViewmore(item.id)}>
                                            {item.admin_comment.length >= 10 &&
                                                <Text style={{ fontSize: 15, color: "red", top: 10 }}>{(this.state.viewmore && (this.state.viewMoreId === item.id)) ? I18n.t('viewless') : I18n.t('viewmore')}</Text>
                                            }
                                        </TouchableOpacity>
                                    </Text>
                                }
                                <View style={{ paddingVertical: 5, top: 5, maxWidth: "87%" }}>
                                    {
                                        this.selfassesmentstatus(item)
                                    }
                                </View>
                                <View style={{ paddingVertical: 5, top: 5, maxWidth: "87%" }}>
                                    {
                                        this.professionalassesmentstatus(item)
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View >
        )
    }

    render() {
        return (
            <Container>
                <View style={{ flex: 1, }}>
                    <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: '#e3e6f0' }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#00a0e4', paddingLeft: 10 }}>{I18n.t("assessmentrequests")}</Text>
                    </View>
                    {this.state.assreqlistdata !== undefined && this.state.assreqlistdata !== "" ?
                        <FlatList
                            data={this.state.assreqlistdata}
                            renderItem={this._renderBannerItem.bind(this)}
                            keyExtractor={(item, index) => index.toString()}
                            extraData={this.state}
                            ListEmptyComponent={this.ListEmptyComponent}
                        />
                        :
                        <CommonActivityIndicator></CommonActivityIndicator>
                    }
                </View>
                {this.state.showDrawer ?
                    <Drawer global_navigation={this.props.navigation} data={this} />
                    : null
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    profileImage: {
        width: 180,
        height: 210,
        borderRadius: 10,
        marginLeft: 6
    },
    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },
    card: {
        backgroundColor: 'white',
        // marginHorizontal:10,
        marginLeft: 8,
        // marginRight:10,
        borderRadius: 8,
        // paddingVertical: 2,
        // paddingHorizontal:5,
        width: '96%',
        // marginVertical: 10,
        marginVertical: 10,
    },
    elevation: {
        elevation: 15,
        shadowColor: '#6c757d',
    },
    titleStyle: {
        fontSize: 25,
        color: '#696969',
        fontFamily: 'Montserrat-Bold',
    },
    videostyle: {
        // width: 150,
        // height: 200,
        // // borderRadius: 10,
        // // marginLeft: 6
        width: 120,
        height: 250,
        borderRadius: 10,
        marginLeft: 6,
        // position: 'absolute',
    },
    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
    creditlogoStyle: {
        fontSize: 20,
        color: '#696969',
        // marginLeft: 25
    },

})
