import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import Drawer from '../Style/Drawer';
// import fonts from '../assets/fonts';

var data = [
    {
        name: "Level 1",
        about: "Self Assessment",
        about1: "Professional Assessment",
        value: '7',
        videoUrl: 'https://media.istockphoto.com/videos/snowboarding-fresh-snow-turn-video-id500241549'
    },
    {
        name: "Level 2",
        about: "Self Assessment",
        about1: "Professional Assessment",
        value: '14',
        videoUrl: 'https://media.istockphoto.com/videos/skier-spraying-snow-at-the-camera-video-id515900616'
    },
    // {
    //     name: "Snail",
    //     about: "Video Description",
    //     videoUrl: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-slow.mp4'
    // },
    // {
    //     name: "Cat",
    //     about: "Video Description",
    //     videoUrl: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-cute.mp4'
    // },
    // {
    //     name: "Spider",
    //     about: "Video Description ",
    //     videoUrl: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-eek.mp4'
    // },
    // {
    //     name: "Snail",
    //     about: "Video Description",
    //     videoUrl: 'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-slow.mp4'
    // }
]

export default class AssestmentRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDrawer: false
        };
    }

    componentDidMount() {
        // this.callApi();
        // this.CacheProductCallApi();
    }

    clickHamburgerMenu = () => {
        this.setState({ showDrawer: !this.state.showDrawer })
    }
    _renderBannerItem({ item, index }) {
        // { console.log("item", item) }
        return (
            <View>
                <View style={styles.userRow}>
                    <View style={{ paddingHorizontal: 10, flexDirection: 'row' }}>
                        {/* <Image source={{ uri: 'https://picsum.photos/id/11/200/300' }} style={styles.profileImage} /> */}
                        <View>
                            <Video source={{ uri: item.videoUrl }}
                                style={styles.videostyle}
                                repeat={true}
                                ref={(ref) => {
                                    this.player = ref
                                }}
                                resizeMode='cover'
                                onBuffer={this.onBuffer}
                                onError={this.videoError}
                                controls={true}
                                // playInBackground={false}
                                // autoplay={false}
                                paused={true}
                            />
                            {/* <TouchableOpacity style={{marginLeft:40,marginTop:225}} onPress={() => this.props.navigation.navigate("RequestAssesmentScreen")}>
                                <Text style={{ color: "blue", textAlign: 'left',fontFamily: 'Montserrat-Regular', textDecorationLine: 'underline', fontSize: 17 }}>{"Assesment"}</Text>
                            </TouchableOpacity> */}
                        </View>

                        <View style={{ paddingHorizontal: 15, marginLeft: 10 }}>
                            <View style={{ height: 150 }}>
                                <Text style={{ fontSize: 25, color: '#181818', width: 180, fontFamily: 'Montserrat-SemiBold' }}>{item.name}</Text>
                                {/* <TouchableOpacity>
                                    <Text style={{ fontSize: 20, color: '#181818', paddingTop: 20, width: 180, textDecorationLine: 'underline' }}>{item.about}</Text>
                                </TouchableOpacity> */}
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity>
                                        <Text style={{ fontSize: 20, color: '#181818', paddingTop: 20, width: 150 }}>{item.about1}</Text>
                                    </TouchableOpacity>
                                    {/* <TouchableOpacity> */}
                                    <Text style={{ fontSize: 20, color: '#181818', paddingTop: 20, width: 30, }}>{item.value}</Text>
                                    {/* </TouchableOpacity> */}
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                                <TouchableOpacity style={{ backgroundColor: '#D73951', justifyContent: 'center', height: 40, width: 180, borderRadius: 10, right: 10, borderWidth: 1 }} onPress={() => this.props.navigation.navigate("ChatUserListScreen")}>
                                    <Text style={{ color: "#FFF", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{"Chat"}</Text>
                                </TouchableOpacity>
                                {/* <TouchableOpacity style={{ marginLeft: 10, backgroundColor: '#D73951', justifyContent: 'center', height: 50, width: 90, borderRadius: 10, right: 10 }} onPress={() => this.props.navigation.navigate("UserassesmentScreen")}>
                                    <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{"See Demo"}</Text>
                                </TouchableOpacity> */}
                            </View>
                        </View>

                    </View>
                </View>
            </View>
        )
    }



    render() {
        return (
            <Container>
                <View style={{ flex: 1, }}>
                    {/* <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row', backgroundColor: '#fff', paddingVertical: 10, paddingHorizontal: 15, borderBottomWidth: 1 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("MyAccountScreen")}>
                            <Icon type={'Entypo'} name={"user"} style={styles.iconStyle} />
                        </TouchableOpacity>
                        <Icon type={'MaterialCommunityIcons'} name={"crown"} style={styles.iconStyle} />
                        <Icon type={'MaterialCommunityIcons'} name={"trophy"} style={styles.iconStyle} />
                        <View>
                            <Text style={{ fontSize: 15 }}>{"105"}</Text>
                            <Icon type={'MaterialCommunityIcons'} name={"copyright"} style={styles.creditlogoStyle} />
                        </View>
                        <TouchableOpacity onPress={() => this.clickHamburgerMenu()}>
                            <Icon type={'Entypo'} name={"menu"} style={styles.iconStyle} />
                        </TouchableOpacity>
                    </View> */}

                    <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: 'gray' }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: 'white', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#FFF', paddingLeft: 10 }}>{"Coaching Requests"}</Text>
                    </View>

                    {/* <View style={{ flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 15 }}>
                        <Text style={styles.titleStyle}>{"CoachingRequest"}</Text>
                    </View> */}

                    <FlatList
                        data={data}
                        renderItem={this._renderBannerItem.bind(this)}
                        keyExtractor={(item, index) => index.toString()}
                        extraData={this.state}
                        ListEmptyComponent={this.ListEmptyComponent}
                    />

                </View>
                {this.state.showDrawer ?
                    <Drawer global_navigation={this.props.navigation} data={this} />
                    : null
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    profileImage: {
        width: 180,
        height: 210,
        borderRadius: 10,
        marginLeft: 6
    },
    // profileName: {
    //     marginLeft: 6,
    //     fontSize: 20,
    //     color: '#181818'
    // },
    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },
    userRow: {
        // borderBottomWidth: StyleSheet.hairlineWidth,
        // borderBottomColor: '#171717',
        paddingVertical: 10,
        borderWidth: StyleSheet.hairlineWidth,
    },
    titleStyle: {
        fontSize: 30,
        color: '#696969',
        fontFamily: 'Montserrat-Bold',
    },
    // backgroundVideo: {
    //     position: 'absolute',
    //     top: 0,
    //     left: 0,
    //     bottom: 0,
    //     right: 0,
    // },
    videostyle: {
        // width: 150,
        // height: 200,
        // // borderRadius: 10,
        // // marginLeft: 6
        width: 170,
        height: 210,
        borderRadius: 10,
        marginLeft: 6,
        // position: 'absolute',
    },

    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
    creditlogoStyle: {
        fontSize: 20,
        color: '#696969',
        // marginLeft: 25
    },

})
