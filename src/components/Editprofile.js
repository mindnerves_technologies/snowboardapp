import React, { Component } from 'react';
import { View, Keyboard, StyleSheet, Dimensions, Alert, TouchableOpacity, Button, Image } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
import CheckBox from 'react-native-check-box';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import NetInfo from "@react-native-community/netinfo";
import Images from "../constants/Images";

var ImagePicker = require('react-native-image-picker');


const { height, width } = Dimensions.get("window");

export default class Editprofile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userList: props.userList || [],
            fname: '',
            lname: '',
            email: '',
            password: '',
            confirmPassword: '',
            isChecked: false,
            fnameerror: null,
            lnameerror: null,
            emailerror: null,
            passworderror: null,
            connection_status: false,
            photo: null,
            Profiledata: "",
            // isNameEditable: false,
        }
    }
    componentDidMount() {

        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({
                    userId: data.userId, getuserData: data, Token: data.userToken,
                    fname: data.userFName, lname: data.userLName, email: data.userEmail
                })
                // this.HeaderApi(data);
                this.LoadViewProfileAPI();
            }
        })

        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });
                //call API
            } else {
                this.setState({ connection_status: false });
            }
        });

    }

    LoadViewProfileAPI = () => {

        // this.setState({ showProgressBar: true })

        // if (this.state.connection_status) {
        console.log("In api")
        var url = 'http://139.162.159.9/snowboard/api/user/' + this.state.userId;
        let mUserData = {
            "UID": this.state.userId
        };
        fetch(url, {
            method: 'GET',
            headers: {
                // 'Authorization': 'Bearer exn50dak2a5iahy02hawo5il0y6j25ct',
                'Content-Type': 'application/json; charset=utf-8',
                'Accept': 'application/json',
                'Cache-Control': 'no-cache',
                'Access-control-allow-origin': '*',
            },
            // body: JSON.stringify(mUserData),
        })
            .then((response) => response.json())
            .then((response) => {
                console.log("response", response)
                this.setState({ Profiledata: response })
            })
            .catch((error) => {
                console.log("In catch 75")
                this.setState({ showProgressBar: false })
                Toast.showWithGravity(error, Toast.SHORT, Toast.CENTER);
            })
            .done();

        // } 

        // else {
        //     Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);

        //     this.setState({ showProgressBar: false })
        // }
    }

    handleChoosePhoto = () => {
        // const options = {
        //     noData: true
        // };
        // ImagePicker.launchImageLibrary(options, response => {
        //     console.log("response", response);
        //     // console.log("response.assets[0].uri", response.assets[0].uri);
        //     if (response !== null) {
        //         this.setState({ photo: response });
        //     }
        // });
        ImagePicker.launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: true,
            },
            (response) => {
                console.log("response in image picker", response);
                this.setState({ photo: response.assets[0] });
            },
        )
    };


    SignUp = () => {
        const { email, fname, lname, fnameerror, lnameerror } = this.state
        if (fname !== "" && lname !== "" && email !== "" && fnameerror == null, lnameerror == null) {
            this.EditApi();
        }
        else {
            Toast.showWithGravity('Enter correct credentials and accept our terms & conditions', Toast.SHORT, Toast.CENTER);
            // this.setState({fnameerror: "Required min 3 character and max 10 character and number not accepted", lnameerror: " Required min 3 character and max 10 character and number not accepted", emailerror: "This field is required", passworderror: "This field is required" })
        }

    }

    EditApi = () => {

        if (this.state.connection_status) {
            let formData = new FormData
            if (this.state.photo) {
                console.log("this.state.photo", this.state.photo)
                formData.append('profile_image', {
                    name: 'photo.jpg',
                    uri: this.state.photo.uri,
                    type: 'image/jpeg'
                })
                // formData.append("image", this.state.photo);
            }
            formData.append("user_id", this.state.userId);
            formData.append("first_name", this.state.fname);
            formData.append("last_name", this.state.lname);
            formData.append("email", this.state.email);
            console.log("formData", formData)
            var url = 'http://139.162.159.9/snowboard/api/register'

            // let mUserData =
            // {
            //     user_id:this.state.userId,
            //     first_name: this.state.fname,
            //     last_name: this.state.lname,
            //     email: this.state.email,
            //     // password: this.state.password
            // };
            // console.log("mUserData", mUserData);
            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + this.state.Token,
                    'Content-Type': 'multipart/form-data',
                },
                body: formData,
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("response in edit api", response)
                    if (response.status === 'success') {
                        let user_data = {
                            userId: response.data.id,
                            userFName: response.data.first_name,
                            userLName: response.data.last_name,
                            userEmail: response.data.email,
                            userLevel: response.data.level,
                            userDOB: response.data.dob,
                            userStatus: response.status,
                            userCredit: response.data.credit,
                            userTrophy: response.data.trophy,
                            userCrown: response.data.crown,
                            userToken: response.data.api_token,
                            userImage: response.data.image,
                        }
                        // this.setState({ profile_image: response.data.image});
                        AsyncStorage.setItem('userData', JSON.stringify(user_data))
                        // Toast.showWithGravity('Welcome To Snowboard App' + "\n\t\t\t\t\t\t\t\t\t\t\t\t\t🙏🙏", Toast.LONG, Toast.CENTER)
                        this.props.navigation.replace('MyAccountScreen')
                        // this.props.navigation.replace('VideoListScreen', {
                        //     // 	// param_OID: item.OID
                        // })
                        // this.props.navigation.navigate('VideoListScreen')
                    }
                    else {
                        Toast.showWithGravity(response.message.tostring(), Toast.SHORT, Toast.CENTER);
                    }

                })
                .catch((error) => {
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }

    validatefirstname = (text) => {
        let reg = /^[a-zA-Z]{3,10}$/;
        if (reg.test(text) === false) {
            this.setState({ fname: text, fnameerror: " Required min 3 character and max 10 character and Number not accepted" })
            return false;
        }
        else {
            this.setState({ fname: text, fnameerror: null })
        }
    }

    validatelastname = (text) => {
        let reg = /^[a-zA-Z]{3,10}$/;
        if (reg.test(text) === false) {
            this.setState({ lname: text, lnameerror: " Required min 3 character and max 10 character and number not accepted" })
            return false;
        }
        else {
            this.setState({ lname: text, lnameerror: null })
        }
    }

    render() {
        const { photo } = this.state;
        return (
            <Container>
                <Content>
                    <View style={{ flexDirection: 'row', paddingVertical: 10, paddingLeft: 25, backgroundColor: '#e3e6f0' }}>
                        <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#8A8A8A', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                        <Text style={{ fontSize: 25, fontFamily: 'Montserrat-Bold', color: '#00a0e4', marginLeft: 20 }}>{"Edit Profile"}</Text>
                    </View>
                    <View style={{ bottom: 20 }}>
                        {console.log("photo ", photo)}
                        {photo !== null ?
                            <View>
                                <Image
                                    source={{ uri: photo.uri }}
                                    resizeMode="cover"
                                    style={{
                                        width: 150, height: 150, marginLeft: 130, top: 50, borderRadius: 170,
                                    }} />
                            </View>
                            :
                            <View>
                                {console.log("after photot null")}
                                {console.log("after photot null", this.state.Profiledata)}
                                {this.state.Profiledata !== undefined && this.state.Profiledata !== "" && this.state.Profiledata.data.image !== "" ?
                                    <Image
                                        source={{ uri: "http://139.162.159.9/snowboard/" + this.state.Profiledata.data.image }}
                                        resizeMode="cover"
                                        style={{
                                            width: 150, height: 150, marginLeft: 130, top: 50, borderRadius: 170,
                                        }} />
                                    :
                                    <View>
                                        <Image
                                            source={Images.Profileimg}
                                            resizeMode="cover"
                                            style={{
                                                width: 150, height: 150, marginLeft: 130, top: 50, borderRadius: 170,
                                            }} />
                                    </View>
                                }
                            </View>

                        }
                        <TouchableOpacity
                            style={{ alignItems: 'center', width: 120, justifyContent: 'center', backgroundColor: '#717384', height: 30, borderRadius: 10, marginTop: 80, marginLeft: 145 }}
                            onPress={this.handleChoosePhoto}
                        >
                            <Text style={{ color: '#fff', fontSize: 15, textAlign: 'center' }}>{"Upload_Image"}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.card}>
                        {/* <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', paddingVertical: 10 }}>
                        </View> */}
                        <Form style={styles.formStyle}>
                            <View style={styles.textInputContainer}>
                                <Input
                                    style={styles.textInputStyle}
                                    value={this.state.fname}
                                    ref={"fullNameRef"}
                                    returnKeyLabel='Submit'
                                    // placeholder={this.state.getuserData?.userFName}
                                    onChangeText={(text) => this.validatefirstname(text)}
                                    textColor={'#000'}
                                    // inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                    editable={true}
                                />
                                {/* <TouchableOpacity style={styles.textFieldIcon}
                                    onPress={() => {
                                        this.setState({ isNameEditable: true }
                                            // , () => { this.fullNameRef.focus()} 
                                        )
                                    }}>
                                    <Icon type={'MaterialIcons'} name={"edit"} style={styles.iconStyle} />
                                </TouchableOpacity> */}
                            </View>
                            {this.state.fnameerror !== null && <Text style={{ color: 'red' }}>{this.state.fnameerror}</Text>}

                            <View style={styles.textInputContainer}>
                                <Input
                                    style={styles.textInputStyle}
                                    getRef={(input) => this.nameInput = input}
                                    value={this.state.lname}
                                    blurOnSubmit={true}
                                    // placeholder={this.state.getuserData?.userLName}
                                    // onChangeText={(text) => this.setState({ lname: text })}
                                    onChangeText={(text) => this.validatelastname(text)}
                                    textColor={'#000'}
                                    inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                    labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                    returnKeyType={"next"}
                                    keyboardType={"email-address"}
                                    autoCapitalize='none'
                                    onSubmitEditing={() => this._focusInput('emailInput')}
                                    editable={true}
                                />
                                {/* <View style={styles.textFieldIcon}>
                                    <Icon type={'MaterialIcons'} name={"edit"} style={styles.iconStyle} />
                                </View> */}
                            </View>
                            {this.state.lnameerror !== null && <Text style={{ color: 'red' }}>{this.state.lnameerror}</Text>}

                            <View style={styles.textInputContainer}>
                                <Input
                                    style={styles.textInputStyle}
                                    getRef={(input) => this.emailInput = input}
                                    value={this.state.email}
                                    blurOnSubmit={true}
                                    placeholder={this.state.getuserData?.userEmail}
                                    // onChangeText={(text) => this.setState({ email: text })}
                                    onChangeText={(text) => this.validateEmail(text)}
                                    textColor={'#000'}
                                    inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                    labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                    returnKeyType={"next"}
                                    keyboardType={"email-address"}
                                    autoCapitalize='none'
                                    editable={false}
                                    onSubmitEditing={() => this._focusInput('passwordInput')}
                                />
                                {/* <View style={styles.textFieldIcon}>
                                    <Icon type={'MaterialIcons'} name={"mail"} style={styles.iconStyle} />
                                </View> */}
                            </View>
                            {this.state.emailerror !== null && <Text style={{ color: 'red' }}>{this.state.emailerror}</Text>}
                        </Form>
                        <View style={{ width: '100%', paddingTop: 25 }}>
                            <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 50, borderRadius: 15 }}
                                onPress={() => this.SignUp()}
                            >
                                <Text style={{ color: "white", fontSize: 22, fontFamily: 'Montserrat-Regular', }}>{"Update"}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginVertical: 25 }}>
                        </View>
                    </View>
                </Content>
            </Container>
        )
    }
}


const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 30,
        fontWeight: '900',
        color: '#D73951',
        fontFamily: 'Montserrat-Bold',
        marginTop: 15,
        // textTransform: 'uppercase', 
        // textAlign: 'center'
    },
    card: {
        marginHorizontal: 25,
        backgroundColor: '#fff',
        // shadowOffset: { width: 3, height: 3, },
        // shadowColor: 'black',
        // shadowOpacity: 0.3,
        marginTop: 25,
    },
    formStyle: {
        width: '100%',
        paddingTop: 10,
        // flex: 1,
        paddingBottom: 40,
        backgroundColor: '#fff',
    },
    textInputContainer: {
        // flex: 1, 
        // marginTop: 5,
        borderBottomWidth: 0.5, borderColor: 'lightgray',
        backgroundColor: '#fff',
    },
    textInputItemStyle: {
        backgroundColor: '#fff'
    },
    textInputTitleStyle: {
        color: '#181818',
        fontFamily: 'Montserrat-Regular'
    },
    textInputStyle: {
        height: 45,
        // marginTop: 5,
        marginBottom: 15,
        // width: width - 130,
        borderColor: "gray",
        // paddingLeft: 5,
        // paddingRight: 20,
        // marginRight: 35,
        // color: '#181818',
    },
    textFieldIcon: {
        position: 'absolute', right: 10, top: 5
    },
    iconStyle: {
        fontSize: 35,
        color: '#8A8A8A'
    },

})
