import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Toast from 'react-native-simple-toast';
import I18n from '../common/localization/index';
// import fonts from '../assets/fonts';


export default class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    }

    componentDidMount() {
        // this.callApi();
        // this.CacheProductCallApi();
    }
    ResetPassword = () => {
        const { email} = this.state
        if (email !=="" ) {
          this.ResetPasswordApi();
        }
        else {
          Toast.showWithGravity('Enter emailId', Toast.SHORT, Toast.CENTER);
        }
    
      }

      ResetPasswordApi = () => {

        // if (this.state.connection_status) {
    
        var url = 'http://139.162.159.9/snowboard/api/forgot_password'
    
        let mUserData =
        {
          email: this.state.email,
        };
    
    
        fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(mUserData),
        })
          .then((response) => response.json())
          .then((response) => {
            console.log("response", response)
            if (response.status === 'ok') {
              // await Util.setAsyncStorage('user-info')
              Toast.showWithGravity("Email Sent Successfully Reset Your Password Using Link", Toast.SHORT, Toast.CENTER);
              // this.props.navigation.navigate('VideoListScreen')
            }
            else {
              Toast.showWithGravity(response.message, Toast.SHORT, Toast.CENTER);
            }
    
          })
          .catch((error) => {
            this.setState({ showProgressBar: false })
            Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
          })
          .done();
        // } 
        // else {
        //   Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //   this.setState({ showProgressBar: false })
        // }
      }


    render() {
        const { data1 } = this.state;
        return (
            <View style={{flex:1}}>
                <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: '#e3e6f0' }}>
                    <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#00a0e4', paddingLeft: 20 }}>{I18n.t("forgetpassword")}</Text>
                </View>
                <View style={{justifyContent: 'flex-start', paddingHorizontal: 25 }}>
                    <Form style={styles.formStyle}>
                        <View style={styles.textInputContainer}>
                            <Label style={{ fontSize: 20, fontFamily: 'Montserrat-Regular',paddingLeft:10}}>{I18n.t('enteryouremailid')}</Label>
                            <Item floatingLabel last style={styles.textInputItemStyle}>
                                <Label style={styles.textInputTitleStyle}>{I18n.t('EMailId')}</Label>
                                <Input
                                    style={styles.textInputStyle}
                                    getRef={(input) => this.emailInput = input}
                                    value={this.state.email}
                                    blurOnSubmit={true}
                                    placeholderTextColor={'#fff'}
                                    onChangeText={(text) => this.setState({ email: text })}
                                    textColor={'#000'}
                                    inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                    labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                    returnKeyType={"next"}
                                    keyboardType={"email-address"}
                                    autoCapitalize='none'
                                    onSubmitEditing={() => this._focusInput('passwordInput')}
                                />
                            </Item>
                            <View style={styles.textFieldIcon}>
                                <Icon type={'FontAwesome'} name={"envelope"} style={styles.iconStyle} />
                            </View>
                        </View>

                    </Form>
                    <View style={{ width: '100%', paddingTop: 30 }}>
                        <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 50, borderRadius: 15,}} onPress={() => this.ResetPassword()}>
                            <Text style={{ color: "white", fontSize: 22, fontFamily: 'Montserrat-Regular' }}>{I18n.t("sendlink")}</Text>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', paddingTop: 30, marginBottom: 20 }}>
                    </View> */}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 30,
        // fontWeight: '900',
        color: '#696969',
        fontFamily: 'Montserrat-Bold',
        marginTop: 15,
        // textTransform: 'uppercase', 
        // textAlign: 'center'
    },
    // card: {
    //   marginLeft: 20,
    //   marginRight: 20,
    //   backgroundColor: '#fff',
    //   shadowOffset: { width: 3, height: 3, },
    //   shadowColor: 'black',
    //   shadowOpacity: 0.3,
    //   marginTop: 50,
    // },
    formStyle: {
        paddingTop: 20,
        width: '100%',
        // flex: 1,
        paddingBottom: 20,
        backgroundColor: '#fff',
    },
    textInputContainer: {
        // flex: 1, 
        // marginTop: 10,
        borderBottomWidth: 0.5, borderColor: 'lightgray',
        backgroundColor: '#fff',
    },
    textInputItemStyle: {
        backgroundColor: '#fff',

    },
    textInputTitleStyle: {
        color: '#181818',
        fontFamily: 'Montserrat-Regular'
        // marginBottom:10

    },
    textInputStyle: {
        height: 45,
        marginTop: 5,
        marginBottom: 15,
        // width: width - 130,
        borderColor: "gray",
        // paddingLeft: 5,
        // paddingRight: 20,
        // marginRight: 35,
        // color: '#181818',
    },
    textFieldIcon: {
        position: 'absolute', right: 10, top: 65
    },
    iconStyle: {
        fontSize: 35,
        color: '#8A8A8A'
    },

})
