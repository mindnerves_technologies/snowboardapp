import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, Button, FlatList, ActivityIndicator, Dimensions, ImageBackground } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
import Carousel from 'react-native-snap-carousel';
const { height, width } = Dimensions.get("window");
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import CommonActivityIndicator from '../Util/ActivityIndicator';
import messaging from '@react-native-firebase/messaging';
import { StripeProvider } from '@stripe/stripe-react-native';
import { initStripe } from '@stripe/stripe-react-native';
// const isCarousel = React.useRef(null)
var data = [
    {
        title: "Ski Together",
        body: "  pellentesque eu, pretium quis, sem.",
        imgUrl: "https://cdn.pixabay.com/photo/2019/09/26/03/54/manipulation-4505018__340.jpg"
    },
    {
        title: "Ski Together",
        body: ". . Curabitur at lacus ac velit ornare lobortis. ",
        imgUrl: "https://cdn.pixabay.com/photo/2017/01/17/19/00/snowboarding-1987709__340.jpg"
    },
    {
        title: "Ski Together",
        body: "  posuere ac, mattis non, nunc.",
        imgUrl: "https://cdn.pixabay.com/photo/2016/11/19/21/00/activity-1841153__340.jpg"
    }
]
export default class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            publishableKey: '',
            setPublishableKey: '',
        }
    }


    componentDidMount() {
        this.firebaseStart();
        this.fetchPublishableKey();
        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.props.navigation.replace('VideoListScreen')
            }
            else {
                this.props.navigation.replace('HomeScreen')
            }
        })
    }

    fetchPublishableKey = async () => {
        console.log("key before fetch")
        const key = await fetchKey(); // fetch key from your server here
        console.log("key",key)
        setPublishableKey(key);
    };

    firebaseStart() {
        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                Alert.alert('This is getInitialNotification');
                if (remoteMessage) {
                    if (remoteMessage.data.slug) {
                        // this.props.navigation.navigate('ProductDetais', {
                        //  slug: remoteMessage.data.slug
                        // })
                    }
                }
            });

        // Notification click when app is in background state
        messaging().onNotificationOpenedApp(remoteMessage => {
            Alert.alert('This is onNotificationOpenedApp');
            if (remoteMessage.data.slug) {
                // this.props.navigation.navigate('ProductDetais', {
                //  slug: remoteMessage.data.slug
                // })
            }
        });

    }

    render() {
        return (
                <View>
                    <CommonActivityIndicator></CommonActivityIndicator>
                </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {

    },
    image: {
        width: 100,
        height: 100,
    },
    header: {

    },
    body: {

    }
})