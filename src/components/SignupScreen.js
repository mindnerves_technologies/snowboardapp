import React, { Component } from 'react';
import { View, Keyboard, StyleSheet, Dimensions, Alert, TouchableOpacity, Button } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
import CheckBox from 'react-native-check-box';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NetInfo from "@react-native-community/netinfo";
import I18n from '../common/localization/index';
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";


const { height, width } = Dimensions.get("window");

export default class SignupScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userList: props.userList || [],
            fname: '',
            lname: '',
            email: '',
            password: '',
            confirmPassword: '',
            isChecked: false,
            fnameerror: null,
            lnameerror: null,
            emailerror: null,
            passworderror: null,
            showPasswordField: true,
            connection_status: false,
            fcm_token: '',
        }
    }
    componentDidMount() {
        this.checkPermission();
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
            onRegister: function (token) {
                console.log("TOKEN:", token);
            },
            onNotification: function (notification) {
                console.log("NOTIFICATION:", notification);
                //   notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
            onAction: function (notification) {
                console.log("ACTION:", notification.action);
                console.log("NOTIFICATION:", notification);
            },
            onRegistrationError: function (err) {
                console.error(err.message, err);
            },
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },

            popInitialNotification: true,
            requestPermissions: true,
        });

        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });
                //call API
            } else {
                this.setState({ connection_status: false });
            }
        });
    }

    async checkPermission() {
        console.log("in checkpermission")
        const enabled = await messaging().hasPermission();
        console.log("in checkpermission after enabled", enabled)
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async getToken() {
        console.log("in getToken")
        // if (!fcmToken) {
        // console.log("check fcmToken")
        fcmToken = await messaging().getToken();

        // if (fcmToken) {
        console.log("check fcmToken", fcmToken)
        this.setState({ fcm_token: fcmToken })
        // }
        // }
    }

    async requestPermission() {
        console.log("request permission call")
        try {
            await messaging().requestPermission();
            this.getToken();
        } catch (error) {

        }
    }

    showPassword() {
        this.setState({ showPasswordField: !this.state.showPasswordField });
    }

    SignUp = () => {
        const { email, password, fname, lname, fnameerror, lnameerror, emailerror, passworderror, isChecked } = this.state
        console.log('58', passworderror)
        if (fname !== "" && lname !== "" && email !== "" && password !== "", fnameerror == null, lnameerror == null, emailerror == null, passworderror == null, isChecked !== false) {
            this.SignUpApi();
        }
        else {
            Toast.showWithGravity('Enter correct credentials and accept our terms & conditions', Toast.SHORT, Toast.CENTER);
            // this.setState({fnameerror: "Required min 3 character and max 10 character and number not accepted", lnameerror: " Required min 3 character and max 10 character and number not accepted", emailerror: "This field is required", passworderror: "This field is required" })
        }

    }

    SignUpApi = () => {

        if (this.state.connection_status) {

            var url = 'http://139.162.159.9/snowboard/api/user'

            let mUserData =
            {
                first_name: this.state.fname,
                last_name: this.state.lname,
                email: this.state.email,
                password: this.state.password,
                device_id: this.state.fcm_token,
                device_type: "android"
            };
            console.log("mUserData", mUserData);
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("response in registration", response)
                    if (response.status === 'success') {
                        let user_data = {
                            userId: response.data.id,
                            userFName: response.data.first_name,
                            userLName: response.data.last_name,
                            userEmail: response.data.email,
                            userLevel: response.data.level,
                            userDOB: response.data.dob,
                            userStatus: response.status,
                            userCredit: response.data.credit,
                            userTrophy: response.data.trophy,
                            userCrown: response.data.crown,
                            userToken: response.data.api_token,
                        }
                        AsyncStorage.setItem('userData', JSON.stringify(user_data))
                        // Toast.showWithGravity('Welcome To Snowboard App' + "\n\t\t\t\t\t\t\t\t\t\t\t\t\t🙏🙏", Toast.LONG, Toast.CENTER)
                        this.props.navigation.replace('VideoListScreen')
                        // this.props.navigation.replace('VideoListScreen', {
                        //     // 	// param_OID: item.OID
                        // })
                        // this.props.navigation.navigate('VideoListScreen')
                    }
                    else {
                        Toast.showWithGravity(response.message.tostring(), Toast.SHORT, Toast.CENTER);
                    }

                })
                .catch((error) => {
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }
    // signUpTap = () => {

    //     Keyboard.dismiss();
    //     const { name, lname, email, password, confirmPassword } = this.state
    //     const fields = { name, lname, email, password, confirmPassword };
    //     const result = validateRegister(fields);

    //     if (result) {
    //         let data = { name, lname, email, password };
    //         this.props.signup(data);
    //     }

    // }

    validatefirstname = (text) => {
        let reg = /^[a-zA-Z]{3,10}$/;
        if (reg.test(text) === false) {
            this.setState({ fname: text, fnameerror: " Required min 3 character and max 10 character and Number not accepted" })
            return false;
        }
        else {
            this.setState({ fname: text, fnameerror: null })
        }
    }

    validatelastname = (text) => {
        let reg = /^[a-zA-Z]{3,10}$/;
        if (reg.test(text) === false) {
            this.setState({ lname: text, lnameerror: " Required min 3 character and max 10 character and number not accepted" })
            return false;
        }
        else {
            this.setState({ lname: text, lnameerror: null })
        }
    }

    validateEmail = (text) => {
        let reg = /^([\.\_a-zA-Z0-9]+)@([a-zA-Z]+)\.([a-zA-Z]){2,8}$/;
        if (reg.test(text) === false) {
            this.setState({ email: text, emailerror: "@  and . Required" })
            return false;
        }
        else {
            this.setState({ email: text, emailerror: null })
        }
    }

    validatepassword = (text) => {
        let reg = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
        if (reg.test(text) === false) {
            this.setState({ password: text, passworderror: "Min 8 char., at least one uppercase , one lowercase letter, one number and one special character" })
            return false;
        }
        else {
            this.setState({ password: text, passworderror: null })
        }
    }


    render() {
        return (
            <Container>
                <Content>

                    {/* <View style={{ marginBottom: 30, flexDirection: 'row', marginTop: 20 }}>
                            <Text style={styles.titleStyle}>{"Sign up"}</Text>

                        </View> */}
                    <View style={{ flexDirection: 'row', paddingVertical: 10, paddingLeft: 25, backgroundColor: '#e3e6f0' }}>
                        {/* <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#8A8A8A', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} /> */}
                        <Text style={{ fontSize: 25, fontFamily: 'Montserrat-Bold', color: '#00a0e4', marginLeft: 20 }}>{I18n.t("SignUp")}</Text>
                    </View>
                    <View style={styles.card}>
                        <View style={{ width: '100%' }}>
                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderWidth: 1, height: 50, borderRadius: 10, marginTop: 25 }}>
                                <Icon type={'MaterialCommunityIcons'} name={"google"} style={{ fontSize: 25, color: '#000' }} />
                                <Text style={{ color: "#ooo", fontSize: 20, fontWeight: '900', paddingLeft: 5, fontFamily: 'Montserrat-Regular' }}>{I18n.t("Logingoogle")}</Text>
                            </TouchableOpacity>
                        </View>
                        {/* <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', paddingVertical: 10 }}>
                        </View> */}
                        <Form style={styles.formStyle}>

                            <View style={styles.textInputContainer}>
                                <Item floatingLabel last style={styles.textInputItemStyle}>
                                    <Label style={styles.textInputTitleStyle}>{I18n.t('firstname')}</Label>
                                    <Input
                                        style={styles.textInputStyle}
                                        getRef={(input) => this.nameInput = input}
                                        value={this.state.fname}
                                        blurOnSubmit={true}
                                        // onChangeText={(text) => this.setState({ name: text })}
                                        onChangeText={(text) => this.validatefirstname(text)}
                                        textColor={'#000'}
                                        inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        returnKeyType={"next"}
                                        keyboardType={"email-address"}
                                        autoCapitalize='none'
                                    // onSubmitEditing={() => this._focusInput('emailInput')}
                                    />
                                </Item>
                                <View style={styles.textFieldIcon}>
                                    <Icon type={'FontAwesome'} name={"user-circle"} style={styles.iconStyle} />
                                </View>
                            </View>
                            {this.state.fnameerror !== null && <Text style={{ color: 'red' }}>{this.state.fnameerror}</Text>}

                            <View style={styles.textInputContainer}>
                                <Item floatingLabel last style={styles.textInputItemStyle}>
                                    <Label style={styles.textInputTitleStyle}>{I18n.t('lastname')}</Label>
                                    <Input
                                        style={styles.textInputStyle}
                                        getRef={(input) => this.nameInput = input}
                                        value={this.state.lname}
                                        blurOnSubmit={true}
                                        // onChangeText={(text) => this.setState({ lname: text })}
                                        onChangeText={(text) => this.validatelastname(text)}
                                        textColor={'#000'}
                                        inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        returnKeyType={"next"}
                                        keyboardType={"email-address"}
                                        autoCapitalize='none'
                                        onSubmitEditing={() => this._focusInput('emailInput')}
                                    />
                                </Item>
                                <View style={styles.textFieldIcon}>
                                    <Icon type={'FontAwesome'} name={"user-circle"} style={styles.iconStyle} />
                                </View>
                            </View>
                            {this.state.lnameerror !== null && <Text style={{ color: 'red' }}>{this.state.lnameerror}</Text>}

                            <View style={styles.textInputContainer}>
                                <Item floatingLabel last style={styles.textInputItemStyle}>
                                    <Label style={styles.textInputTitleStyle}>{I18n.t('EMailId')}</Label>
                                    <Input
                                        style={styles.textInputStyle}
                                        getRef={(input) => this.emailInput = input}
                                        value={this.state.email}
                                        blurOnSubmit={true}
                                        // onChangeText={(text) => this.setState({ email: text })}
                                        onChangeText={(text) => this.validateEmail(text)}
                                        textColor={'#000'}
                                        inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        returnKeyType={"next"}
                                        keyboardType={"email-address"}
                                        autoCapitalize='none'
                                        onSubmitEditing={() => this._focusInput('passwordInput')}
                                    />
                                </Item>
                                <View style={styles.textFieldIcon}>
                                    <Icon type={'MaterialIcons'} name={"mail"} style={styles.iconStyle} />
                                </View>
                            </View>
                            {this.state.emailerror !== null && <Text style={{ color: 'red' }}>{this.state.emailerror}</Text>}

                            <View style={{ flex: 1, borderBottomWidth: 0.5, borderColor: "lightgray" }}>
                                <Item floatingLabel last style={styles.textInputItemStyle}>
                                    <Label style={styles.textInputTitleStyle}>{I18n.t('Password')}</Label>
                                    <Input
                                        style={styles.textInputStyle}
                                        getRef={(input) => this.passwordInput = input}
                                        value={this.state.password}
                                        secureTextEntry={true}
                                        // onChangeText={(text) => this.setState({ password: text })}
                                        onChangeText={(text) => this.validatepassword(text)}
                                        secureTextEntry={this.state.showPasswordField}
                                        textColor={'#000'}
                                        inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        returnKeyType={"next"}
                                        onSubmitEditing={() => this._focusInput('confirmPasswordInput')}
                                    />
                                </Item>
                                <View style={styles.textFieldIcon}>
                                    <TouchableOpacity onPress={() => this.showPassword()}>
                                        <Icon type={'FontAwesome'}
                                            name={this.state.showPasswordField ? 'eye-slash' : 'eye'}
                                            style={styles.iconStyle} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.passworderror !== null && <Text style={{ color: 'red' }}>{this.state.passworderror}</Text>}
                            {/* <View style={{ flex: 1, borderBottomWidth: 0.5, borderColor: "lightgray" }}>
                                <Item floatingLabel last style={styles.textInputItemStyle}>
                                    <Label style={styles.textInputTitleStyle}>{'Confirm Password'}</Label>
                                    <Input
                                        style={styles.textInputStyle}
                                        getRef={(input) => this.confirmPasswordInput = input}
                                        value={this.state.confirmPassword}
                                        secureTextEntry={true}
                                        onChangeText={(text) => this.setState({ confirmPassword: text })}
                                        textColor={'#000'}
                                        inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                                        returnKeyType={"done"}
                                    />
                                </Item>
                                <View style={styles.textFieldIcon}>
                                    <Icon type={'Entypo'} name={"lock"} style={styles.iconStyle} />
                                </View>
                            </View> */}

                        </Form>
                        {/* </View> */}
                        {/* <View style={{ marginHorizontal: 20, marginTop: 20 }}>
                        <Button title="Sign up" color="#841584" bgColor="#243747" width="100%" onPress={this.signUpTap} />
                    </View> */}
                        <View style={{ flexDirection: 'row', paddingTop: 5, }}>
                            <CheckBox
                                onClick={() => {
                                    this.setState({
                                        isChecked: !this.state.isChecked
                                    })
                                }}
                                isChecked={this.state.isChecked}
                            // leftText={"CheckBox"}
                            />
                            {/* <View style={{Width:300}}> */}
                            <Text style={{ fontFamily: 'Montserrat-Regular', marginLeft: 5 }}>{I18n.t("iagreewith")}</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('TermsconditionScreen')}>
                                <Text style={{ fontFamily: 'Montserrat-Regular', color: '#1e90ff', textDecorationLine: 'underline' }}>{I18n.t("termsconditions")}</Text>
                            </TouchableOpacity>
                            <Text style={{ fontFamily: 'Montserrat-Regular', }}>{I18n.t("and")}</Text>

                            {/* </View> */}
                        </View>
                        <View style={{ paddingBottom: 5, marginLeft: 30, paddingTop: 3 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('PrivacypolicyScreen')}>
                                <Text style={{ fontFamily: 'Montserrat-Regular', color: '#1e90ff', textDecorationLine: 'underline' }}>{I18n.t("privacypolicy")}</Text>
                            </TouchableOpacity>
                        </View>
                        {/* <View style={{ width: '100%', paddingTop: 20 }}>
                            <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#1e90ff', height: 50, borderRadius: 10 }} onPress={() => this.props.navigation.navigate("VideoListScreen")}>
                                <Text style={{ color: "white", fontSize: 20 }}>{"Sign up"}</Text>
                            </TouchableOpacity>
                        </View> */}
                        <View style={{ width: '100%', paddingTop: 25 }}>
                            <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 50, borderRadius: 15 }} onPress={() => this.SignUp()}>
                                <Text style={{ color: "white", fontSize: 22, fontFamily: 'Montserrat-Regular', }}>{I18n.t("SignUp")}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginVertical: 25 }}>
                        </View>
                        <Text style={{ textAlign: 'center', color: '#000', fontSize: 18, fontFamily: 'Roboto', paddingBottom: 10, fontFamily: 'Montserrat-Regular' }}>{I18n.t("alreadyhaveaccount")}</Text>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Login")}>
                            <Text style={{ color: "#1e90ff", fontSize: 22, textAlign: 'center' }}>{I18n.t("SignIn")}</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        )
    }
}


const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 30,
        fontWeight: '900',
        color: '#D73951',
        fontFamily: 'Montserrat-Bold',
        marginTop: 15,
        // textTransform: 'uppercase', 
        // textAlign: 'center'
    },
    card: {
        marginHorizontal: 25,
        backgroundColor: '#fff',
        // shadowOffset: { width: 3, height: 3, },
        // shadowColor: 'black',
        // shadowOpacity: 0.3,
        // marginTop: 15,
    },
    formStyle: {
        width: '100%',
        paddingTop: 10,
        // flex: 1,
        paddingBottom: 40,
        backgroundColor: '#fff',
    },
    textInputContainer: {
        // flex: 1, 
        // marginTop: 5,
        borderBottomWidth: 0.5, borderColor: 'lightgray',
        backgroundColor: '#fff',
    },
    textInputItemStyle: {
        backgroundColor: '#fff'
    },
    textInputTitleStyle: {
        color: '#181818',
        fontFamily: 'Montserrat-Regular'
    },
    textInputStyle: {
        height: 45,
        // marginTop: 5,
        marginBottom: 15,
        // width: width - 130,
        borderColor: "gray",
        // paddingLeft: 5,
        // paddingRight: 20,
        // marginRight: 35,
        // color: '#181818',
    },
    textFieldIcon: {
        position: 'absolute', right: 10, top: 25
    },
    iconStyle: {
        fontSize: 35,
        color: '#8A8A8A'
    },

})
