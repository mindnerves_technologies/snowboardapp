import React, { Component } from "react";
import { View, StyleSheet, BackHandler, Image, ScrollView, TouchableOpacity, Keyboard, Button, FlatList, ActivityIndicator, Dimensions } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NetInfo from "@react-native-community/netinfo";
import CommonActivityIndicator from '../Util/ActivityIndicator';
import Api from "../constant/Api";
// import I18n from 'react-native-i18n';
//29:DB:F9:66:7A:D6:A8:5E:0E:9B:47:24:B4:19:E1:AF:E1:E4:5C:EB
import I18n from '../common/localization/index';
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-google-signin/google-signin';
// import fonts from '../assets/fonts';
GoogleSignin.configure({
  webClientId:'250167560616-tjqb792nqan6nrfn9dakumiu7ap2204p.apps.googleusercontent.com',
  // androidClientId: '250167560616-tjqb792nqan6nrfn9dakumiu7ap2204p.apps.googleusercontent.com',
  // webClientId:"662123461681-d8d6k2eqb4dpt276aaq9e5hlq95cfra9.apps.googleusercontent.com",
  offlineAccess:true
})

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      emailerror: null,
      passworderror: null,
      connection_status: false,
      activityIndicator: true,
      showfooter: true,
      showPasswordField: true,
      fcm_token: '',
      userGoogleInfo:{},
      loaded:false
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount = () => {
    // this.willBlur = this.props.navigation.addListener("willBlur", payload =>
    // 	BackHandler.removeEventListener("hardwareBackPress", this.handleBackButtonClick()),
    // );

    this.checkPermission();
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
            onRegister: function (token) {
                console.log("TOKEN:", token);
            },
            onNotification: function (notification) {
                console.log("NOTIFICATION:", notification);
                //   notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
            onAction: function (notification) {
                console.log("ACTION:", notification.action);
                console.log("NOTIFICATION:", notification);
            },
            onRegistrationError: function (err) {
                console.error(err.message, err);
            },
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },

            popInitialNotification: true,
            requestPermissions: true,
        });

    //To get the network state once

    NetInfo.addEventListener(state => {
      if (state.isConnected) {
        this.setState({ connection_status: true });
        //call API
      } else {
        this.setState({ connection_status: false });
      }
    });

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => this.keyboardDidShow(),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => this.keyboardDismiss(),
    );
  }

  async checkPermission() {
    console.log("in checkpermission")
    const enabled = await messaging().hasPermission();
    console.log("in checkpermission after enabled", enabled)
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
}

async getToken() {
  console.log("in getToken")
  // if (!fcmToken) {
  // console.log("check fcmToken")
  fcmToken = await messaging().getToken();

  // if (fcmToken) {
  console.log("check fcmToken", fcmToken)
  this.setState({ fcm_token: fcmToken })
  // }
  // }
}

async requestPermission() {
  console.log("request permission call")
  try {
      await messaging().requestPermission();
      this.getToken();
  } catch (error) {

  }
}

  UNSAFE_componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  keyboardDismiss = () => {
    this.setState({ showfooter: true })
  }

  keyboardDidShow = () => {
    this.setState({ showfooter: false })
  }
  
  showPassword() {
    this.setState({ showPasswordField: !this.state.showPasswordField });
  }

  handleBackButtonClick() {
    this.props.navigation.navigate('HomeScreen')
    //this.refs['newPassword'].blur()
    //  this.props.navigation.goBack(null);
    //this.props.navigation.goBack(null);
    return true;
  }

  SignInn = () => {
    const { email, password } = this.state
    if (email !== "" && password !== "") {
      this.SignInApi();
    }
    else {
      Toast.showWithGravity('Enter email and password', Toast.SHORT, Toast.CENTER);
    }

  }

  SignInApi = () => {
    this.setState({ activityIndicator: false })
    {console.log("this.state.connection_status",this.state.fcm_token,)}

    if (this.state.connection_status) {

      var url = Api.baseUrl + 'login'

      let mUserData =
      {
        email: this.state.email,
        password: this.state.password,
        device_id:this.state.fcm_token,
        device_type:"android"
      };


      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(mUserData),
      })
        .then((response) => response.json())
        .then((response) => {
          console.log(" login response", response)

          if (response.status === 'success') {

            if (response.data !== null) {


              let user_data = {
                userId: response.data.id,
                userFName: response.data.first_name,
                userLName: response.data.last_name,
                userEmail: response.data.email,
                userLevel: response.data.level,
                userDOB: response.data.dob,
                userStatus: response.status,
                userCredit:response.data.credit,
                userTrophy:response.data.trophy,
                userCrown:response.data.crown,
                userToken:response.token,
              }
              console.log("user_data after login object", user_data);
              // await analytics().setUserId(response.data.id.toString())

              // this.LoadViewProfileAPI(response.data.id)

              this.setState({ activityIndicator: true })
              AsyncStorage.setItem('userData', JSON.stringify(user_data))
              // Toast.showWithGravity('Welcome To Snowboard App'+"\n\t\t\t\t\t\t\t\t\t\t\t\t\t🙏🙏", Toast.LONG, Toast.CENTER)
              this.props.navigation.replace('VideoListScreen')

              //this.props.navigation.goBack(null);

            } else {
              Toast.showWithGravity("data is empty", Toast.SHORT, Toast.CENTER);
              this.setState({ activityIndicator: true })
            }


          }

          else {
            this.setState({ activityIndicator: true })
            Toast.showWithGravity('Invalid Email and Password', Toast.SHORT, Toast.CENTER);
          }

        })
        .catch((error) => {
          this.setState({ activityIndicator: true })
          Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
        })
        .done();
    }
    else {
      Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
      this.setState({ activityIndicator: true })
    }
  }

  _focusInput(inputField) {
    this[inputField]._root.focus();
  }

  validateemail = (text) => {
    let reg = /^([\.\_a-zA-Z0-9]+)@([a-zA-Z]+)\.([a-zA-Z]){2,8}$/;
    if (reg.test(text) === false) {
      this.setState({ email: text, emailerror: "@  and . Required" })
      return false;
    }
    else {
      this.setState({ email: text, emailerror: null })
    }
  }

  validatepassword = (text) => {
    let reg = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
    if (reg.test(text) === false) {
      this.setState({ password: text, passworderror: "Min 8 char., at least one uppercase , one lowercase letter, one number and one special character" })
      return false;
    }
    else {
      this.setState({ password: text, passworderror: null })
    }
  }

  SignIn=async()=>{
    try {
      await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    this.setState({ userGoogleInfo:userInfo,loaded:true });
    console.log("userInfo",userInfo)
    } catch (error) {
      console.log("error here",error);
    }
    
  }

  render() {
    const { data1 } = this.state;
    return (

      <View style={{ flex: 1, justifyContent: 'flex-start' }}>
        <View style={{ flexDirection: 'row', paddingVertical: 10, paddingLeft: 25, backgroundColor: '#e3e6f0' }}>
          <Text style={{ fontSize: 25, fontFamily: 'Montserrat-Bold', color: '#00a0e4', marginLeft: 20 }}>{I18n.t("SignIn")}</Text>
        </View>
        <View style={{ paddingHorizontal: 25 }}>
          <View style={{ width: '100%' }}>
            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderWidth: 1, height: 50, borderRadius: 10, marginTop: 25 }}
            onPress={this.SignIn}>
              <Icon type={'MaterialCommunityIcons'} name={"google"} style={{ fontSize: 25, color: '#000' }} />
              <Text style={{ color: "#ooo", fontSize: 20, fontWeight: '900', paddingLeft: 5, fontFamily: 'Montserrat-Regular' }}>{I18n.t("Logingoogle")}</Text>
            </TouchableOpacity>
            {/* <GoogleSigninButton
            onPress={this.SignIn}
            // size={GoogleSigninButton.size.Wide}
            /> */}
          </View>
          
          <Form style={styles.formStyle}>
            <View style={styles.textInputContainer}>
              <Item floatingLabel last style={styles.textInputItemStyle}>
                <Label style={styles.textInputTitleStyle}>{I18n.t('EMailId')}</Label>
                <Input
                  style={styles.textInputStyle}
                  getRef={(input) => this.emailInput = input}
                  value={this.state.email}
                  blurOnSubmit={true}
                  placeholderTextColor={'#fff'}
                  // onChangeText={(text) => this.setState({ email: text })}
                  onChangeText={(text) => this.validateemail(text)}
                  textColor={'#000'}
                  inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                  labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                  returnKeyType={"next"}
                  keyboardType={"email-address"}
                  autoCapitalize='none'
                  onSubmitEditing={() => this._focusInput('passwordInput')}
                />
              </Item>
              <View style={[styles.textFieldIcon, {}]}>
                <Icon type={'FontAwesome'} name={"envelope"} style={[styles.iconStyle, { paddingTop: 15 }]} />
              </View>
            </View>
            {this.state.emailerror !== null && <Text style={{ color: 'red' }}>{this.state.emailerror}</Text>}

            <View style={styles.textInputContainer}>
              <Item floatingLabel last style={styles.textInputItemStyle}>
                <Label style={styles.textInputTitleStyle}>{I18n.t('Password')}</Label>
                <Input
                  style={styles.textInputStyle}
                  getRef={(input) => this.passwordInput = input}
                  value={this.state.password}
                  secureTextEntry={true}
                  // onChangeText={(text) => this.setState({ password: text })}
                  onChangeText={(text) => this.validatepassword(text)}
                  secureTextEntry={this.state.showPasswordField}
                  textColor={'#000'}
                  inputContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                  labelTextStyle={{ paddingLeft: 10, paddingRight: 10 }}
                  returnKeyType={"done"}
                />
              </Item>
              <View style={styles.textFieldIcon}>
                <TouchableOpacity onPress={() => this.showPassword()}>
                <Icon type={'FontAwesome'} 
                name={this.state.showPasswordField ? 'eye-slash' : 'eye'}
                style={[styles.iconStyle, { paddingTop: 15, fontSize: 37, right: 2 }]} />
                </TouchableOpacity>
              </View>
            </View>
            {this.state.passworderror !== null && <Text style={{ color: 'red' }}>{this.state.passworderror}</Text>}
          </Form>
          {/* </View> */}
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')}>
            <Text style={{ textAlign: 'right', color: '#1e90ff', fontSize: 20, fontFamily: 'Montserrat-Regular' }}>{I18n.t('ForgetPassword')}</Text>
          </TouchableOpacity>
          
          <View style={{ width: '100%', paddingTop: 30 }}>
            <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00a0e4', height: 50, borderRadius: 15, }} onPress={() => this.state.activityIndicator && this.SignInn()}>
              <Text style={{ color: "white", fontSize: 22, fontFamily: 'Montserrat-Regular' }}>{this.state.activityIndicator ? "Sign In" : <CommonActivityIndicator></CommonActivityIndicator>}</Text>
            </TouchableOpacity>
          </View>
          
          <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', paddingTop: 30, marginBottom: 20 }}>
          </View>
          <Text style={{ textAlign: 'center', color: '#000', fontSize: 18, fontFamily: 'Montserrat-Regular', paddingBottom: 5 }}>{I18n.t("DontHaveAnAccount")}</Text>

          <TouchableOpacity onPress={() => this.props.navigation.navigate("SignupScreen")}>
            <Text style={{ color: "#1e90ff", fontSize: 22, textAlign: 'center' }}>{I18n.t("SignUp")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 30,
    // fontWeight: '900',
    color: '#696969',
    fontFamily: 'Montserrat-Bold',
    marginTop: 15,
    // textTransform: 'uppercase', 
    // textAlign: 'center'
  },
  // card: {
  //   marginLeft: 20,
  //   marginRight: 20,
  //   backgroundColor: '#fff',
  //   shadowOffset: { width: 3, height: 3, },
  //   shadowColor: 'black',
  //   shadowOpacity: 0.3,
  //   marginTop: 50,
  // },
  formStyle: {
    paddingTop: 20,
    width: '100%',
    // flex: 1,
    paddingBottom: 20,
    backgroundColor: '#fff',
  },
  textInputContainer: {
    // flex: 1, 
    // marginTop: 10,
    borderBottomWidth: 0.5, borderColor: 'lightgray',
    backgroundColor: '#fff',
  },
  textInputItemStyle: {
    backgroundColor: '#fff',

  },
  textInputTitleStyle: {
    color: '#181818',
    fontFamily: 'Montserrat-Regular'
    // marginBottom:10

  },
  textInputStyle: {
    height: 45,
    marginTop: 5,
    marginBottom: 15,
    // width: width - 130,
    borderColor: "gray",
    // paddingLeft: 5,
    // paddingRight: 20,
    // marginRight: 35,
    // color: '#181818',
  },
  textFieldIcon: {
    position: 'absolute', right: 10, top: 25
  },
  iconStyle: {
    fontSize: 30,
    color: '#8A8A8A'
  },

})
