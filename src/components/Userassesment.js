import React, { Component } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, RefreshControl, Button, FlatList, ActivityIndicator, Dimensions, TextInput, Pressable } from "react-native";
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
const { height, width } = Dimensions.get("window");
import Video from 'react-native-video';
import Toast from 'react-native-simple-toast';
import Drawer from '../Style/Drawer';
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import tick from '../assets/images/icons1/tick.png'
import warning from '../assets/images/icons1/warning.png'
import Modal from "react-native-modal";
import * as ImagePicker from 'react-native-image-picker';
import CommonActivityIndicator from '../Util/ActivityIndicator';
import { Rating, AirbnbRating } from 'react-native-ratings';
import Api from "../constant/Api";
import I18n from '../common/localization/index';
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";
// import fonts from '../assets/fonts';


const SNOWFLAKE_IMAGE = require('../Images/snowflake.png')
export default class Userassesment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDrawer: false,
            showExitAppAlert: false,
            showExitAppAlert1: false,
            CoachingAlert: false,
            connection_status: false,
            flakecolour: false,
            flakecolour1: false,
            flakecolour2: false,
            count: "0",
            refreshing: false,
            selfsassdata: null,
            //Video Upload part
            text: "",               // to store text input
            textMessage: false,     // text input message flag
            loading: false,         // manage loader
            image: null,              // store image
            video: null,
            showUploadModal: false,
            uploadStatus: false,
        };
    }

    componentDidMount() {
        // this.callApi();
        // this.CacheProductCallApi();
        this.checkPermission();
        // Must be outside of any component LifeCycle (such as `componentDidMount`).
        PushNotification.configure({
            onRegister: function (token) {
                console.log("TOKEN:", token);
            },
            onNotification: function (notification) {
                console.log("NOTIFICATION:", notification);
                //   notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
            onAction: function (notification) {
                console.log("ACTION:", notification.action);
                console.log("NOTIFICATION:", notification);
            },
            onRegistrationError: function (err) {
                console.error(err.message, err);
            },
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },

            popInitialNotification: true,
            requestPermissions: true,
        });

        Util.getAsyncStorage('AssessmentLevelData').then((data) => {
            console.log("AssessmentLevelData", data)
            if (data !== null) {
                this.setState({ acceptchalengedata: data.assesment_level_Data, assesment_video_id: data.assesment_video_id, level: data.level })
            }
        })

        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data, getuserData: data, userToken1: data.userToken })
                this.HeaderApi(data);
            }
        })

        NetInfo.addEventListener(state => {
            if (state.isConnected) {
                this.setState({ connection_status: true });
                //call API

            } else {
                this.setState({ connection_status: false });
            }
        });

    }

    async checkPermission() {
        console.log("in checkpermission")
        const enabled = await messaging().hasPermission();
        console.log("in checkpermission after enabled", enabled)
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async getToken() {
        console.log("in getToken")
        // if (!fcmToken) {
        console.log("check fcmToken")
        fcmToken = await messaging().getToken();

        // if (fcmToken) {
        console.log("check fcmToken", fcmToken)
        // }
        // }
    }

    async requestPermission() {
        console.log("request permission call")
        try {
            await messaging().requestPermission();
            this.getToken();
        } catch (error) {

        }
    }

    HeaderApi = (data) => {
        // if (this.state.connection_status) {
        console.log("data.userToken,", data.userToken)
        var url = 'http://139.162.159.9/snowboard/api/crowntrophydetails'
        console.log("after url", data.userToken)

        fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': "Bearer " + data.userToken,
                'Content-Type': 'application/json',
            },
            // body: JSON.stringify(mUserData),
        })
            // .then((response) =>{
            //     // response.json()
            //     console.log("response assreqlist", response)
            // } )
            // // .then((response) => {
            // //     console.log("response assreqlist", response)
            // //     this.setState({ assreqlistdata: response.data.data_assessment_request })
            // // })
            .then((response) => response.json())
            .then((response) => {
                console.log("response headerapi", response)
                this.setState({ headerapidata: response.data })
            })
            .catch((error) => {
                console.log("error", error)
                this.setState({ showProgressBar: false })
                Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
            })
            .done();
        // }
        // else {
        //     Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
        //     this.setState({ showProgressBar: false })
        // }
    }

    clickHamburgerMenu = () => {
        this.setState({ showDrawer: !this.state.showDrawer })
    }
    AskAssestment = () => {
        this.setState({ showExitAppAlert: true })
        return true
        //this.props.navigation.goBack(null);
    };
    AskAssestmentprofessional = () => {
        this.AskAssestmentprofessionalApi();
        this.setState({ showExitAppAlert1: true })
        return true
        //this.props.navigation.goBack(null);
    };
    Coachingprofessional = () => {
        this.AskAssestmentprofessionalApi();
        this.setState({ CoachingAlert: true })
        return true
        //this.props.navigation.goBack(null);
    };
    AskAssestmentprofessionalApi = () => {
        if (this.state.connection_status) {

            var url = Api.baseUrl + 'creditsetting'

            let mUserData =
            {
                // "level": this.props.navigation.state.params.param_level,
            };

            fetch(url, {
                method: 'GET',
                headers: {
                    // 'Authorization': "Bearer " + this.state.userToken,
                    'Content-Type': 'application/json',
                },
                // body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("response challenge", response)
                    this.setState({ creditdata: response })
                })
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong1', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }



    Hidepopup = (item) => {
        console.log("item", item);
        console.log("this.state.count", this.state.count);
        console.log("this.state.Comment", this.state.Comment);
        this.selfassessmentApi();
        // Toast.showWithGravity(" Rating and Comment submitted", Toast.SHORT, Toast.CENTER)
        this.setState({ showExitAppAlert: false })
        return true
        //this.props.navigation.goBack(null);
    };
    _onRefresh = () => {
        console.log("In refresh")
        this.setState({ refreshing: true });
        Util.getAsyncStorage('AssessmentLevelData').then((data) => {
            console.log("AssessmentLevelData", data)
            if (data !== null) {
                this.setState({ acceptchalengedata: data.assesment_level_Data, assesment_video_id: data.assesment_video_id, level: data.level })
            }
        })

        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data, getuserData: data, userToken1: data.userToken })
                this.HeaderApi(data);
            }
        })
        { console.log("after api calling refresh", this.state.refreshing) }
        this.setState({ refreshing: false });
    }

    selfassessmentApi = () => {

        if (this.state.connection_status) {

            var url = Api.baseUrl + 'create_selfassesment_request'

            { console.log("this.state.Comment", this.state.Comment) }
            { console.log("this.state.count in api", this.state.count) }
            { console.log("this.state.assesment_video_id", this.state.assesment_video_id, this.state.userToken1) }

            let mUserData =
            {
                "description": this.state.Comment,
                "assesment_level": this.state.level,
                "assesment_video_id": this.state.assesment_video_id,
                "star": this.state.count,
                "assessment": "no"
            };

            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + this.state.userToken1,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("selfassesment response", response)
                    this.setState({ selfsassdata: response.data })
                    if (response.status == 'success') {
                        Toast.showWithGravity("Your assessment request has been submitted successfully", Toast.SHORT, Toast.CENTER)
                        this.props.navigation.navigate("ChallengeScreen")
                    }
                    else {
                        Toast.showWithGravity("Your assessment request has not been submitted", Toast.SHORT, Toast.CENTER)
                    }
                    // Toast.showWithGravity("hii", Toast.SHORT, Toast.CENTER)
                })
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }

    assessmentrequest = () => {
        {
            { console.log("creditdata") }
            this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.creditdata !== "" && this.state.creditdata !== undefined
            if (this.state.headerapidata.credit >= this.state.creditdata.data.assisment_request) {
                this.assessmentrequestApi();
                // Toast.showWithGravity("Request Send Successfully", Toast.SHORT, Toast.CENTER)
            }
            else {
                // this.setState({ showExitAppAlert1: false })
                Toast.showWithGravity("You don't have sufficient credit for assessment", Toast.SHORT, Toast.CENTER)
            }
        }
        this.setState({ showExitAppAlert1: false })
        return true
    };
    assessmentrequestApi = () => {

        if (this.state.connection_status) {

            var url = Api.baseUrl + 'create_assesment_request'
            { console.log("this.state.level", this.state.level) }
            { console.log("this.state.assesment_video_id", this.state.assesment_video_id, this.state.userToken1) }

            let mUserData =
            {
                // "description":"test",
                "assesment_level": this.state.level,
                "assesment_video_id": this.state.assesment_video_id,
                // "star":" ",
                "assessment": "yes"
            };

            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + this.state.userToken1,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("assesment response", response)
                    this.setState({ assrequest: response })
                    if (response.status == 'success') {
                        this.setState({ showExitAppAlert1: false })
                        Toast.showWithGravity("Your assessment request has been submitted successfully", Toast.SHORT, Toast.CENTER)
                        this.props.navigation.navigate("ChallengeScreen")
                    }
                    else {
                        Toast.showWithGravity("Your assessment request has not been submitted", Toast.SHORT, Toast.CENTER)
                    }
                    // Toast.showWithGravity("hii", Toast.SHORT, Toast.CENTER)
                })
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }

    coachingrequest = () => {
        {
            { console.log("creditdata", this.state.creditdata) }
            this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.creditdata !== "" && this.state.creditdata !== undefined
            if (this.state.headerapidata.credit >= this.state.creditdata.data.coaching_request) {
                this.coachingrequestApi();
                // Toast.showWithGravity("Request Send Successfully", Toast.SHORT, Toast.CENTER)
            }
            else {
                // this.setState({ showExitAppAlert1: false })
                Toast.showWithGravity("You don't have sufficient credit for coaching", Toast.SHORT, Toast.CENTER)
            }
        }
        this.setState({ CoachingAlert: false })
        return true
    };

    coachingrequestApi = () => {
        if (this.state.connection_status) {

            var url = Api.baseUrl + 'coaching'


            let mUserData =
            {
                "comment": "ss",
                "assesment_level": this.state.level,
                "assesment_video_id": this.state.assesment_video_id
            };

            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': "Bearer " + this.state.userToken1,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(mUserData),
            })
                .then((response) => response.json())
                .then((response) => {
                    console.log("coaching response", response)
                    this.setState({ coachrequest: response })
                    if (response.status == 'success') {
                        Toast.showWithGravity("Your coaching request has been submitted successfully", Toast.SHORT, Toast.CENTER)
                        this.props.navigation.navigate("ChallengeScreen")
                    }
                    else {
                        Toast.showWithGravity("Your coaching request has not been submitted", Toast.SHORT, Toast.CENTER)
                    }
                })
                .catch((error) => {
                    console.log("error", error)
                    this.setState({ showProgressBar: false })
                    Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
                })
                .done();
        }
        else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }
    }

    selectVideo = () => {
        ImagePicker.launchImageLibrary({ mediaType: 'video', includeBase64: true }, (response) => {
            console.log("response385", response);
            this.setState({ video: response.assets }, () => { this.uploadVideoApi() });
        })
    }

    uploadVideoApi = () => {
        if (this.state.connection_status) {

            if (this.state.video) {
                this.setState({ showProgressBar: true })
                let formData = new FormData
                formData.append('video', {
                    name: 'name.mp4',
                    uri: this.state.video[0].uri,
                    type: 'video/mp4'
                })
                {this.state.selfsassdata !==null ? formData.append('self_assessment_id', this.state.selfsassdata):formData.append('self_assessment_id', null)}

                var url = Api.baseUrl + 'uploadselfassesmentvideo'
                console.log('440', url, formData)

                fetch(url, {
                    method: 'POST',
                    headers: {
                        'Authorization': "Bearer " + this.state.userToken1,
                        'Content-Type': 'multipart/form-data',
                    },
                    body: formData,
                })
                    .then((response) => response.json())
                    .then((response) => {
                        if (response.status === 'success') {
                            console.log("451", response)
                            this.setState({ showProgressBar: false, showUploadModal: true, uploadStatus: true })
                        } else {
                            this.setState({ showProgressBar: false, showUploadModal: true, uploadStatus: false })
                        }
                    })
                    .catch((error) => {
                        console.log("error458", error)
                        this.setState({ showProgressBar: false, showUploadModal: true, uploadStatus: false })
                        // Toast.showWithGravity('Something went wrong', Toast.SHORT, Toast.CENTER);
                    })
                    .done();
            }
        } else {
            Toast.showWithGravity("Please check your internet connection", Toast.SHORT, Toast.CENTER);
            this.setState({ showProgressBar: false })
        }

    }

    uploadVideoModal = () => {
        return (
            <View>
                <Modal
                    animationType="fade"
                    isVisible={true}>
                    <View style={{ width: width / 1.1, backgroundColor: '#ffff', alignItems: 'center', borderRadius: 10, paddingVertical: 10 }}>
                        {this.state.uploadStatus ? <View style = {{alignItems:'center'}}>
                            <Image style={{ width: 30, height: 30, marginBottom: 5 }} source={tick} />
                            <Text style={{ padding: 5, textAlign: 'center', fontSize: 16, fontFamily: 'Montserrat-Regular', marginBottom: 8 }}>{'Your video is uploaded'}</Text>
                        </View>
                            : <View style = {{alignItems:'center'}}>
                                <Image style={{ width: 30, height: 30, marginBottom: 5 }} source={warning} />
                                <Text style={{ padding: 5, textAlign: 'center', fontSize: 16, fontFamily: 'Montserrat-Regular', marginBottom: 8 }}>{'Something went wrong, Try again later'}</Text></View>}
                        <TouchableOpacity
                            onPress={() => this.setState({ showUploadModal: false })}
                            style={{ backgroundColor: '#00A0E4', marginTop: 10, borderRadius: 55, marginLeft: 0 }}>
                            <Text style={{ fontWeight: '900', fontSize: 16, paddingHorizontal: 30, paddingVertical: 10, color: '#fff' }}>
                                {'Ok'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        )
    }

    starhighlight = () => {
        console.log("inside anonymous function")
        if (this.state.acceptchalengedata?.assesment_details == null || this.state.acceptchalengedata?.assesment_details == "") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 70 }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.adminstar == "") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 70 }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.adminstar == "0") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 70 }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.adminstar == "1") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 70, color: 'yellow' }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.adminstar == "2") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 70, color: 'yellow' }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10, color: 'yellow' }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.adminstar == "3") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 70, color: 'yellow' }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10, color: 'yellow' }]} />
                    <Icon type={'FontAwesome'} name={"star"} style={[styles.iconStyle, { paddingLeft: 10, color: 'yellow' }]} />
                </View>
            )
        }

        return true
    };

    snowflakehighlight = () => {
        console.log("inside anonymous function")
        if (this.state.acceptchalengedata?.assesment_details == null) {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 40 }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.snowflex == null) {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 40 }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.snowflex == "0") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 40 }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.snowflex == "1") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 40, color: 'skyblue' }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.snowflex == "2") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 40, color: 'skyblue' }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10, color: 'skyblue' }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.snowflex == "3") {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 40, color: 'skyblue' }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10, color: 'skyblue' }]} />
                    <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10, color: 'skyblue' }]} />
                </View>
            )
        }

        return true
    };

    coachingrequeststatus = () => {
        console.log("coachingrequeststatus")
        if (this.state.acceptchalengedata !== "" && this.state.acceptchalengedata !== undefined && this.state.acceptchalengedata.coachingdetails == null) {
            return (
                <View>
                    <TouchableOpacity style={{ backgroundColor: '#00a0e4', justifyContent: 'center', height: 50, width: '100%', borderRadius: 10, }} onPress={() => this.Coachingprofessional()}>
                        <Text style={{ fontSize: 20, color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{I18n.t("coachingprofessional")}</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        if (this.state.acceptchalengedata?.coachingdetails?.status == "pending") {
            return (
                <View style={{ backgroundColor: '#c0c0c0', justifyContent: 'center', height: 50, width: '100%', borderRadius: 10, }}
                >
                    <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{I18n.t("coachingpendingprofessional")}</Text>
                    {/* {this.state.acceptchalengedata?.coachingdetails?.status} */}
                </View>
            )
        }
        if (this.state.acceptchalengedata?.coachingdetails?.status == "accept") {
            return (
                <View style={{ flexDirection: 'row', paddingTop: 5 }}>
                    <TouchableOpacity style={{ backgroundColor: '#00a0e4', justifyContent: 'center', height: 50, width: '100%', borderRadius: 10, }}
                        onPress={() => this.props.navigation.navigate("ChatScreen")}
                    >
                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2, fontSize: 20 }}>{I18n.t("chat")}</Text>
                    </TouchableOpacity>
                </View>
            )
        }

        if (this.state.acceptchalengedata?.coachingdetails?.status == "completed") {
            return (
                <View>
                    <TouchableOpacity style={{ backgroundColor: '#00a0e4', justifyContent: 'center', height: 50, width: '100%', borderRadius: 10, }} onPress={() => this.Coachingprofessional()}>
                        <Text style={{ fontSize: 20, color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{I18n.t("coachingprofessional")}</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        return true
    };

    professionalassesmentreueststatus = () => {
        console.log("professionalassesmentreueststatus")
        if (this.state.acceptchalengedata !== "" && this.state.acceptchalengedata !== undefined && this.state.acceptchalengedata.assesment_details == null || this.state.acceptchalengedata?.assesment_details?.assessment_status == null) {
            return (
                <View>
                    <TouchableOpacity style={{ backgroundColor: '#D73951', justifyContent: 'center', height: 50, width: 150, borderRadius: 10, }} onPress={() => this.AskAssestmentprofessional()}>
                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{I18n.t("professionalassessment")}</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.assessment_status == "pending") {
            return (
                <View style={{ backgroundColor: '#c0c0c0', justifyContent: 'center', height: 50, width: 150, borderRadius: 10, }}
                >
                    <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{I18n.t("assesspending")}
                    </Text>
                </View>
            )
        }
        if (this.state.acceptchalengedata?.assesment_details?.assessment_status == "assess") {
            return (
                <View style={{ backgroundColor: '#c0c0c0', justifyContent: 'center', height: 50, width: 150, borderRadius: 10, }}
                >
                    <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{I18n.t("assessmentassessed")}
                    </Text>
                </View>
            )
        }
        return true
    };

    render() {
        { console.log("this.state.acceptchalengedata", this.state.acceptchalengedata) }
        return (
            <Container>
                <View style={{ flex: 1, }}>
                    <View style={{
                        width: '100%', justifyContent: 'space-between',
                        flexDirection: 'row', backgroundColor: '#e3e6f0', paddingVertical: 12, paddingHorizontal: 15, borderBottomWidth: 1
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("MyAccountScreen")}>
                            <Icon type={'FontAwesome'} name={"user-circle"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        </TouchableOpacity>
                        {this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.headerapidata.crown !== "false" && this.state.headerapidata.crown !== null ?
                            <Icon type={'MaterialCommunityIcons'} name={"crown"} style={[styles.iconStyle, { paddingTop: 4, color: this.state.headerapidata.crown }]} />
                            :
                            <Icon type={'MaterialCommunityIcons'} name={"crown"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        }
                        {this.state.headerapidata !== undefined && this.state.headerapidata !== "" && this.state.headerapidata.thropy !== "false" && this.state.headerapidata.thropy !== null ?
                            <Icon type={'FontAwesome'} name={"trophy"} style={[styles.iconStyle, { paddingTop: 4, color: this.state.headerapidata.thropy }]} />
                            :
                            <Icon type={'FontAwesome'} name={"trophy"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        }
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CreditPlanScreen')}>
                            {this.state.headerapidata !== undefined && this.state.headerapidata !== "" &&
                                <Text style={{ fontSize: 15 }}>{this.state.headerapidata.credit}</Text>
                            }
                            <Icon type={'FontAwesome'} name={"copyright"} style={styles.creditlogoStyle} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.clickHamburgerMenu()}>
                            <Icon type={'FontAwesome'} name={"bars"} style={[styles.iconStyle, { paddingTop: 4 }]} />
                        </TouchableOpacity>
                    </View>

                    {/* <View style={{ flexDirection: 'row', paddingVertical: 20, paddingHorizontal: 10, alignItems: 'flex-start', }}>
                        <Text style={styles.titleStyle}>{"Title Of Challenge"}</Text>
                    </View> */}
                    <View style={{ justifyContent: 'center', height: height }}>
                        {this.state.showProgressBar ? <CommonActivityIndicator /> : <ScrollView
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            {this.state.acceptchalengedata !== undefined && this.state.acceptchalengedata !== null && this.state.acceptchalengedata.admin_assesment_video_details !== "" &&
                                <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15 }}>
                                    <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 10 }} onPress={() => this.props.navigation.navigate('ChallengeScreen', { fromCome: new Date().getTime() })} />
                                    <Text style={{ fontSize: 29, fontFamily: 'Montserrat-Bold', color: '#00a0e4', paddingLeft: 10 }}>{this.state.acceptchalengedata.admin_assesment_video_details.title}</Text>
                                </View>
                            }
                            {this.state.acceptchalengedata !== undefined && this.state.acceptchalengedata !== null && this.state.acceptchalengedata.admin_assesment_video_details !== "" &&
                                <Video source={{ uri:"http://139.162.159.9/snowboard/" + this.state.acceptchalengedata.admin_assesment_video_details.url }}
                                    style={styles.videostyle}
                                    repeat={true}
                                    ref={(ref) => {
                                        this.player = ref
                                    }}
                                    resizeMode='cover'
                                    onBuffer={this.onBuffer}
                                    onError={this.videoError}
                                    poster={'https://media.istockphoto.com/vectors/illustration-of-simple-black-video-player-design-template-for-laptop-vector-id1217988423?k=20&m=1217988423&s=612x612&w=0&h=PR7L7GrPienKTe1-eXPf-Yenm0ATDPwJeOc82ZTygbA='}
                                    posterResizeMode={'cover'}
                                    controls={true}
                                    paused={true}
                                />
                                // :
                                // <Video source={{ uri: 'https://media.istockphoto.com/videos/skier-spraying-snow-at-the-camera-video-id515900616' }}
                                //     style={styles.videostyle}
                                //     repeat={true}
                                //     ref={(ref) => {
                                //         this.player = ref
                                //     }}
                                //     resizeMode='cover'
                                //     onBuffer={this.onBuffer}
                                //     onError={this.videoError}
                                //     poster={'https://media.istockphoto.com/vectors/illustration-of-simple-black-video-player-design-template-for-laptop-vector-id1217988423?k=20&m=1217988423&s=612x612&w=0&h=PR7L7GrPienKTe1-eXPf-Yenm0ATDPwJeOc82ZTygbA='}
                                //     posterResizeMode={'cover'}
                                //     controls={true}
                                //     paused={true}
                                // />

                            }
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity style={{ backgroundColor: '#00a0e4', justifyContent: 'center', height: 50, width: '82%', marginTop: 15, borderRadius: 10 }}
                                    onPress={() => this.selectVideo()}
                                // onPress={() => this.props.navigation.navigate("UserassesmentScreen")}
                                >
                                    <Text style={{ fontSize: 18, color: "#FFF", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{I18n.t("uploadvideo")}</Text>
                                </TouchableOpacity>
                            </View>

                            {this.state.showUploadModal && this.uploadVideoModal()}

                            {console.log("this.state.acceptchalengedata?.assesment_details?.adminstar", this.state.acceptchalengedata?.assesment_details?.adminstar)}
                            {console.log("this.state.acceptchalengedata?.assesment_details?.snowflex", this.state.acceptchalengedata?.assesment_details?.snowflex)}
                            <View style={{ width: '100%', flexDirection: 'row', backgroundColor: '#fff', paddingVertical: 20 }}>

                                {/* <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 40 }]} />
                        <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                        <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} /> */}
                                {this.state.acceptchalengedata !== undefined &&
                                    this.snowflakehighlight()
                                }
                                {this.state.acceptchalengedata !== undefined &&
                                    this.starhighlight()
                                }
                            </View>
                            {/* {console.log("this.state.acceptchalengedata.assesment_details now",this.state.acceptchalengedata.assesment_details)} */}
                            <View style={{ flexDirection: 'row', paddingTop: 10, justifyContent: 'space-between', paddingHorizontal: 40 }}>
                                {this.state.acceptchalengedata !== "" && this.state.acceptchalengedata !== undefined && this.state.acceptchalengedata.assesment_details?.snowflex == null ?
                                    <TouchableOpacity style={{ backgroundColor: '#717384', justifyContent: 'center', height: 50, width: 150, borderRadius: 10, }} onPress={() => this.AskAssestment()}>
                                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{I18n.t("selfassessment")}</Text>
                                    </TouchableOpacity>
                                    :
                                    <View style={{ backgroundColor: '#c0c0c0', justifyContent: 'center', height: 50, width: 150, borderRadius: 10, }}
                                    // onPress={() => this.AskAssestment()}
                                    >
                                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{I18n.t("selfassessment")}</Text>
                                    </View>
                                }

                                {
                                    this.professionalassesmentreueststatus()
                                }
                                {/* {this.state.acceptchalengedata !== "" && this.state.acceptchalengedata !== undefined && this.state.acceptchalengedata.assesment_details == null || this.state.acceptchalengedata?.assesment_details?.assessment_status == null ?
                                    <TouchableOpacity style={{ backgroundColor: '#D73951', justifyContent: 'center', height: 50, width: 150, borderRadius: 10, }} onPress={() => this.AskAssestmentprofessional()}>
                                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{"Assessment by Professional"}</Text>
                                    </TouchableOpacity>
                                    :
                                    <View style={{ backgroundColor: '#c0c0c0', justifyContent: 'center', height: 50, width: 150, borderRadius: 10, }}
                                    >
                                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{"Assessment "}
                                            {this.state.acceptchalengedata?.assesment_details?.assessment_status}
                                        </Text>
                                    </View>
                                } */}

                            </View>
                            <View style={{ paddingTop: 20, paddingHorizontal: 40 }}>
                                {/* {this.state.acceptchalengedata !== "" && this.state.acceptchalengedata !== undefined && this.state.acceptchalengedata.coachingdetails == null ?
                                    <View>
                                        <TouchableOpacity style={{ backgroundColor: '#00a0e4', justifyContent: 'center', height: 50, width: '100%', borderRadius: 10, }} onPress={() => this.Coachingprofessional()}>
                                            <Text style={{ fontSize: 20, color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{"Coaching By Professional"}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View>
                                        {this.state.acceptchalengedata?.coachingdetails?.status !== "accept" ?
                                            <View style={{ backgroundColor: '#c0c0c0', justifyContent: 'center', height: 50, width: '100%', borderRadius: 10, }}
                                            >
                                                <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2 }}>{"Coaching "}
                                                    {this.state.acceptchalengedata?.coachingdetails?.status}
                                                    {" By Professional"}</Text>
                                            </View>
                                            :
                                            <View>
                                                <View style={{ flexDirection: 'row', paddingTop: 5 }}>

                                                    <TouchableOpacity style={{ backgroundColor: '#00a0e4', justifyContent: 'center', height: 50, width: '100%', borderRadius: 10, }}
                                                        onPress={() => this.props.navigation.navigate("ChatScreen")}
                                                    >
                                                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular', padding: 2, fontSize: 20 }}>{"Chat"}</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        }
                                    </View>
                                } */}
                                {
                                    this.coachingrequeststatus()
                                }
                            </View>
                            {/* <FlatList
                        data={data}
                        renderItem={this._renderBannerItem.bind(this)}
                        keyExtractor={(item, index) => index.toString()}
                        extraData={this.state}
                        ListEmptyComponent={this.ListEmptyComponent}
                    /> */}
                        </ScrollView>}
                    </View>
                </View>
                {this.state.showDrawer ?
                    <Drawer global_navigation={this.props.navigation} data={this} />
                    : null
                }
                <Modal
                    animationIn="slideInLeft"
                    animationOut="slideOutLeft"
                    animationInTiming={500}
                    animationOutTiming={500}
                    isVisible={this.state.showExitAppAlert}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>

                        <View style={{ width: '85%', backgroundColor: 'white', borderRadius: 5 }}>
                            <TouchableOpacity style={{ left: 265 }} onPress={() => this.setState({ showExitAppAlert: false })}>
                                <Icon type={'MaterialCommunityIcons'} name={"close"} style={[styles.iconStyle, {}]} />
                            </TouchableOpacity>
                            <View style={{ justifyContent: 'center', flexDirection: 'row', backgroundColor: '#fff', paddingVertical: 20 }}>
                                <TouchableOpacity onPress={() => this.setState({ flakecolour: true, flakecolour1: false, flakecolour2: false, count: "1" })}>
                                    {this.state.flakecolour ?
                                        <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { color: 'skyblue' }]} />
                                        :
                                        <Icon type={'FontAwesome5'} name={"snowflake"} style={styles.iconStyle} />
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ flakecolour: true, flakecolour1: true, flakecolour2: false, count: "2" })}>
                                    {this.state.flakecolour1 ?
                                        <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10, color: 'skyblue' }]} />
                                        :
                                        <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                                    }
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ flakecolour: true, flakecolour1: true, flakecolour2: true, count: "3" })}>
                                    {this.state.flakecolour2 ?
                                        <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10, color: 'skyblue' }]} /> :
                                        <Icon type={'FontAwesome5'} name={"snowflake"} style={[styles.iconStyle, { paddingLeft: 10 }]} />
                                    }
                                </TouchableOpacity>
                                {/* <Rating
                                    type='custom'
                                    ratingImage={SNOWFLAKE_IMAGE}
                                    ratingColor='#3498db'
                                    ratingBackgroundColor='#c8c7c8'
                                    ratingCount={3}
                                    imageSize={30}
                                    // onFinishRating={this.ratingCompleted}
                                    // style={{ paddingVertical: 10 }}
                                /> */}
                            </View>
                            {/* <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 15, color: '#6c757d', }}>{'Comment'}</Text> */}
                            <Text style={{ fontSize: 20, color: '#00a0e4', left: 30 }}>{I18n.t('note')}</Text>
                            <View style={{ alignItems: 'center' }}>

                                <View
                                    style={[{
                                        backgroundColor: '#fff',
                                        // alignSelf: 'left',
                                        // right:70,
                                        borderWidth: 1,
                                        borderColor: "gray",
                                        height: 80,
                                        borderRadius: 5,
                                        width: '80%',
                                        // alignSelf: 'center',
                                        marginVertical: 7,
                                    }]}>

                                    <TextInput
                                        style={{
                                            height: 80,
                                            fontFamily: "Montserrat-Regular",
                                            fontSize: 16,
                                            color: "#000",
                                            // paddingLeft: 15,
                                        }}
                                        placeholder={I18n.t("entercomment")}
                                        placeholderTextColor={'#7E7E7E'}
                                        // // placeholderStyle={{ textAlign: 'center' }}
                                        textAlignVertical='top'
                                        autoFocus={true}
                                        underlineColorAndroid='transparent'
                                        // numberOfLines={2}
                                        multiline={true}
                                        // returnKeyType={"next"}
                                        ref="Comment"
                                        //value={this.state.EmailId} 
                                        onChangeText={(Comment) => this.setState({ Comment: Comment })}
                                        value={this.state.Comment}
                                    // allowFontScaling={false} 
                                    />
                                </View>
                                <View style={{ paddingVertical: 10 }}>
                                    <TouchableOpacity style={{ backgroundColor: '#D73951', justifyContent: 'center', height: 40, width: 150, borderRadius: 10 }}
                                        onPress={(item) => this.Hidepopup(item)}
                                    >
                                        <Text style={{ color: "white", textAlign: 'center', fontFamily: 'Montserrat-Regular' }}>{I18n.t("Submit")}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* </View> */}
                        </View>
                    </View>
                </Modal>

                {/* Assessment by professional Popup start*/}

                <Modal
                    animationIn="slideInLeft"
                    animationOut="slideOutLeft"
                    animationInTiming={500}
                    animationOutTiming={500}
                    isVisible={this.state.showExitAppAlert1}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        {this.state.creditdata !== "" && this.state.creditdata !== undefined &&
                            <View style={{ width: '100%', backgroundColor: 'white', borderRadius: 5 }}>

                                <Text
                                    style={{
                                        fontFamily: 'Montserrat-Regular', fontSize: 18, color: 'black',
                                        marginTop: 15, marginLeft: 20, textAlign: 'left', marginRight: 5
                                    }}>
                                    {I18n.t('itwillcharge') + this.state.creditdata.data.assisment_request + I18n.t('creditsforassessment')}
                                </Text>

                                <Text
                                    style={{
                                        fontFamily: 'Montserrat-Regular', fontSize: 18, color: 'black',
                                        marginTop: 15, marginLeft: 20, textAlign: 'left', right: 5
                                    }}>
                                    {I18n.t('doyouwantsubmitassessmentrequest')}
                                </Text>

                                <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 15, justifyContent: 'flex-end' }}>
                                    <Pressable
                                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                        onPress={() => {
                                            this.setState({ showExitAppAlert1: false })
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: 'Montserrat-Regular', fontSize: 16, color: 'black',
                                                marginRight: 30,
                                            }}>
                                            {I18n.t('no')}
                                        </Text>
                                    </Pressable>

                                    <Pressable
                                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                        onPress={() => {
                                            this.assessmentrequest();
                                            // this.setState({ showExitAppAlert1: false })
                                            // Toast.showWithGravity(" Assessment Request submitted", Toast.SHORT, Toast.CENTER)
                                            // this.props.navigation.navigate('CreditPlanScreen')
                                            // BackHandler.exitApp();
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: 'Montserrat-Regular', fontSize: 16, color: 'black',
                                                marginRight: 30,
                                            }}>
                                            {I18n.t('yes')}
                                        </Text>
                                    </Pressable>
                                </View>
                            </View>
                        }
                    </View>
                </Modal>
                {/* Coaching by professional Popup start*/}
                <Modal
                    animationIn="slideInLeft"
                    animationOut="slideOutLeft"
                    animationInTiming={500}
                    animationOutTiming={500}
                    isVisible={this.state.CoachingAlert}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        {this.state.creditdata !== "" && this.state.creditdata !== undefined &&
                            <View style={{ width: '100%', backgroundColor: 'white', borderRadius: 5 }}>

                                <Text
                                    style={{
                                        fontFamily: 'Montserrat-Regular', fontSize: 18, color: 'black',
                                        marginTop: 15, marginLeft: 20, textAlign: 'left', marginRight: 5
                                    }}>
                                    {I18n.t('itwillcharge') + this.state.creditdata.data.coaching_request + I18n.t('creditsforcoaching')}
                                </Text>

                                <Text
                                    style={{
                                        fontFamily: 'Montserrat-Regular', fontSize: 18, color: 'black',
                                        marginTop: 15, marginLeft: 20, textAlign: 'left'
                                    }}>
                                    {I18n.t('doyouwanttosubmitcoachingrequest')}
                                </Text>

                                <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 15, justifyContent: 'flex-end' }}>
                                    <Pressable
                                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                        onPress={() => {
                                            this.setState({ CoachingAlert: false })
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: 'Montserrat-Regular', fontSize: 16, color: 'black',
                                                marginRight: 30,
                                            }}>
                                            {I18n.t('no')}
                                        </Text>
                                    </Pressable>

                                    <Pressable
                                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                        onPress={() => {
                                            this.coachingrequest();
                                            // this.setState({ CoachingAlert: false })
                                            // Toast.showWithGravity(" Coaching Request submitted", Toast.SHORT, Toast.CENTER)
                                            // this.props.navigation.navigate('CreditPlanScreen')
                                            // BackHandler.exitApp();
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: 'Montserrat-Regular', fontSize: 16, color: 'black',
                                                marginRight: 30,
                                            }}>
                                            {I18n.t('yes')}
                                        </Text>
                                    </Pressable>
                                </View>
                            </View>
                        }
                    </View>
                </Modal>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    profileImage: {
        width: 180,
        height: 210,
        borderRadius: 10,
        marginLeft: 6
    },
    // profileName: {
    //     marginLeft: 6,
    //     fontSize: 20,
    //     color: '#181818'
    // },
    msgText: {
        marginLeft: 6,
        fontSize: 15,
        color: '#181818'
    },
    userRow: {
        // borderBottomWidth: StyleSheet.hairlineWidth,
        // borderBottomColor: '#171717',
        paddingVertical: 10,
        borderWidth: StyleSheet.hairlineWidth,
    },
    titleStyle: {
        fontSize: 30,
        color: '#D73951',
        fontFamily: 'Montserrat-Bold',
        // textAlign:'center',
        // justifyContent:'center',
    },
    // backgroundVideo: {
    //     position: 'absolute',
    //     top: 0,
    //     left: 0,
    //     bottom: 0,
    //     right: 0,
    // },
    videostyle: {
        // width: 150,
        // height: 200,
        // // borderRadius: 10,
        // // marginLeft: 6
        width: '95%',
        height: 350,
        borderRadius: 10,
        marginHorizontal: 10,
        resizeMode: 'cover'
        // position: 'absolute',
    },

    iconStyle: {
        fontSize: 35,
        color: '#696969',
        // marginLeft: 25
    },
    creditlogoStyle: {
        fontSize: 20,
        color: '#696969',
        // marginLeft: 25
    },

})
