import React, { Component } from 'react';
import { View, Keyboard, StyleSheet, Dimensions, Alert, TouchableOpacity, Button } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Text, Form, Item, Input, Label, Icon } from 'native-base';
import I18n from '../common/localization/index';

export default class TermsconditionScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 15, backgroundColor: '#e3e6f0' }}>
                    <Icon type={'AntDesign'} name={'arrowleft'} style={{ fontSize: 30, color: '#181818', marginTop: 5 }} onPress={() => this.props.navigation.goBack()} />
                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#00a0e4', paddingLeft: 20 }}>{I18n.t("termsconditions")}</Text>
                </View>
            </View>
        )
    }
}