import React, { Component } from "react";
import { StyleSheet, Dimensions, } from "react-native";
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
// import {verticalScale,moderateScale,scale} from 'react-native-size-matters'
export default StyleSheet.create({
    ImgBox: {
		width: 25,
		height: 25,
		resizeMode: 'contain',
		tintColor: 'blue',
		alignSelf: 'center',
		alignItems: 'center',
        color:'#696969',
        fontSize:20,
	},
    TIMES_Regular16: {
        fontFamily: 'Montserrat-Regular',
        paddingVertical:10,
        fontSize: 16,
        color: "#000"
      },
})