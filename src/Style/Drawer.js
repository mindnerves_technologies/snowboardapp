import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, YellowBox, Alert,
    FlatList, TextInput, CheckBox, ActivityIndicator, ImageBackground, BackHandler,
    PixelRatio, Share, Platform, StatusBar, Pressable, Linking, SafeAreaView, ScrollView
} from 'react-native';
import { Container, Content, Form, Item, Input, Label, Icon, CheckCircleIcon } from 'native-base';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import Modal from "react-native-modal";
import Images from "../constants/Images";
import global from '../Style/GlobalValue';
import global_style from '../Style/GlobalStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Util from '../Util/Util';
import I18n from '../common/localization/index';
import RNRestart from 'react-native-restart';

export default class Drawer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            deviceWidth: deviceWidth,
            deviceHeight: deviceHeight,
            showDrawer: true,
            languageDropdown: false,
            currentLanguage: "English",
        }
    }

    componentDidMount = () => {
        AsyncStorage.getItem('LANGUAGE').then(res => {
            let lang = JSON.parse(res)
            this.setState({ currentLanguage: lang })
          }).catch((error) => {
          })

        Util.getAsyncStorage('userData').then((data) => {
            console.log("data", data)
            if (data !== null) {
                this.setState({ userId: data.userId, getuserData: data })
            }
        })
    }

    gotoAssestmentRequest = () => {
        this.setState({ showDrawer: false });
        this.props.data.setState({ showDrawer: false })
        this.props.global_navigation.navigate('AssestmentRequest')
    }
    gotoCoachingRequest = () => {
        this.setState({ showDrawer: false });
        this.props.data.setState({ showDrawer: false })
        this.props.global_navigation.navigate('CoachingRequest')
    }
    gotoCreditTransaction = () => {
        this.setState({ showDrawer: false });
        this.props.data.setState({ showDrawer: false })
        this.props.global_navigation.navigate('creditTransaction')
    }
    gotoTermsconditionScreen = () => {
        this.setState({ showDrawer: false });
        this.props.data.setState({ showDrawer: false })
        this.props.global_navigation.navigate('TermsconditionScreen')
    }
    gotoPrivacypolicyScreen = () => {
        this.setState({ showDrawer: false });
        this.props.data.setState({ showDrawer: false })
        this.props.global_navigation.navigate('PrivacypolicyScreen')
    }

    changeLanguage = (language) => {
        AsyncStorage.setItem('LANGUAGE', JSON.stringify(language))
        // RNRestart.Restart();
        {console.log("language",language)}
        if (language === 'English') {
          I18n.locale = 'en';
        } else {
          I18n.locale = 'fr';
        }
        this.setState({ languageDropdown: !this.state.languageDropdown, currentLanguage: language })
        // this.props.navigation.navigate("VideoListScreen");
      }

    render() {
        return (
            <SafeAreaView style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
                <View style={{
                    flex: 1,
                    backgroundColor: '#fff',
                }} onLayout={(event) => { this.setState({ deviceHeight: event.nativeEvent.layout.height, deviceWidth: event.nativeEvent.layout.width }) }} >
                    {/* <StatusBar
                        barStyle="light-content"
                        hidden={false}
                        backgroundColor='blue'
                        translucent={false} /> */}
                    <Modal
                        animationIn={"slideInRight"}
                        animationOut={"slideOutRight"}
                        animationInTiming={600}
                        animationOutTiming={400}
                        onBackdropPress={() => { this.setState({ showDrawer: false }), this.props.data.setState({ showDrawer: false }) }}
                        onBackButtonPress={() => { this.setState({ showDrawer: false }), this.props.data.setState({ showDrawer: false }) }}
                        style={{ marginLeft: 100, marginTop: 0, marginBottom: 0, width: this.state.deviceWidth / 1.3, backgroundColor: 'white', height: '100%', }}
                        isVisible={this.state.showDrawer}>
                        <ScrollView style={{ flex: 1, backgroundColor: 'white', height: this.state.deviceHeight }}>
                            <View
                                style={{ width: '100%', backgroundColor: '#e3e6f0', flexDirection: "row", padding: 10, justifyContent: "space-between" }} >
                                <View style={{ flexDirection: "row" }}>

                                    <Image
                                        style={[{ width: 30, height: 30, resizeMode: 'contain', tintColor: "#696969", marginRight: 20 }]}
                                        source={Images.Profileimg} />
                                    {console.log("this.state.getuserData", this.state.getuserData)}
                                    {this.state.getuserData !== undefined && this.state.getuserData !== null &&
                                        <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: 16, color: "#00a0e4", alignSelf: "center", alignSelf: "center" }}>
                                            {this.state.getuserData.userFName} {this.state.getuserData.userLName} </Text>
                                    }
                                </View>
                                <TouchableOpacity
                                    hitSlop={{ top: 10, bottom: 10, left: 60, right: 60 }}
                                    onPress={() => { this.setState({ showDrawer: false }), this.props.data.setState({ showDrawer: false }) }}>
                                    <Image
                                        style={[{ width: 25, height: 25, resizeMode: 'contain', tintColor: "#181818", marginRight: 20, alignSelf: "center" }]}
                                        source={Images.icons_delete} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1 }}>
                                <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginHorizontal: 30 }}>
                                    <TouchableOpacity
                                        activeOpacity={global.activeOpacity}
                                        hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                        onPress={() => this.gotoAssestmentRequest()}
                                        style={{
                                            padding: 5,
                                            width: (this.state.deviceWidth / 1.6),
                                            alignSelf: 'center', flexDirection: 'row',

                                        }}>
                                        <Icon type={'FontAwesome'} name={"book"} style={[global_style.ImgBox, { marginRight: 20 }]} />
                                        <Text
                                            style={[global_style.TIMES_Regular16, {
                                                marginLeft: 3, marginTop: 5,
                                            }]}>
                                            {I18n.t('assessmentrequest')}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginHorizontal: 30 }}>
                                    <TouchableOpacity
                                        activeOpacity={global.activeOpacity}
                                        hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                        onPress={() => this.gotoCoachingRequest()}
                                        style={{
                                            padding: 5,
                                            width: (this.state.deviceWidth / 1.6),
                                            alignSelf: 'center', flexDirection: 'row',

                                        }}>
                                        <Icon type={'FontAwesome'} name={"user-circle"} style={[global_style.ImgBox, { marginRight: 20 }]} />
                                        <Text
                                            style={[global_style.TIMES_Regular16, {
                                                marginLeft: 3, marginTop: 5,
                                            }]}>
                                            {I18n.t('coachingrequest')}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginHorizontal: 30 }}>
                                    <TouchableOpacity
                                        activeOpacity={global.activeOpacity}
                                        hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                        onPress={() => this.gotoCreditTransaction()}
                                        style={{
                                            padding: 5,
                                            width: (this.state.deviceWidth / 1.6),
                                            alignSelf: 'center', flexDirection: 'row',

                                        }}>
                                        <Icon type={'FontAwesome'} name={"copyright"} style={[global_style.ImgBox, { marginRight: 20 }]} />
                                        <Text
                                            style={[global_style.TIMES_Regular16, {
                                                marginLeft: 3, marginTop: 5,
                                            }]}>
                                            {I18n.t('credittransaction')}
                                        </Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginHorizontal: 30 }}>
                                    <TouchableOpacity
                                        activeOpacity={global.activeOpacity}
                                        hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                        onPress={() => this.setState({ languageDropdown: !this.state.languageDropdown })}
                                        style={{
                                            padding: 5,
                                            width: (this.state.deviceWidth / 1.6),
                                            alignSelf: 'center', flexDirection: 'row',

                                        }}>
                                        <Icon type={'MaterialIcons'} name={this.state.languageDropdown ? "keyboard-arrow-up" : "keyboard-arrow-down"} style={[global_style.ImgBox, { marginRight: 20, fontSize: 30 }]} />
                                        <Text
                                            style={[global_style.TIMES_Regular16, {
                                                marginLeft: 3, marginTop: 5,
                                            }]}>
                                            {I18n.t('language')}
                                        </Text>
                                    </TouchableOpacity>

                                    {this.state.languageDropdown &&
                                        <View>
                                            <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginHorizontal: 30 }}>
                                                <TouchableOpacity
                                                    activeOpacity={global.activeOpacity}
                                                    hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                                    style={{

                                                        width: (this.state.deviceWidth / 1.6), padding: 5,
                                                        alignSelf: 'center', flexDirection: 'row'
                                                    }}
                                                    onPress={() => this.changeLanguage("English")}
                                                >
                                                    <View 
                                                    // type={'MaterialIcons'} name="check" color={this.state.currentLanguage == "English" ? "blue" : "white"} 
                                                    style={[global_style.ImgBox, { marginRight: 20, fontSize: 30 }]} />

                                                    <Text
                                                        style={[global_style.TIMES_Regular16, {
                                                            marginLeft: 5, marginTop: 5,
                                                        }]}>
                                                        {'English'}
                                                    </Text>

                                                </TouchableOpacity>
                                            </View>
                                            
                                                <TouchableOpacity
                                                    activeOpacity={global.activeOpacity}
                                                    hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                                    style={{

                                                        width: (this.state.deviceWidth / 1.6), padding: 5,
                                                        alignSelf: 'center', flexDirection: 'row'
                                                    }}
                                                    onPress={() => this.changeLanguage("french")}
                                                >

                                                    <View 
                                                    // type={'MaterialIcons'} name="check" color={this.state.currentLanguage == "English" ? "blue" : "white"} 
                                                    style={[global_style.ImgBox, { marginRight: 20, fontSize: 30 }]} />

                                                    <Text
                                                        style={[global_style.TIMES_Regular16, {
                                                            marginLeft: 5, marginTop: 5,
                                                        }]}>
                                                        {'fran�ais'}
                                                    </Text>

                                                </TouchableOpacity>
                                        </View>}

                                </View>

                                <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginHorizontal: 30 }}>
                                    <TouchableOpacity
                                        activeOpacity={global.activeOpacity}
                                        hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                        onPress={() => this.gotoTermsconditionScreen()}
                                        style={{
                                            padding: 5,
                                            width: (this.state.deviceWidth / 1.6),
                                            alignSelf: 'center', flexDirection: 'row',

                                        }}>
                                        <Icon type={'FontAwesome'} name={'file'} style={[global_style.ImgBox, { marginRight: 20 }]} />
                                        <Text
                                            style={[global_style.TIMES_Regular16, {
                                                marginLeft: 3, marginTop: 5,
                                            }]}>
                                            {I18n.t('termsandconditions')}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginHorizontal: 30 }}>
                                    <TouchableOpacity
                                        activeOpacity={global.activeOpacity}
                                        hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                        onPress={() => this.gotoPrivacypolicyScreen()}
                                        style={{
                                            padding: 5,
                                            width: (this.state.deviceWidth / 1.6),
                                            alignSelf: 'center', flexDirection: 'row',

                                        }}>
                                        <Icon type={'Entypo'} name={"check"} style={[global_style.ImgBox, { marginRight: 20 }]} />
                                        <Text
                                            style={[global_style.TIMES_Regular16, {
                                                marginLeft: 3, marginTop: 5,
                                            }]}>
                                            {I18n.t('privacypolicy')}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717', marginHorizontal: 30 }}>
                                    <TouchableOpacity
                                        activeOpacity={global.activeOpacity}
                                        hitSlop={{ top: 10, bottom: 10, left: 30, right: 30 }}
                                        // onPress={() => this.gotoDashboard()}
                                        style={{
                                            padding: 5,
                                            width: (this.state.deviceWidth / 1.6),
                                            alignSelf: 'center', flexDirection: 'row',

                                        }}>
                                        <Icon type={'Entypo'} name={"warning"} style={[global_style.ImgBox, { marginRight: 20 }]} />
                                        <Text
                                            style={[global_style.TIMES_Regular16, {
                                                marginLeft: 3, marginTop: 5,
                                            }]}>
                                            {I18n.t('helpcenter')}
                                        </Text>
                                    </TouchableOpacity>
                                    <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: '#171717' }} />
                                </View>
                            </View>
                        </ScrollView>
                        {/* <Text style={{ fontSize: 20 }}>Drawer Screen</Text> */}
                    </Modal>
                </View>
            </SafeAreaView>
        )
    }
}