/**
 * @author Kartikaiya Mayatwal
 * Date Dec 24, 2021
 */

import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import promise from './Promise';
import reducer from '../src/redux/reducers';

export default function configureStore(onCompletion){
	const enhancer = compose(
		applyMiddleware(
			thunk,
			promise
		)
	);

	const store = createStore(reducer, enhancer);
	return store;
}
