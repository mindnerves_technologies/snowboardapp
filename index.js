
/**
 * @author Kartikaiya Mayatwal
 * Date Dec 24, 2021
 */

import React, { Component } from 'react';
import { View, Platform, BackHandler } from 'react-native';
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import configureStore from './src/configureStore';

export default class NavigationProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            store: configureStore()
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
        });
    }

    componentWillUnmount() {
        if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress')
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Provider store={this.state.store}>
                    <App />
                </Provider>
            </View>
        );
    }
}

AppRegistry.registerComponent(appName, () => NavigationProject);

