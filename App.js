
// import { createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';
// import ReactNative, { Dimensions } from 'react-native';
// const { height, width } = Dimensions.get('window');

// // import SplashScreen from './src/pages/SplashScreen/index';
// import LoginScreen from './src/components/Login';
// import UploadVideoScreen from './src/components/UploadVideoScreen';
// import SignupScreen from './src/components/SignupScreen';
// import HomeScreen from './src/components/HomeScreen';
// import ChatUserListScreen from './src/components/ChatUserListScreen';
// import VideoListScreen from './src/components/VideoListScreen';
// // import DrawerContent from './src/common/menu';

// const AppNavigation = createStackNavigator({
//     // VideoListScreen: { screen: VideoListScreen, navigationOptions: { header: null, gesturesEnabled: false } },
//     HomeScreen: { screen: HomeScreen, navigationOptions: { header: null, gesturesEnabled: false } },
//     ChatUserListScreen: { screen: ChatUserListScreen, navigationOptions: { header: null, gesturesEnabled: false } },
//     Login: { screen: LoginScreen, navigationOptions: { header: null, gesturesEnabled: false } },
//     UploadVideoScreen: { screen: UploadVideoScreen, navigationOptions: { header: null, gesturesEnabled: false } },
//     SignupScreen: { screen: SignupScreen, navigationOptions: { header: null, gesturesEnabled: false } },
//     VideoListScreen: { screen: VideoListScreen, navigationOptions: { header: null, gesturesEnabled: false } },

// });


// const MyApp = createDrawerNavigator({
//     App: { screen: AppNavigation, }
// }, {
//     drawerWidth: width,
//     drawerPosition: ReactNative.I18nManager.isRTL ? 'right' : 'left',
//     // contentComponent: DrawerContent
// });

// const LandingNavigation = createStackNavigator({

//     Root: { screen: MyApp, navigationOptions: ({ navigation }) => ({ header: null, gesturesEnabled: false }) },
// });

// const App = createAppContainer(LandingNavigation);

// export default App;


import { createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';
import ReactNative, { Dimensions } from 'react-native';
const { height, width } = Dimensions.get('window');


// import SplashScreen from './src/pages/SplashScreen/index';
import HomeScreen from './src/components/HomeScreen';
import LoginScreen from './src/components/Login';
import RequestAssesmentScreen from './src/components/RequestAssesmentScreen';
import SignupScreen from './src/components/SignupScreen';
import UserassesmentScreen from './src/components/Userassesment';
import ChatUserListScreen from './src/components/ChatUserListScreen';
import VideoListScreen from './src/components/VideoListScreen';
import MyAccountScreen from './src/components/MyAccountScreen';
import PendingRequestScreen from './src/components/PendingRequestScreen';
import ChatScreen from './src/components/ChatScreen';
import ChallengeScreen from './src/components/Challenge';
import TermsconditionScreen from './src/components/TermsconditionScreen';
import PrivacypolicyScreen from './src/components/PrivacypolicyScreen';
import AssestmentRequest from './src/components/AssestmentRequest';
import CoachingRequest from './src/components/CoachingRequest';
import ForgotPassword from './src/components/ForgotPassword';
import SplashScreen from './src/components/SplashScreen';
import CreditPlanScreen from './src/components/CreditPlanScreen';
import Editprofile from './src/components/Editprofile';
import creditTransaction from './src/components/creditTransaction';
import PaymentScreen from './src/components/Payment';
import { StripeProvider } from '@stripe/stripe-react-native';
// const publishableKey = "pk_test_51KVZg7SIjp4YCshV5achqah7KPzMjI57e7ndtkG9NmlM9Kmscm7DukZfCSHQnUclC0trNtGncgkfvWzOT5Cl7jnL00TH6baBEv";
const AppNavigation = createStackNavigator({
    
    SplashScreen: { screen: SplashScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    HomeScreen: { screen: HomeScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    CreditPlanScreen: { screen: CreditPlanScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    PaymentScreen: { screen: PaymentScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    creditTransaction: { screen: creditTransaction, navigationOptions: { header: null, gesturesEnabled: false } },
    Editprofile: { screen: Editprofile, navigationOptions: { header: null, gesturesEnabled: false } },
    Login: { screen: LoginScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    ForgotPassword: { screen: ForgotPassword, navigationOptions: { header: null, gesturesEnabled: false } },
    RequestAssesmentScreen: { screen: RequestAssesmentScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    SignupScreen: { screen: SignupScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    MyAccountScreen: { screen: MyAccountScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    TermsconditionScreen: { screen: TermsconditionScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    ChallengeScreen: { screen: ChallengeScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    UserassesmentScreen: { screen: UserassesmentScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    PrivacypolicyScreen: { screen: PrivacypolicyScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    VideoListScreen: { screen: VideoListScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    ChatUserListScreen: { screen: ChatUserListScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    ChatScreen: { screen: ChatScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    PendingRequestScreen: { screen: PendingRequestScreen, navigationOptions: { header: null, gesturesEnabled: false } },
    AssestmentRequest: { screen: AssestmentRequest, navigationOptions: { header: null, gesturesEnabled: false } },
    CoachingRequest: { screen: CoachingRequest, navigationOptions: { header: null, gesturesEnabled: false } },
    // videoupload: { screen: videoupload, navigationOptions: { header: null, gesturesEnabled: false } },

});


const MyApp = createDrawerNavigator({
    App: { screen: AppNavigation, }
}, {
    drawerWidth: width,
    drawerPosition: ReactNative.I18nManager.isRTL ? 'right' : 'left',
    // contentComponent: DrawerContent
});

const LandingNavigation = createStackNavigator({

    Root: { screen: MyApp, navigationOptions: ({ navigation }) => ({ header: null, gesturesEnabled: false }) },
});

const App = createAppContainer(LandingNavigation);

export default App;








